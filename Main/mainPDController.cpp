#include <Deformable/FEMPDController.h>
#include <iostream>

using namespace PHYSICSMOTION;
typedef FLOAT T;
DECL_MAT_VEC_MAP_TYPES_T

int main(int,char**) {
  FEMPDController<T> ctrl;
  ctrl.resetP(Vec::Random(5),10);
  ctrl.debugDGDu(20,0.1);
  ctrl.resetD(Vec::Random(10),15);
  ctrl.debugDGDu(20,0.1);
  ctrl.resetPD(Vec::Random(5),10,Vec::Random(10),15);
  ctrl.debugDGDu(20,0.1);

  FEMReducedPDController<T> ctrlR;
  ctrlR.resetPReduced(Vec::Random(5),2.0,MatT::Random(15,5));
  ctrl.debugDGDu(20,0.1);
  ctrlR.resetDReduced(Vec::Random(10),2.0,MatT::Random(12,10));
  ctrl.debugDGDu(20,0.1);
  ctrlR.resetPDReduced(Vec::Random(5),2.0,Vec::Random(5),2.0,MatT::Random(12,5));
  ctrl.debugDGDu(20,0.1);
  return 0;
}
