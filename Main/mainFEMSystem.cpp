#include <Deformable/FEMMesh.h>
#include <Deformable/FEMSystem.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <Environment/EnvironmentVisualizer.h>
#include <TinyVisualizer/MakeTexture.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  std::shared_ptr<FEMMesh<T>> body;
  GLfloat dx;
  if(TYPE==0) {
    dx=10;
    body.reset(new FEMMesh<T>("torus.obj_tet.mesh",true));
  } else if(TYPE==1) {
    dx=2;
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
  } else if(TYPE==2) {
    dx=2;
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    FEMOctreeMesh<T> bodyHOct(*body,0);
    body=bodyHOct.getMesh();
  } else return 0;

  FEMSystem<T> sys(body);
  sys.setEnv(std::shared_ptr<EnvironmentHeight<T>>(new EnvironmentHeight<T>));
  sys.getEnv()->createHills(-10,10,-10,10,[&](double x,double y)->double {
    return sin(x)*sin(y)-2;
  },.1);
  sys.addGravitationalEnergy(9.81f);
  //sys.addLinearElasticEnergy(10000,1000);
  //sys.addStVKElasticEnergy(10000,1000);
  sys.addCorotatedElasticEnergy(10000,1000);
  //sys.addNonHookeanElasticEnergy(10000,1000);
  sys.addFluidEnergy(2,0.8,9.81f,5);
  sys.addPointConstraint(body->pos0(),FEMSystem<T>::Vec3T::Zero(),1000);
  sys.SerializableBase::writeStr("sys.dat");
  sys=FEMSystem<T>();
  sys.SerializableBase::readStr("sys.dat");
  sys.addRandomPenetrationEnergy(1000);
  sys.debugAssemble();

  //drawer
  Drawer drawer(argc,argv);
  //system
  FEMSystem<T>::Vec pos=body->pos0();
  std::shared_ptr<Shape> s=visualizeFEMMeshSurface(body);
  drawer.addShape(s);
  //env
  std::shared_ptr<Shape> envS=visualizeEnvironment(sys.getEnv(),Eigen::Matrix<GLfloat,2,1>(0.01f,0.01f));
  envS->setColorAmbient(GL_TRIANGLES,1,1,1);
  envS->setTexture(drawGrid(10,0.01f,0.03f,Eigen::Matrix<GLfloat,3,1>(1,1,1),Eigen::Matrix<GLfloat,3,1>(135/255.,54/255.,0/255.)));
  drawer.addShape(envS);
  //update collision
  std::shared_ptr<MeshShape> cS;
  std::vector<FEMCollision<T>> colls;
  drawer.setKeyFunc([&](GLFWwindow* wnd,int key,int scan,int action,int mods,bool captured) {
    if(captured)
      return;
    else if(key==GLFW_KEY_UP && action==GLFW_PRESS) {
      for(int i=0; i<pos.size(); i+=3)
        pos.template segment<3>(i)+=FEMSystem<T>::Vec3T::UnitZ()*0.1f*dx;
      updateSurface(s,body,pos);
      sys.detectCollision(pos,colls);
      visualizeCollision(drawer,cS,colls);
    } else if(key==GLFW_KEY_DOWN && action==GLFW_PRESS) {
      for(int i=0; i<pos.size(); i+=3)
        pos.template segment<3>(i)-=FEMSystem<T>::Vec3T::UnitZ()*0.1f*dx;
      updateSurface(s,body,pos);
      sys.detectCollision(pos,colls);
      visualizeCollision(drawer,cS,colls);
    }
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,1.5),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.2,.2,.2),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.mainLoop();
  return 0;
}
