#include <Deformable/FEMGradientInfo.h>

using namespace PHYSICSMOTION;
typedef FLOAT T;
DECL_MAT_VEC_MAP_TYPES_T
DECL_MAP_FUNCS
typedef Eigen::Triplet<T,int> STrip;
typedef ParallelVector<STrip> STrips;
typedef Eigen::SparseMatrix<T,0,int> SMatT;

#define N 102
#define N2 204
#define M 10
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  FEMGradientInfo<T>::debug(N,N2,M);
  return 0;
}
