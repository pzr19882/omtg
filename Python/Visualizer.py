BASE="../../omtg-build"

import numpy as np
import importlib,sys
sys.path.append(BASE)
import pyPhysicsMotion as pm
from pyTinyVisualizer import pyTinyVisualizer as vis

def drawFEM(sys,waterLevel=None,cb=None,lightPos=[0,0,1],waterSize=5,scaleEnvTc=None):
    drawer=vis.Drawer(0,None)
    drawer.addPlugin(vis.CameraExportPlugin(vis.GLFW_KEY_2,vis.GLFW_KEY_3,"camera.dat"))
    drawer.addPlugin(vis.CaptureGIFPlugin(vis.GLFW_KEY_1,"record.gif",drawer.FPS()))
    
    #body
    if isinstance(sys,pm.FEMReducedSystemFLOAT):
        bodyS=pm.visualizeFEMLowDimensionalMeshSurfaceFLOAT(sys)
    else: 
        assert isinstance(sys,pm.FEMSystemFLOAT)
        bodyS=pm.visualizeFEMMeshSurfaceFLOAT(sys.getBody())
    drawer.addShape(bodyS)

    #water level
    if waterLevel is not None:
        waterS=pm.visualizeFEMWaterLevelFLOAT(waterLevel,waterSize)
        drawer.addShape(waterS)
        
    #env
    if sys.getEnv() is not None:
        if scaleEnvTc is not None:
            envS=pm.visualizeEnvironmentFLOAT(sys.getEnv(),scaleEnvTc)
        else: envS=pm.visualizeEnvironmentFLOAT(sys.getEnv())
        envS.setColorAmbient(vis.GL_TRIANGLES,1.,1.,1.)
        envS.setTexture(vis.drawGrid(10,0.01,0.03,
                                     np.array([1,1,1],dtype=np.single),
                                     np.array([135/255.,54/255.,0/255.],dtype=np.single)))
        envSTrans=vis.Bullet3DShape()
        envSTrans.addShape(envS)
        drawer.addShape(envSTrans)

    #light
    drawer.addLightSystem()
    drawer.getLight().lightSz(10)
    drawer.getLight().addLight(np.array(lightPos,dtype=np.single),
                               np.array([.3,.3,.3],dtype=np.single),
                               np.array([.8,.8,.8],dtype=np.single),
                               np.array([.5,.5,.5],dtype=np.single))  
    
    if cb is not None:
        cb.bodyS=bodyS
        if sys.getEnv() is not None:
            cb.envSTrans=envSTrans
        cb.envS=sys.getEnv()
        drawer.setPythonCallback(cb)
        drawer.addPlugin(vis.ImGuiPlugin(cb))
    drawer.addCamera3D(90,np.array([0,0,1],dtype=np.single))
    drawer.getCamera3D().setManipulator(vis.FirstPersonCameraManipulator(drawer.getCamera3D()))
    drawer.mainLoop()
