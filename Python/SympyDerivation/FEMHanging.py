BASE="../../libdifferentiable-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	type=0
	if type==0:
		body=pm.FEMMeshFLOAT(BASE+"/torus.obj_tet.mesh")
	elif type==1:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
	else:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		bodyHOct=FEMOctreeMeshFLOAT(body,0)
		body=bodyHOct.getMesh()
    
	waterLevel=-2
	waterDensity=2
	Lambda=10000
	Mu=10000
	g=9.81
    
	sys=pm.FEMSystemFLOAT(body)
	sys.addStVKElasticEnergy(Lambda,Mu)
	sys.addGravitationalEnergy(g)
	sys.addPointConstraint(body.restPos(),np.array([0,0,0],dtype=np.double),1000)
    
	frames=[body.restPos(),body.restPos()]
	while(len(frames)<1000):
		print("Simulating frame: %d"%len(frames))
		ret=sys.step(frames[-2],frames[-1],pm.FEMSystemFLOAT.SP)
		frames.append(ret[1])

    #viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,body,frames[self.frameId])
			self.frameId=(self.frameId+1)%len(frames)
	drawFEM(sys,cb=CustomPythonCallback())
