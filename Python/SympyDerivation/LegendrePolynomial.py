from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

a,b,c,d,s=symbols('a b c d s')
ca=-(s-2*s**2+s**3)/2
cb=(2-5*s**2+3*s**3)/2
cc=(s+4*s**2-3*s**3)/2
cd=(s**3-s**2)/2
P=a*ca+b*cb+c*cc+d*cd

print("P(0)=",P.subs(s,0))
print("P'(0)=",diff(P,s).subs(s,0))
print("P(1)=",P.subs(s,1))
print("P'(1)=",diff(P,s).subs(s,1))

aB,bB,cB,dB=symbols('aB bB cB dB')
ca=(1-s)**3
cb=3*(1-s)**2*s
cc=3*(1-s)*s**2
cd=s**3
PB=aB*ca+bB*cb+cB*cc+dB*cd

print("PB(0)=",PB.subs(s,0))
print("PB'(0)=",diff(PB,s).subs(s,0))
print("PB(1)=",PB.subs(s,1))
print("PB'(1)=",diff(PB,s).subs(s,1))

#solve for control points
dict=solve(P-PB,aB,bB,cB,dB)
print(dict[aB])
print(dict[bB])
print(dict[cB])
print(dict[dB])
PBSame=PB.subs(aB,dict[aB]).subs(bB,dict[bB]).subs(cB,dict[cB]).subs(dB,dict[dB])
print("Err=",simplify(PBSame-P))