import numpy as np
from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

def det2(m):
    return m[0,0]*m[1,1]-m[0,1]*m[1,0]
def det3(m):
    return  m[0,0]*(m[1,1]*m[2,2]-m[1,2]*m[2,1])-\
            m[0,1]*(m[1,0]*m[2,2]-m[2,0]*m[1,2])+\
            m[0,2]*(m[1,0]*m[2,1]-m[1,1]*m[2,0])

#decl points
def integrateHex(expr):
    for d in range(3):
        expr=integrate(expr,(bary[d],0,1))
    return simplify(expr)
def declPt(id):
    return np.array(symbols('VSS[%d][0] VSS[%d][1] VSS[%d][2]'%(id,id,id)))
vss=[declPt(i) for i in range(4)]

#flatten
vssFlat=[]
for v in vss:
    vssFlat+=list(v)

#compute deformable gradient
F=np.array([[vss[1][0]-vss[0][0],vss[2][0]-vss[0][0],vss[3][0]-vss[0][0]],
            [vss[1][1]-vss[0][1],vss[2][1]-vss[0][1],vss[3][1]-vss[0][1]],
            [vss[1][2]-vss[0][2],vss[2][2]-vss[0][2],vss[3][2]-vss[0][2]]])
vol=det3(F)

#value
print("T vol=%s;"%default_printer(vol))

#gradient
gradient=[diff(vol,i) for i in vssFlat]
off=0
print("if(grad) {")
print("  grad->resize(24);")
for g in gradient:
    print("  (*grad)[%d]=%s;"%(off,default_printer(g)))
    off+=1
print("}")
    
#hessian
hessian=[[diff(g,i) for g in gradient] for i in vssFlat]
offr=0
print("if(hess) {")
print("  hess->setZero(24,24);")
for r in hessian:
    offc=0
    for c in r:
        if c==0:
            continue
        elif(offr>offc):
            print("  (*hess)(%d,%d)=(*hess)(%d,%d);"%(offr,offc,offc,offr))
        else: 
            print("  (*hess)(%d,%d)=%s;"%(offr,offc,default_printer(c)))
        offc+=1
    offr+=1
print("}")
print("return vol;")