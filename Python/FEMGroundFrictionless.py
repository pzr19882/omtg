BASE="../../omtg-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	type=0
	if type==0:
		body=pm.FEMMeshFLOAT(BASE+"/torus.obj_tet.mesh",True)
	elif type==1:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
	else:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		bodyHOct=FEMOctreeMeshFLOAT(body,0)
		body=bodyHOct.getMesh()
    
	Lambda=10000
	Mu=10000
	g=9.81
    
	sys=pm.FEMSystemFLOAT(body)
	sys.setEnv(pm.EnvironmentHeightFLOAT());
	class CustomEnvCallback(pm.EnvironmentCallback):
		def __init__(self):
		  	pm.EnvironmentCallback.__init__(self)
		def height(self,x,y):
			return -2+x*0.1
	sys.getEnv().createHills(-20,20,-20,20,CustomEnvCallback(),.1)
	sys.addStVKElasticEnergy(Lambda,Mu)
	sys.addGravitationalEnergy(g)
	sys.maxContact(10)
	sys.mu(0)
    
	frames=[body.restPos(),body.restPos()]
	ctrl=pm.FEMPDControllerFLOAT()
	while(len(frames)<1000):
		print("Simulating frame: %d"%len(frames))
		ret=sys.step(frames[-2],frames[-1],ctrl,pm.FEMSystemFLOAT.SP)
		frames.append(ret[0])

    #viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,body,frames[self.frameId])
			self.frameId=(self.frameId+1)%len(frames)
	drawFEM(sys,cb=CustomPythonCallback(),scaleEnvTc=np.array([.01,.01],dtype=np.single))
