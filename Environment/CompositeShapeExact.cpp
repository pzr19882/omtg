#include "CompositeShapeExact.h"
#include "MeshExact.h"
#include "ConvexHullExact.h"
#include "SphericalBBoxExact.h"
#include <Utils/IO.h>

namespace PHYSICSMOTION {
CompositeShapeExact::CompositeShapeExact() {}
CompositeShapeExact::CompositeShapeExact
(const std::vector<std::shared_ptr<ShapeExact>>& geoms,
 const std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>>& trans):_geoms(geoms),_trans(trans) {
  for(int i=0; i<(int)_geoms.size(); i++) {
    BBoxExact bb=_geoms[i]->getBB();
    for(const T& x: {
          bb.minCorner()[0],bb.maxCorner()[0]
        })
      for(const T& y: {
            bb.minCorner()[1],bb.maxCorner()[1]
          })
        for(const T& z: {
              bb.minCorner()[2],bb.maxCorner()[2]
            })
          _bb.setUnion(ROT(_trans[i])*Vec3T(x,y,z)+CTR(_trans[i]));
  }
  ASSERT(_trans.size()==_geoms.size())
}
CompositeShapeExact::CompositeShapeExact
(const std::vector<std::shared_ptr<ShapeExact>>& geoms):_geoms(geoms),_trans(geoms.size(),Mat3X4T::Identity()) {
  for(int i=0; i<(int)_geoms.size(); i++) {
    BBoxExact bb=_geoms[i]->getBB();
    _bb.setUnion(bb.minCorner());
    _bb.setUnion(bb.maxCorner());
  }
  ASSERT(_trans.size()==_geoms.size())
}
bool CompositeShapeExact::read(std::istream& is,IOData* dat) {
  registerType<MeshExact>(dat);
  registerType<ConvexHullExact>(dat);
  registerType<BBoxExact>(dat);
  registerType<SphericalBBoxExact>(dat);
  readBinaryData(_geoms,is,dat);
  readBinaryData(_trans,is,dat);
  readBinaryData(_bb,is,dat);
  return is.good();
}
bool CompositeShapeExact::write(std::ostream& os,IOData* dat) const {
  registerType<MeshExact>(dat);
  registerType<ConvexHullExact>(dat);
  registerType<BBoxExact>(dat);
  registerType<SphericalBBoxExact>(dat);
  writeBinaryData(_geoms,os,dat);
  writeBinaryData(_trans,os,dat);
  writeBinaryData(_bb,os,dat);
  return os.good();
}
std::shared_ptr<SerializableBase> CompositeShapeExact::copy() const {
  std::shared_ptr<CompositeShapeExact> ret(new CompositeShapeExact);
  ret->_geoms.resize(_geoms.size());
  for(int i=0; i<(int)ret->_geoms.size(); i++)
    ret->_geoms[i]=std::dynamic_pointer_cast<ShapeExact>(_geoms[i]->copy());
  ret->_trans=_trans;
  ret->_bb=_bb;
  return ret;
}
std::string CompositeShapeExact::type() const {
  return typeid(CompositeShapeExact).name();
}
const BBoxExact& CompositeShapeExact::getBB() const {
  return _bb;
}
bool CompositeShapeExact::empty() const {
  for(std::shared_ptr<ShapeExact> s:_geoms)
    if(!s->empty())
      return false;
  return true;
}
void CompositeShapeExact::getMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                                  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) const {
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vssTmp;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> issTmp;
  for(int i=0; i<(int)_geoms.size(); i++) {
    _geoms[i]->getMesh(vssTmp,issTmp);
    Eigen::Matrix<int,3,1> off((int)vss.size(),(int)vss.size(),(int)vss.size());
    for(const Eigen::Matrix<double,3,1>& v:vssTmp)
      vss.push_back(ROT(_trans[i]).template cast<double>()*v+CTR(_trans[i]).template cast<double>());
    for(const Eigen::Matrix<int,3,1>& i:issTmp)
      iss.push_back(i+off);
  }
}
bool CompositeShapeExact::closestInner(const Vec3T& pt,Vec3T& n,Vec3T& normal,Mat3T& hessian,
                                       T& rad,Eigen::Matrix<int,2,1>& feat,bool cache,
                                       std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>>*) const {
  bool ret=true;
  Vec3T nTmp,normalTmp;
  Mat3T hessianTmp;
  T radTmp;
  Eigen::Matrix<int,2,1> featTmp;
  double minDist=std::numeric_limits<double>::max();  //this is not safe, reverting to double from rational
  for(int i=0; i<(int)_geoms.size(); i++) {
    bool retTmp=_geoms[i]->closestInner(ROT(_trans[i]).transpose()*(pt-CTR(_trans[i])),nTmp,normalTmp,hessianTmp,radTmp=0,featTmp,cache,NULL);
    double dist=nTmp.template cast<double>().norm()-(double)radTmp;  //this is not safe, reverting to double from rational
    if(dist<minDist) {
      n=ROT(_trans[i])*nTmp;
      normal=ROT(_trans[i])*normalTmp;
      hessian=ROT(_trans[i]).transpose()*hessianTmp*ROT(_trans[i]);
      rad=radTmp;
      feat=featTmp;
      ret=retTmp;
      minDist=dist;
    }
  }
  return ret;
}
void CompositeShapeExact::scale(T coef) {
  for(std::shared_ptr<ShapeExact> s:_geoms)
    s->scale(coef);
  for(Mat3X4T& t:_trans)
    CTR(t)*=coef;
}
const std::vector<std::shared_ptr<ShapeExact>>& CompositeShapeExact::getGeoms() const {
  return _geoms;
}
const std::vector<CompositeShapeExact::Mat3X4T,Eigen::aligned_allocator<CompositeShapeExact::Mat3X4T>>& CompositeShapeExact::getTrans() const {
  return _trans;
}
}
