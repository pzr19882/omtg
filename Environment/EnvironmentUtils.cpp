#include "EnvironmentUtils.h"
#include "ConvexHullExact.h"
#include <Utils/Pragma.h>
#include <stack>

//mesh processing
namespace PHYSICSMOTION {
size_t EdgeHash::operator()(const Eigen::Matrix<int,2,1>& key) const {
  std::hash<int> h;
  return h(key[0])+h(key[1]);
}
bool EdgeHash::operator()(const Eigen::Matrix<int,2,1>& a,const Eigen::Matrix<int,2,1>& b) const {
  for(int i=0; i<2; i++)
    if(a[i]<b[i])
      return true;
    else if(a[i]>b[i])
      return false;
  return false;
}
size_t TriangleHash::operator()(const Eigen::Matrix<int,3,1>& key) const {
  std::hash<int> h;
  return h(key[0])+h(key[1])+h(key[2]);
}
bool TriangleHash::operator()(const Eigen::Matrix<int,3,1>& a,const Eigen::Matrix<int,3,1>& b) const {
  for(int i=0; i<3; i++)
    if(a[i]<b[i])
      return true;
    else if(a[i]>b[i])
      return false;
  return false;
}
void buildEdge(const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,
               std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash>& edgeMap) {
  for(int i=0; i<(int)iss.size(); i++) {
    ASSERT_MSG(iss[i][2]!=-1,"You cannot use BottomUp BVH building in 3D Meshes!")
    for(int d=0; d<3; d++) {
      //edge index
      Eigen::Matrix<int,2,1> e(iss[i][d],iss[i][(d+1)%3]);
      if(e[0]>e[1])
        std::swap(e[0],e[1]);
      //insert edge
      auto it=edgeMap.find(e);
      if(it==edgeMap.end())
        edgeMap[e]=std::make_pair(i,-1);
      else {
        ASSERT_MSGV(it->second.second==-1,
                    "Non-manifold mesh detected, "
                    "Edge(%d,%d) alreadying bordering triangle: "
                    "(%d,%d,%d) and (%d,%d,%d) when trying to border triangle: "
                    "(%d,%d,%d)!",e[0],e[1],
                    iss[it->second.first][0],iss[it->second.first][1],iss[it->second.first][2],
                    iss[it->second.second][0],iss[it->second.second][1],iss[it->second.second][2],
                    iss[i][0],iss[i][1],iss[i][2])
        it->second.second=i;
      }
    }
  }
}
void makeUniform(std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,
                 int i,int j,int v0,int v1) {
  int v0i,v1i,v0j,v1j;
  for(int d=0; d<3; d++) {
    if(iss[i][d]==v0)v0i=d;
    if(iss[i][d]==v1)v1i=d;
    if(iss[j][d]==v0)v0j=d;
    if(iss[j][d]==v1)v1j=d;
  }
  bool isI=(v0i+1)%3==v1i;
  bool isJ=(v0j+1)%3==v1j;
  if(isI==isJ)
    std::swap(iss[j][1],iss[j][2]);
}
void makeUniform(std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  //initialize edge
  std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash> eMap;
  buildEdge(iss,eMap);
  //make uniform
  std::vector<bool> visited(iss.size(),false);
  for(int i=0; i<(int)visited.size(); i++)
    if(!visited[i]) {
      std::stack<int> queue;
      queue.push(i);
      visited[i]=true;
      while(!queue.empty()) {
        int ti=queue.top();
        queue.pop();
        const Eigen::Matrix<int,3,1>& I=iss[ti];
        Eigen::Matrix<int,2,1> e;
        for(int eid=0; eid<3; eid++) {
          e[0]=(int)I[eid];
          e[1]=(int)I[(eid+1)%3];
          ASSERT(e[0]!=e[1])
          if(e[0]>e[1])
            std::swap(e[0],e[1]);
          std::pair<int,int> edg=eMap[e];
          if(edg.second>=0) {
            if(edg.second==ti)
              std::swap(edg.first,edg.second);
            if(!visited[edg.second]) {
              makeUniform(iss,edg.first,edg.second,e[0],e[1]);
              queue.push(edg.second);
              visited[edg.second]=true;
            }
          }
        }
      }
    }
}
void makeInsideOut(std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  for(int i=0; i<(int)iss.size(); i++) {
    std::swap(iss[i][0],iss[i][1]);
  }
}
std::shared_ptr<ConvexHullExact> makeConvexPolygon(const std::vector<Eigen::Matrix<double,2,1>,Eigen::aligned_allocator<Eigen::Matrix<double,2,1>>>& vss,const Eigen::Matrix<double,2,1>& height) {
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss3D;
  for(const Eigen::Matrix<double,2,1>& v:vss) {
    vss3D.push_back(Eigen::Matrix<double,3,1>(v[0],v[1],height[0]));
    vss3D.push_back(Eigen::Matrix<double,3,1>(v[0],v[1],height[1]));
  }
  return std::shared_ptr<ConvexHullExact>(new ConvexHullExact(vss3D));
}
std::shared_ptr<ConvexHullExact> makeRegularConvexPolygon(int n,double radius,const Eigen::Matrix<double,2,1>& pos,const Eigen::Matrix<double,2,1>& height) {
  std::vector<Eigen::Matrix<double,2,1>,Eigen::aligned_allocator<Eigen::Matrix<double,2,1>>> vss;
  for(int i=0; i<n; i++) {
    double angle=i*M_PI*2/n;
    vss.push_back(Eigen::Matrix<double,2,1>(cos(angle)*radius+pos[0],sin(angle)*radius+pos[1]));
  }
  return makeConvexPolygon(vss,height);
}
double signedVolume(const Eigen::Matrix<double,3,1>& a,
                    const Eigen::Matrix<double,3,1>& b,
                    const Eigen::Matrix<double,3,1>& c) {
  double v321=c.x()*b.y()*a.z();
  double v231=b.x()*c.y()*a.z();
  double v312=c.x()*a.y()*b.z();
  double v132=a.x()*c.y()*b.z();
  double v213=b.x()*a.y()*c.z();
  double v123=a.x()*b.y()*c.z();
  return (1.0f/6.0f)*(-v321+v231+v312-v132-v213+v123);
}
double volume(const std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
              const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  double volume=0.0;
  for(int it=0; it<(int)iss.size(); it++) {
    const Eigen::Matrix<double,3,1>& p1=vss[iss[it].x()];
    const Eigen::Matrix<double,3,1>& p2=vss[iss[it].y()];
    const Eigen::Matrix<double,3,1>& p3=vss[iss[it].z()];
    volume+=signedVolume(p1,p2,p3);
  }
  return volume;
}
}

//make convex
#ifdef CGAL_SUPPORT
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/IO/print_wavefront.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/Polyhedron_3.h>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron_3;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef K::Point_3 Point_3;
typedef K::Point_2 Point_2;
namespace PHYSICSMOTION {
void makeConvex(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  std::vector<Point_3> points;
  for(int i=0; i<(int)vss.size(); i++)
    points.push_back(Point_3(vss[i][0],vss[i][1],vss[i][2]));
  vss.clear();
  iss.clear();
  Polyhedron_3 poly;
  CGAL::convex_hull_3(points.begin(),points.end(),poly);
  for(Vertex_iterator v=poly.vertices_begin(); v!=poly.vertices_end(); ++v)
    vss.push_back(Eigen::Matrix<double,3,1>(v->point()[0],v->point()[1],v->point()[2]));
  for(Facet_iterator i=poly.facets_begin(); i!=poly.facets_end(); ++i) {
    Halfedge_facet_circulator j=i->facet_begin();
    //CGAL_assertion(CGAL::circulator_size(j)>=3);
    std::vector<int> facet;
    do {
      facet.push_back(std::distance(poly.vertices_begin(),j->vertex()));
    } while(++j!=i->facet_begin());
    for(auto t=0; t<(int)facet.size()-2; t++)
      iss.push_back(Eigen::Matrix<int,3,1>(facet[t],facet[t+1],facet[t+2]));
  }
}
void makeConvexProject(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss) {
  std::vector<Point_2> points,pointsOut;
  for(int i=0; i<(int)vss.size(); i++)
    points.push_back(Point_2(vss[i][0],vss[i][1]));

  vss.clear();
  CGAL::convex_hull_2(points.begin(),points.end(),std::back_inserter(pointsOut));
  for(int i=0; i<(int)pointsOut.size(); i++)
    vss.push_back(Eigen::Matrix<double,3,1>(pointsOut[i].x(),pointsOut[i].y(),0));
}
}
#else
namespace PHYSICSMOTION {
void makeConvex(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  FUNCTION_NOT_IMPLEMENTED
}
void makeConvexProject(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss) {
  FUNCTION_NOT_IMPLEMENTED
}
}
#endif
