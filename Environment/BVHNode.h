#ifndef BVH_NODE_H
#define BVH_NODE_H

#include "EnvironmentUtils.h"
#include <Utils/Serializable.h>

namespace PHYSICSMOTION {
template <typename T,typename BBOX>
struct Node : public SerializableBase {
  typedef BBOX BoxType;
  using SerializableBase::read;
  using SerializableBase::write;
  using SerializableBase::operator<;
  using SerializableBase::operator=;
  Node();
  bool read(std::istream& is,IOData* dat);
  bool write(std::ostream& os,IOData* dat) const;
  std::shared_ptr<SerializableBase> copy() const;
  virtual std::string type() const override;
  Node<T,BBOX>& operator=(const Node<T,BBOX>& other);
  static void buildBVHTriangleBottomUp(std::vector<Node<T,BBOX>>& bvh,
                                       const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss);
  static void buildBVHVertexBottomUp(std::vector<Node<T,BBOX>>& bvh,
                                     const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss);
  static void buildBVHBottomUp(std::vector<Node<T,BBOX>>& bvh,
                               const std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash>& edgeMap);
  int _l,_r,_parent,_nrCell;
  BBOX _bb;
  T _cell;
};
}

#endif
