#ifndef FEM_SELF_COLLISION_H
#define FEM_SELF_COLLISION_H

#include "FEMMesh.h"
#include "FEMEnergy.h"
#include "Hexahedron.h"
#include "Tetrahedron.h"
#include "FEMGradientInfo.h"
#include <Utils/VTKWriter.h>

namespace PHYSICSMOTION {
template <typename T>
class FEMReducedSystem;
template <typename T>
class FEMSelfCollision {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  FEMSelfCollision(std::shared_ptr<FEMMesh<T>> mesh);
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> getTBVH(const Vec& p,std::vector<Node<int,BBoxExact>>& bvh) const;
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> getTBVHLocal(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::vector<Node<int,BBoxExact>>& bvh) const;
  void detectCollision(const Vec& p,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,const std::string& path) const;
  void detectCollision(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,const std::string& path) const;
  void detectCollision(const Vec& p,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,VTKWriter<double>* os=NULL) const;
  void detectCollision(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,VTKWriter<double>* os=NULL) const;
  void writeDepthVTK(const Vec& p,const std::string& path) const;
  void writeDepthVTK(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,const std::string& path) const;
  void writeCCVTK(VTKWriter<double>& os,const Vec3T& SV,const Mat3X4T& TET) const;
  void writeCCVTK(VTKWriter<double>& os,const Vec3T& SV,const Mat3X4T& TET,const Mat3T& R,const Vec3T& t) const;
 private:
  void buildExcludeSet(std::shared_ptr<FEMMesh<T>> mesh);
  void buildDistance(std::shared_ptr<FEMMesh<T>> mesh);
  std::unordered_map<int,std::unordered_set<int>> _exclude; //tet id -> surface vertex id
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,T> _depth;
  std::vector<std::shared_ptr<Tetrahedron<T>>> _tss;
  std::vector<Node<int,BBoxExact>> _bvh;
};
}

#endif
