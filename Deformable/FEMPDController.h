#ifndef FEM_PD_CONTROLLER_H
#define FEM_PD_CONTROLLER_H

#include "FEMGradientInfo.h"
#include <Utils/SparseUtils.h>
#include <Utils/Pragma.h>

namespace PHYSICSMOTION {
template <typename T>
class FEMPDController {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  //we will add the following energy to the dynamic system:
  //(kP*|p-pTarget|^2+kD*|(p-pN)/dt-dTarget|^2)/2
  FEMPDController();
  FEMPDController(const Vec& pTarget,T kP);
  FEMPDController(const Vec& pTarget,T kP,const Vec& dTarget,T kD);
  const Vec& pTarget() const;
  const Vec& dTarget() const;
  void setPTarget(const Vec& pTarget);
  void setDTarget(const Vec& dTarget);
  virtual void reset();
  virtual void resetP(const Vec& pTarget,T kP);
  virtual void resetD(const Vec& dTarget,T kD);
  virtual void resetPD(const Vec& pTarget,T kP,const Vec& dTarget,T kD);
  virtual void assemble(Vec* G,MatT* H,const FEMGradientInfo<T>& IN,T dt) const;
  virtual void assemble(T* E,Vec* G,SMatT* H,const Vec& pN,const Vec& p,T dt) const;
  virtual MatT DGDu(int rows,T dt) const;
  virtual void debugDGDu(int rows,T dt);
 protected:
  Vec _pTarget,_dTarget;
  T _kP,_kD;
};
template <typename T>
class FEMReducedPDController : public FEMPDController<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  using FEMPDController<T>::_pTarget;
  using FEMPDController<T>::_dTarget;
  using FEMPDController<T>::_kP;
  using FEMPDController<T>::_kD;
  //we will add the following energy to the dynamic system:
  //(kP*|B^T*p-pTarget|^2+kD*|B^T*(p-pN)/dt-dTarget|^2)/2
  FEMReducedPDController();
  FEMReducedPDController(const Vec& pTarget,T kP,const MatT& B);
  FEMReducedPDController(const Vec& pTarget,T kP,const Vec& dTarget,T kD,const MatT& B);
  void reset() override;
  void resetPReduced(const Vec& pTarget,T kP,const MatT& B);
  void resetDReduced(const Vec& dTarget,T kD,const MatT& B);
  void resetPDReduced(const Vec& pTarget,T kP,const Vec& dTarget,T kD,const MatT& B);
  void assemble(Vec* G,MatT* H,const FEMGradientInfo<T>& IN,T dt) const override;
  void assemble(T* E,Vec* G,SMatT* H,const Vec& pN,const Vec& p,T dt) const override;
  MatT DGDu(int rows,T dt) const override;
 protected:
  MatT _B;
};
}

#endif
