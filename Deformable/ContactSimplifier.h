#ifndef CONTACT_SIMPLIFIER_H
#define CONTACT_SIMPLIFIER_H

#include "FEMCollision.h"

namespace PHYSICSMOTION {
class ContactSimplifier {
 public:
  ContactSimplifier(int nV,const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss);
  template <typename T>
  void simplify(std::vector<FEMCollision<T>>& css,int limit) const {
    if((int)css.size()<=limit)
      return;
    //the first contact is the deepest
    for(int i=1; i<(int)css.size(); i++)
      if(css[i].phi()<css[0].phi())
        std::swap(css[i],css[0]);
    //use farthest point sampling
    for(int i=1; i<limit; i++) {
      int metricI=computeMetric(css,i,i);
      for(int j=i+1; j<(int)css.size(); j++) {
        int metricJ=computeMetric(css,i,j);
        if(metricJ>metricI) {
          std::swap(css[i],css[j]);
          std::swap(metricI,metricJ);
        }
      }
    }
    //aggregate other collision to the limited set
    std::vector<FEMCollision<T>> limitSet(css.begin(),css.begin()+limit);
    for(int i=limit; i<(int)css.size(); i++) {
      int minIndex=computeMinIndex(css,limit,i);
      FEMCollision<T> coll=limitSet[minIndex]+css[i];
      if(coll.normal().squaredNorm()>limitSet[minIndex].normal().squaredNorm())
        limitSet[minIndex]=coll;  //aggregation is successful, normal.length() is increasing
      else limitSet.push_back(css[i]);  //aggregation failed, treat this collision separately
    }
    css.swap(limitSet);
  }
 private:
  template <typename T>
  int computeMetric(const std::vector<FEMCollision<T>>& css,int i,int j) const {
    int ret=_distance.rows();
    for(int k=0; k<i; k++)
      ret=std::min(ret,_distance(css[k].SVId(),css[j].SVId()));
    return ret;
  }
  template <typename T>
  int computeMinIndex(const std::vector<FEMCollision<T>>& css,int i,int j) const {
    int minId=-1;
    int ret=_distance.rows();
    for(int k=0; k<i; k++) {
      int dist=_distance(css[k].SVId(),css[j].SVId());
      if(dist<ret) {
        ret=dist;
        minId=k;
      }
    }
    return minId;
  }
  Eigen::Matrix<int,-1,1> shortestDistance(int i,int nV,const std::unordered_map<int,std::unordered_set<int>>& connectivity) const;
  Eigen::Matrix<int,-1,-1> _distance;
};
}

#endif
