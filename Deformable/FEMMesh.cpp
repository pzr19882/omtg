﻿#include "FEMMesh.h"
#include "FEMGradientInfo.h"
#include "Hexahedron.h"
#include "Tetrahedron.h"
#include <stack>
#include <Utils/IO.h>
#include <Utils/Utils.h>
#include <Utils/Interp.h>
#include <Environment/MeshExact.h>
#include <Environment/EnvironmentUtils.h>

namespace PHYSICSMOTION {
//FEMVertex
template <typename T>
FEMVertex<T>::FEMVertex():_pos0(Vec3T::Zero()) {}
template <typename T>
FEMVertex<T>::FEMVertex(int off):_pos0(Vec3T::Zero()) {
  _interp[off]=1;
}
template <typename T>
FEMVertex<T>::FEMVertex(const GRAD_INTERP& interp):_interp(interp),_pos0(Vec3T::Zero()) {}
template <typename T>
bool FEMVertex<T>::read(std::istream& is,IOData*) {
  readBinaryData(_interp,is);
  readBinaryData(_pos0,is);
  return is.good();
}
template <typename T>
bool FEMVertex<T>::write(std::ostream& os,IOData*) const {
  writeBinaryData(_interp,os);
  writeBinaryData(_pos0,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMVertex<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMVertex(_interp));
}
template <typename T>
std::string FEMVertex<T>::type() const {
  return typeid(FEMVertex<T>).name();
}
template <typename T>
typename FEMVertex<T>::Vec3T FEMVertex<T>::operator[](VecCM vss) const {
  Vec3T ret=Vec3T::Zero();
  for(const std::pair<int,T>& term:_interp)
    ret+=vss.template segment<3>(term.first)*term.second;
  return ret;
}
template <typename T>
typename FEMVertex<T>::Vec3T FEMVertex<T>::operator()(VecCM vss) const {
  Vec3T ret=_pos0;
  for(const std::pair<int,T>& term:_interp)
    ret+=vss.template segment<3>(term.first)*term.second;
  return ret;
}
template <typename T>
void FEMVertex<T>::fixDOF(int off,VecCM vss) {
  if(_interp.find(off)!=_interp.end()) {
    _pos0+=_interp[off]*vss.template segment<3>(off);
    _interp.erase(off);
  }
  GRAD_INTERP I;
  for(const std::pair<int,T>& i:_interp)
    if(i.first<off)
      I[i.first]=i.second;
    else if(i.first>off)
      I[i.first-3]=i.second;
  _interp.swap(I);
}
template <typename T>
FEMVertex<T> FEMVertex<T>::operator*(T other) const {
  FEMVertex<T> ret=*this;
  ret*=other;
  return ret;
}
template <typename T>
const FEMVertex<T>& FEMVertex<T>::operator*=(T other) {
  for(const std::pair<int,T>& v:_interp)
    _interp[v.first]*=other;
  _pos0*=other;
  removeZero();
  return *this;
}
template <typename T>
FEMVertex<T> FEMVertex<T>::operator+(const FEMVertex<T>& other) const {
  FEMVertex<T> ret=*this;
  ret+=other;
  return ret;
}
template <typename T>
const FEMVertex<T>& FEMVertex<T>::operator+=(const FEMVertex<T>& other) {
  for(const std::pair<int,T>& v:other._interp)
    if(_interp.find(v.first)==_interp.end())
      _interp[v.first]=v.second;
    else _interp[v.first]+=v.second;
  _pos0+=other._pos0;
  removeZero();
  return *this;
}
template <typename T>
const typename FEMVertex<T>::GRAD_INTERP& FEMVertex<T>::jacobian() const {
  return _interp;
}
template <typename T>
const typename FEMVertex<T>::Vec3T& FEMVertex<T>::pos0() const {
  return _pos0;
}
template <typename T>
int FEMVertex<T>::maxOffset() const {
  int maxOff=0;
  for(const std::pair<int,T>& v:_interp)
    maxOff=std::max(maxOff,v.first+3);
  return maxOff;
}
template <typename T>
bool FEMVertex<T>::isFixed() const {
  return !_pos0.isZero();
}
template <typename T>
void FEMVertex<T>::removeZero() {
  GRAD_INTERP I;
  for(const std::pair<int,T>& v:_interp)
    if(v.second!=0)
      I[v.first]=v.second;
  _interp.swap(I);
}
//FEMCell
template <typename T>
std::vector<std::pair<typename FEMCell<T>::Vec3T,T>> FEMCell<T>::getStencil(int deg) const {
  MatT ret=MatT::Zero(1,1);
  std::vector<std::pair<Vec3T,T>> stencil;
  //put bary
  integrate([&](Vec3T bary,Vec)->MatT {
    stencil.push_back(std::make_pair(bary,0));
    return ret;
  },deg,ret);
  //put coef
  int off=0;
  ret.setZero(stencil.size(),1);
  integrate([&](Vec3T,Vec)->MatT {
    return Vec::Unit(ret.size(),off++);
  },deg,ret);
  //fill data
  for(int i=0; i<ret.size(); i++)
    stencil[i].second=ret(i,0);
  return stencil;
}
//FEMMesh
template <typename T>
FEMMesh<T>::FEMMesh() {}
template <typename T>
FEMMesh<T>::FEMMesh(const std::string& surfaceMesh,T res,bool makeUnit) {
  MeshExact mesh(surfaceMesh);
  if(makeUnit)
    mesh.scale(1/(mesh.getBB().maxCorner()-mesh.getBB().minCorner()).maxCoeff());
  buildVoxel(mesh,res);
  makeUniformSurface();
  buildBVH();
}
template <typename T>
FEMMesh<T>::FEMMesh(bool,const std::string& tetABQ,bool makeUnit) {
  Eigen::Matrix<int,4,1> tet;
  Eigen::Matrix<double,3,1> pos;
  char c;
  int mode=-1,id;
  std::string line,tmp;
  std::ifstream is(tetABQ);
  //vss
  BBoxExact bb;
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss;
  std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>> tets;
  while(true) {
    std::getline(is,line);
    if(beginsWith(line,"*NODE"))
      mode=0;
    else if(beginsWith(line,"*ELEMENT"))
      mode=1;
    else if(beginsWith(line,"*ELSET"))
      break;
    else if(mode==0) {
      std::istringstream(line) >> id >> c >> pos[0] >> c >> pos[1] >> c >> pos[2];
      bb.setUnion(pos.template cast<BBoxExact::T>());
      vss.push_back(pos);
    } else if(mode==1) {
      std::istringstream(line) >> id >> c >> tet[0] >> c >> tet[1] >> c >> tet[2] >> c >> tet[3];
      tets.push_back(tet-Eigen::Matrix<int,4,1>::Ones());
    }
    if(!is.good() || is.eof())
      return;
  }
  if(makeUnit)
    for(Eigen::Matrix<double,3,1>& v:vss)
      v/=(double)(bb.maxCorner()-bb.minCorner()).maxCoeff();
  buildTet(vss,tets);
  makeUniformSurface();
  buildBVH();
}
template <typename T>
FEMMesh<T>::FEMMesh(const std::string& tetMesh,bool makeUnit) {
  Eigen::Matrix<int,4,1> tet;
  Eigen::Matrix<double,3,1> pos;
  int nr;
  std::string line,tmp;
  std::ifstream is(tetMesh);
  //vss
  BBoxExact bb;
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss;
  while(true) {
    std::getline(is,line);
    if(beginsWith(line,"Vertices")) {
      //try reading #vertices in a same line
      nr=0;
      std::istringstream(line) >> tmp >> nr;
      if(nr==0) {
        //otherwise, try reading from the next line
        std::getline(is,line);
        std::istringstream(line) >> nr;
      }
      ASSERT_MSG(nr>0,"Failed reading number of vertices!")
      break;
    }
    if(!is.good() || is.eof())
      return;
  }
  for(int i=0; i<nr; i++) {
    std::getline(is,line);
    std::istringstream iss(line);
    iss >> pos[0] >> pos[1] >> pos[2];
    bb.setUnion(pos.template cast<BBoxExact::T>());
    vss.push_back(pos);
  }
  if(makeUnit)
    for(Eigen::Matrix<double,3,1>& v:vss)
      v/=(double)(bb.maxCorner()-bb.minCorner()).maxCoeff();
  //iss
  std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>> tets;
  while(true) {
    std::getline(is,line);
    if(beginsWith(line,"Tetrahedra")) {
      //try reading #vertices in a same line
      nr=0;
      std::istringstream(line) >> tmp >> nr;
      if(nr==0) {
        //otherwise, try reading from the next line
        std::getline(is,line);
        std::istringstream(line) >> nr;
      } else nr/=4;
      ASSERT_MSG(nr>0,"Failed reading number of elements!")
      break;
    }
    if(!is.good() || is.eof())
      return;
  }
  int minId=std::numeric_limits<int>::max();
  for(int i=0; i<nr; i++) {
    std::getline(is,line);
    std::istringstream iss(line);
    iss >> tet[0] >> tet[1] >> tet[2] >> tet[3];
    minId=std::min(minId,tet.minCoeff());
    tets.push_back(tet);
  }
  //detect whether this is 1-base
  if(minId==1)
    for(Eigen::Matrix<int,4,1>& tet:tets)
      tet-=Eigen::Matrix<int,4,1>::Ones();
  buildTet(vss,tets);
  makeUniformSurface();
  buildBVH();
}
template <typename T>
FEMMesh<T>::FEMMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                    std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,T res) {
  MeshExact mesh(vss,iss);
  buildVoxel(mesh,res);
  makeUniformSurface();
  buildBVH();
}
template <typename T>
FEMMesh<T>::FEMMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                    std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>>& iss) {
  buildTet(vss,iss);
  makeUniformSurface();
  buildBVH();
}
template <typename T>
bool FEMMesh<T>::read(std::istream& is,IOData* dat) {
  registerType<FEMVertex<T>>(dat);
  registerType<Tetrahedron<T>>(dat);
  registerType<Hexahedron<T>>(dat);
  readBinaryData(_vertices,is,dat);
  readBinaryData(_cells,is,dat);
  readBinaryData(_pos0,is,dat);
  readBinaryData(_surfaceVss,is,dat);
  readBinaryData(_surfaceIss,is,dat);
  readBinaryData(_bvh,is,dat);
  return is.good();
}
template <typename T>
bool FEMMesh<T>::write(std::ostream& os,IOData* dat) const {
  registerType<FEMVertex<T>>(dat);
  registerType<Tetrahedron<T>>(dat);
  registerType<Hexahedron<T>>(dat);
  writeBinaryData(_vertices,os,dat);
  writeBinaryData(_cells,os,dat);
  writeBinaryData(_pos0,os,dat);
  writeBinaryData(_surfaceVss,os,dat);
  writeBinaryData(_surfaceIss,os,dat);
  writeBinaryData(_bvh,os,dat);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMMesh<T>::copy() const {
  std::shared_ptr<FEMMesh<T>> mesh(new FEMMesh());
  //volume
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>> vmap;
  mesh->_vertices.resize(_vertices.size());
  for(int i=0; i<(int)_vertices.size(); i++) {
    mesh->_vertices[i]=std::dynamic_pointer_cast<FEMVertex<T>>(_vertices[i]->copy());
    vmap[_vertices[i]]=mesh->_vertices[i];
  }
  mesh->_cells.resize(_cells.size());
  for(int i=0; i<(int)_cells.size(); i++)
    mesh->_cells[i]=_cells[i]->copy(vmap);
  mesh->_pos0=_pos0;
  //surface
  mesh->_surfaceVss.resize(_surfaceVss.size());
  for(int i=0; i<(int)_surfaceVss.size(); i++)
    mesh->_surfaceVss[i]=std::dynamic_pointer_cast<FEMVertex<T>>(_surfaceVss[i]->copy());
  mesh->_surfaceIss=_surfaceIss;
  mesh->_bvh=_bvh;
  return mesh;
}
template <typename T>
std::string FEMMesh<T>::type() const {
  return typeid(FEMMesh<T>).name();
}
template <typename T>
void FEMMesh<T>::getSBVHSubspace(const FEMGradientInfo<T>& I,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans) const {
  bvh=_bvh;
  for(int i=0,j=0; i<nrSV(); i++,j+=3) {
    Vec3T pos=I.getVPos(j); //we need to ensure that surface vertices always appear first
    if(trans)
      pos=ROT(*trans)*pos+CTR(*trans);
    bvh[i]._bb=BBoxExact(pos.template cast<BBoxExact::T>(),pos.template cast<BBoxExact::T>());
  }
  for(int i=nrSV(); i<(int)bvh.size(); i++) {
    bvh[i]._bb=bvh[bvh[i]._l]._bb;
    bvh[i]._bb.setUnion(bvh[bvh[i]._r]._bb);
  }
}
template <typename T>
void FEMMesh<T>::getSBVHSubspaceLocal(const FEMGradientInfo<T>& I,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans) const {
  bvh=_bvh;
  for(int i=0,j=0; i<nrSV(); i++,j+=3) {
    Vec3T pos=I.getVPosLocal(j); //we need to ensure that surface vertices always appear first
    if(trans)
      pos=ROT(*trans)*pos+CTR(*trans);
    bvh[i]._bb=BBoxExact(pos.template cast<BBoxExact::T>(),pos.template cast<BBoxExact::T>());
  }
  for(int i=nrSV(); i<(int)bvh.size(); i++) {
    bvh[i]._bb=bvh[bvh[i]._l]._bb;
    bvh[i]._bb.setUnion(bvh[bvh[i]._r]._bb);
  }
}
template <typename T>
std::vector<typename FEMMesh<T>::Vec3T,Eigen::aligned_allocator<typename FEMMesh<T>::Vec3T>> FEMMesh<T>::getSBVH(const Vec& p,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans) const {
  bvh=_bvh;
  std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>> ret(nrSV());
  for(int i=0; i<nrSV(); i++) {
    ret[i]=getSV(i)->operator()(mapCV(p));
    if(trans)
      ret[i]=ROT(*trans)*ret[i]+CTR(*trans);
    bvh[i]._bb=BBoxExact(ret[i].template cast<BBoxExact::T>(),ret[i].template cast<BBoxExact::T>());
  }
  for(int i=nrSV(); i<(int)bvh.size(); i++) {
    bvh[i]._bb=bvh[bvh[i]._l]._bb;
    bvh[i]._bb.setUnion(bvh[bvh[i]._r]._bb);
  }
  return ret;
}
template <typename T>
const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& FEMMesh<T>::getSC() const {
  return _surfaceIss;
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMMesh<T>::getV(int i) const {
  return _vertices[i];
}
template <typename T>
std::shared_ptr<FEMCell<T>> FEMMesh<T>::getC(int i) const {
  return _cells[i];
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMMesh<T>::getSV(int i) const {
  return _surfaceVss[i];
}
template <typename T>
Eigen::Matrix<int,3,1> FEMMesh<T>::getSC(int i) const {
  return _surfaceIss[i];
}
template <typename T>
typename FEMMesh<T>::SMatT FEMMesh<T>::getMassMatrix() const {
  SMatT ret;
  STrips trips;
  OMP_PARALLEL_FOR_
  for(int i=0; i<nrC(); i++) {
    MatT M=getC(i)->massMatrix();
    for(int r=0; r<M.rows(); r++)
      for(int c=0; c<M.cols(); c++)
        for(const std::pair<int,T>& cR:getC(i)->V(r)->jacobian())
          for(const std::pair<int,T>& cC:getC(i)->V(c)->jacobian())
            addBlock(trips,cR.first,cC.first,M(r,c)*Mat3T::Identity());
  }
  ret.resize(_pos0.size(),_pos0.size());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T>
typename FEMMesh<T>::Vec3T FEMMesh<T>::getCenterOfMass() const {
  SMatT M=getMassMatrix();
  Vec MP=M*_pos0,ret=Vec3T::Zero();
  for(int i=0; i<MP.size(); i+=3)
    ret+=MP.template segment<3>(i);
  ret/=(M.sum()/3);
  return ret;
}
template <typename T>
void FEMMesh<T>::moveToCenterOfMass() {
  ASSERT_MSG(!isFixed(),"Cannot move a fixed mesh to its center of mass!")
  Vec3T COM=getCenterOfMass();
  for(int i=0; i<_pos0.size(); i+=3)
    _pos0.template segment<3>(i)-=COM;
}
template <typename T>
bool FEMMesh<T>::isFixed() const {
  for(std::shared_ptr<FEMVertex<T>> v:_vertices)
    if(v->isFixed())
      return true;
  for(std::shared_ptr<FEMVertex<T>> v:_surfaceVss)
    if(v->isFixed())
      return true;
  return false;
}
template <typename T>
void FEMMesh<T>::fixDOF(int off) {
  std::unordered_set<std::shared_ptr<FEMVertex<T>>> vss;
  for(std::shared_ptr<FEMVertex<T>> v:_vertices)
    if(vss.find(v)==vss.end()) {
      v->fixDOF(off,pos0());
      vss.insert(v);
    }
  for(std::shared_ptr<FEMVertex<T>> v:_surfaceVss)
    if(vss.find(v)==vss.end()) {
      v->fixDOF(off,pos0());
      vss.insert(v);
    }
  Vec pos0=Vec::Zero(_pos0.size()-3);
  for(int i=0; i<_pos0.size(); i+=3)
    if(i<off)
      pos0.template segment<3>(i)=_pos0.template segment<3>(i);
    else if(i>off)
      pos0.template segment<3>(i-3)=_pos0.template segment<3>(i);
  _pos0=pos0;
}
template <typename T>
void FEMMesh<T>::fixDOF(const FixDOFCallback& func) {
  fixDOF([&](const Vec3T& pos) {
    return func.fix((double)pos[0],(double)pos[1],(double)pos[2]);
  });
}
template <typename T>
void FEMMesh<T>::fixDOF(std::function<bool(const Vec3T& pos)> func) {
  bool more=true;
  while(more) {
    more=false;
    for(int i=0; i<_pos0.size(); i+=3)
      if(func(_pos0.template segment<3>(i))) {
        //std::cout << "Fixing DOF " << i << std::endl;
        fixDOF(i);
        more=true;
        break;
      }
  }
}
template <typename T>
typename FEMMesh<T>::VecCM FEMMesh<T>::pos0() const {
  return mapCV(_pos0);
}
template <typename T>
typename FEMMesh<T>::Vec FEMMesh<T>::restPos() const {
  return _pos0;
}
template <typename T>
int FEMMesh<T>::nrDOF() const {
  int maxOff=0;
  for(int i=0; i<nrV(); i++)
    maxOff=std::max(maxOff,getV(i)->maxOffset());
  return maxOff;
}
template <typename T>
int FEMMesh<T>::nrV() const {
  return (int)_vertices.size();
}
template <typename T>
int FEMMesh<T>::nrC() const {
  return (int)_cells.size();
}
template <typename T>
int FEMMesh<T>::nrSV() const {
  return (int)_surfaceVss.size();
}
template <typename T>
int FEMMesh<T>::nrSC() const {
  return (int)_surfaceIss.size();
}
//helper
template <typename T>
void FEMMesh<T>::buildVoxel(const MeshExact& mesh,T res) {
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vssM;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> issM;
  mesh.getMesh(vssM,issM);
  //identify boundary
  std::cout << "Filling interface hex!" << std::endl;
  BBoxExact bbAll;
  _surfaceIss=issM;
  std::vector<Eigen::Matrix<int,3,1>> vertexBBId(vssM.size());
  std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> css,visited;
  for(int i=0; i<(int)issM.size(); i++) {
    TriangleExact tri(vssM[issM[i][0]].template cast<TriangleExact::T>(),
                      vssM[issM[i][1]].template cast<TriangleExact::T>(),
                      vssM[issM[i][2]].template cast<TriangleExact::T>());
    BBoxExact bb=tri.getBB(),bbId;
    //test every cell in the bounding box
    Eigen::Matrix<int,3,1> minC=(bb.minCorner().template cast<T>()/res).array().floor().matrix().template cast<int>();
    Eigen::Matrix<int,3,1> maxC=(bb.maxCorner().template cast<T>()/res+Vec3T::Ones()).array().floor().matrix().template cast<int>();
    for(int x=minC[0]; x<=maxC[0]; x++)
      for(int y=minC[1]; y<=maxC[1]; y++)
        for(int z=minC[2]; z<=maxC[2]; z++) {
          Eigen::Matrix<int,3,1> id(x,y,z);
          if(css.find(id)!=css.end())
            continue;
          bbId=BBoxExact(id.cast<BBoxExact::T>()*BBoxExact::T(res),(id+Eigen::Matrix<int,3,1>::Ones()).cast<BBoxExact::T>()*BBoxExact::T(res));
          if(tri.intersect(bbId)) {
            bbAll.setUnion(id.template cast<BBoxExact::T>());
            css.insert(id);
          }
        }
    //fill in vertexBBId
    for(int d=0; d<3; d++) {
      Eigen::Matrix<int,3,1> id=(tri.v(d).template cast<T>()/res).array().floor().matrix().template cast<int>();
      ASSERT((id.array()>=minC.array()).all() && (id.array()<=maxC.array()).all())
      vertexBBId[issM[i][d]]=id;
    }
  }
  //flood fill
  std::cout << "Flood filling internal hex!" << std::endl;
  visited=css;
  for(int x=bbAll.minCorner()[0]; x<=bbAll.maxCorner()[0]; x++)
    for(int y=bbAll.minCorner()[1]; y<=bbAll.maxCorner()[1]; y++)
      for(int z=bbAll.minCorner()[2]; z<=bbAll.maxCorner()[2]; z++) {
        Eigen::Matrix<int,3,1> id(x,y,z);
        if(visited.find(id)==visited.end()) {
          std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> newRegion;
          std::stack<Eigen::Matrix<int,3,1>> ss;
          ss.push(id);
          visited.insert(id);
          newRegion.insert(id);
          //popagate
          bool isValid=true;
          while(!ss.empty()) {
            id=ss.top();
            ss.pop();
            if(!bbAll.contain(id.template cast<BBoxExact::T>()))
              isValid=false;
            else {
              for(int d=0; d<3; d++)
                for(int off=-1; off<=1; off+=2) {
                  Eigen::Matrix<int,3,1> id2=id+Eigen::Matrix<int,3,1>::Unit(d)*off;
                  if(visited.find(id2)==visited.end()) {
                    visited.insert(id2);
                    newRegion.insert(id2);
                    ss.push(id2);
                  }
                }
            }
          }
          if(isValid)
            css.insert(newRegion.begin(),newRegion.end());
        }
      }
  //assemble
  std::unordered_map<Eigen::Matrix<int,3,1>,std::shared_ptr<FEMVertex<T>>,TriangleHash> vss;
  buildMesh(css,vss,res);
  //surfaceVss
  T coef[8];
  _surfaceVss.clear();
  for(int i=0; i<(int)vssM.size(); i++) {
    const Eigen::Matrix<int,3,1>& id=vertexBBId[i];
    Vec3T frac=vssM[i].template cast<T>()/res-id.template cast<T>();
    stencil3D(coef,frac[0],frac[1],frac[2]);
    std::shared_ptr<FEMVertex<T>> v(new FEMVertex<T>);
    for(int z=0,off=0; z<=1; z++)
      for(int y=0; y<=1; y++)
        for(int x=0; x<=1; x++)
          *v+=*(vss.find(id+Eigen::Matrix<int,3,1>(x,y,z))->second)*coef[off++];
    _surfaceVss.push_back(v);
  }
}
template <typename T>
void FEMMesh<T>::buildMesh(const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>& css,
                           std::unordered_map<Eigen::Matrix<int,3,1>,std::shared_ptr<FEMVertex<T>>,TriangleHash>& vss,T res) {
  _vertices.clear();
  std::vector<T> pos0;
  for(const Eigen::Matrix<int,3,1>& c:css) {
    std::shared_ptr<FEMVertex<T>> vssHex[8];
    for(int z=0,off=0; z<=1; z++)
      for(int y=0; y<=1; y++)
        for(int x=0; x<=1; x++) {
          Eigen::Matrix<int,3,1> cCorner=c+Eigen::Matrix<int,3,1>(x,y,z);
          if(vss.find(cCorner)==vss.end()) {
            vss[cCorner]=std::shared_ptr<FEMVertex<T>>(new FEMVertex<T>(((int)_vertices.size())*3));
            for(int d=0; d<3; d++)
              pos0.push_back(cCorner[d]*res);
            _vertices.push_back(vss[cCorner]);
          }
          vssHex[off++]=vss[cCorner];
        }
    _cells.push_back(std::shared_ptr<FEMCell<T>>(new Hexahedron<T>(res,vssHex)));
  }
  _pos0=Eigen::Map<Vec>(pos0.data(),pos0.size());
}
template <typename T>
void FEMMesh<T>::buildTet(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                          std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>>& iss) {
  std::cout << "Constructing tet!" << std::endl;
  _vertices.clear();
  _pos0.resize(vss.size()*3);
  for(int i=0; i<(int)vss.size(); i++) {
    _vertices.push_back(std::shared_ptr<FEMVertex<T>>(new FEMVertex<T>(i*3)));
    _pos0.template segment<3>(i*3)=vss[i].template cast<T>();
  }
  _cells.clear();
  for(int i=0; i<(int)iss.size(); i++) {
    Mat3X4T v;
    std::shared_ptr<FEMVertex<T>> V[4];
    for(int d=0; d<4; d++) {
      v.col(d)=vss[iss[i][d]].template cast<T>();
      V[d]=_vertices[iss[i][d]];
    }
    _cells.push_back(std::shared_ptr<FEMCell<T>>(new Tetrahedron<T>(v,V)));
  }
  //find surface
  std::cout << "Finding surface triangles!" << std::endl;
  std::unordered_map<Eigen::Matrix<int,3,1>,Eigen::Matrix<int,2,1>,TriangleHash> faceMap;
  for(int i=0; i<(int)iss.size(); i++) {
    const Eigen::Matrix<int,4,1>& tet=iss[i];
    for(int d=0; d<4; d++) {
      Eigen::Matrix<int,3,1> face(tet[(d+1)%4],tet[(d+2)%4],tet[(d+3)%4]);
      sort3(face[0],face[1],face[2]);
      if(faceMap.find(face)==faceMap.end())
        faceMap[face]=Eigen::Matrix<int,2,1>(i,-1);
      else {
        ASSERT_MSG(faceMap[face][1]==-1,"Non-manifold mesh detected!")
        faceMap[face][1]=i;
      }
    }
  }
  //vss
  int nrFace=0;
  _surfaceVss.clear();
  std::unordered_map<int,int> vssMap;
  for(const auto& p:faceMap)
    if(p.second[1]==-1) {
      for(int d=0; d<3; d++)
        if(vssMap.find(p.first[d])==vssMap.end()) {
          vssMap[p.first[d]]=(int)_surfaceVss.size();
          _surfaceVss.push_back(_vertices[p.first[d]]);
        }
      nrFace++;
    }
  //iss
  _surfaceIss.resize(nrFace);
  nrFace=0;
  for(const auto& p:faceMap)
    if(p.second[1]==-1) {
      for(int d=0; d<3; d++)
        _surfaceIss[nrFace][d]=vssMap[p.first[d]];
      nrFace++;
    }
}
template <typename T>
void FEMMesh<T>::buildBVH() {
  _bvh.assign(_surfaceVss.size(),Node<int,BBoxExact>());
  for(int i=0; i<(int)_bvh.size(); i++) {
    Node<int,BBoxExact>& n=_bvh[i];
    Vec3T cv=_surfaceVss[i]->operator()(mapCV(_pos0));
    n._bb=BBoxExact(cv.template cast<BBoxExact::T>(),cv.template cast<BBoxExact::T>());
    n._nrCell=1;
    n._cell=i;
  }
  Node<int,BBoxExact>::buildBVHVertexBottomUp(_bvh,_surfaceIss);
}
template <typename T>
void FEMMesh<T>::makeUniformSurface() {
  makeUniform(_surfaceIss);
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss;
  for(int i=0; i<(int)_surfaceVss.size(); i++)
    vss.push_back(_surfaceVss[i]->operator()(mapCV(_pos0)).template cast<double>());
  if(volume(vss,_surfaceIss)<0)
    makeInsideOut(_surfaceIss);
}
template struct FEMVertex<FLOAT>;
template struct FEMCell<FLOAT>;
template struct FEMMesh<FLOAT>;
}
