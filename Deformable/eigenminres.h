#ifndef __EIGEN_MINRES_H
#define __EIGEN_MINRES_H

#include <Eigen/Core>
#include <unsupported/Eigen/IterativeSolvers>

class MatrixReplacement;
using Eigen::SparseMatrix;

namespace Eigen {
namespace internal {
// MatrixReplacement looks-like a SparseMatrix, so let's inherits its traits:
template<>
struct traits<MatrixReplacement> : public Eigen::internal::traits<Eigen::SparseMatrix<double> > {
};
}
}

class MatrixReplacement : public Eigen::EigenBase<MatrixReplacement> {
 public:
  // Required typedefs, constants, and method:
  typedef double Scalar;
  typedef double RealScalar;
  typedef int StorageIndex;
  enum {
    ColsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    IsRowMajor = false
  };
  Index rows() const {
    return _stiffness.rows();
  }
  Index cols() const {
    return _stiffness.cols();
  }
  template<typename Rhs>
  Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct> operator*(const Eigen::MatrixBase<Rhs>& x) const {
    return Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct>(*this, x.derived());
  }
  // Custom API:
  MatrixReplacement(const Eigen::SparseMatrix<double>& stiffness,const Eigen::SparseMatrix<double>& mass):_stiffness(stiffness),_mass(mass) {}
  void attachEigenvector(double eval,const Eigen::Matrix<double,-1,1>& evec) {
    _eval=eval;
    _evec=evec;
  }
  Eigen::Matrix<double,-1,1> mult(const Eigen::Matrix<double,-1,1>& rhs) const {
    Eigen::Matrix<double,-1,1> ret;
    ret=_stiffness*rhs;
    ret-=_mass*rhs*_eval;
    ret+=_evec*rhs.dot(_evec);
    return ret;
  }
 private:
  Eigen::SparseMatrix<double> _stiffness,_mass;
  Eigen::Matrix<double,-1,1> _evec;
  double _eval;
};

namespace Eigen {
namespace internal {
template<typename Rhs>
struct generic_product_impl<MatrixReplacement, Rhs, SparseShape, DenseShape, GemvProduct> // GEMV stands for matrix-vector
  : generic_product_impl_base<MatrixReplacement,Rhs,generic_product_impl<MatrixReplacement,Rhs> > {
  typedef typename Product<MatrixReplacement,Rhs>::Scalar Scalar;

  template<typename Dest>
  static void scaleAndAddTo(Dest& dst, const MatrixReplacement& lhs, const Rhs& rhs, const Scalar& alpha) {
    // This method should implement "dst += alpha * lhs * rhs" inplace
    dst.noalias() += lhs.mult(rhs)*alpha;
  }
};
}
}

#endif
