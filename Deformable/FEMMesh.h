#ifndef FEM_MESH_H
#define FEM_MESH_H

#include <Environment/BBoxExact.h>
#include <Utils/SOSPolynomial.h>
#include <Utils/SparseUtils.h>
#include <unordered_set>

namespace PHYSICSMOTION {
template <typename T>
class FEMOctreeMesh;
template <typename T>
struct FEMGradientInfo;
struct MeshExact;
struct FixDOFCallback {
 public:
  virtual ~FixDOFCallback() {}
  virtual bool fix(double x,double y,double z) const {
    return false;
  }
};
template <typename T>
struct FEMVertex : public SerializableBase {
  DECL_MAT_VEC_MAP_TYPES_T
  typedef std::unordered_map<int,T> GRAD_INTERP;
  FEMVertex();
  FEMVertex(int off);
  FEMVertex(const GRAD_INTERP& interp);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
#ifndef SWIG
  Vec3T operator[](VecCM d) const;  //derivative along d
  Vec3T operator()(VecCM p) const;  //position recovered from p
  void fixDOF(int off,VecCM vss);
#endif
  FEMVertex<T> operator*(T other) const;
  const FEMVertex<T>& operator*=(T other);
  FEMVertex<T> operator+(const FEMVertex<T>& other) const;
  const FEMVertex<T>& operator+=(const FEMVertex<T>& other);
  const GRAD_INTERP& jacobian() const;
  const Vec3T& pos0() const;
  int maxOffset() const;
  bool isFixed() const;
 private:
  void removeZero();
  GRAD_INTERP _interp;
  Vec3T _pos0;
};
template <typename T>
struct FEMCell : public SerializableBase {
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef SOSPolynomial<T,'x'> PolyXT;
  virtual std::shared_ptr<FEMCell<T>> copy(const std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>>& vmap) const=0;
  virtual std::shared_ptr<FEMCell<T>> toReducedCell(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper) const=0;
  virtual std::shared_ptr<FEMVertex<T>> V(int i) const=0;
#ifndef SWIG
  virtual void calcPointDist(VecCM vss,const Vec3T& pt,T& sqrDistance,Vec3T& cp,Vec3T& b) const=0;
  virtual Vec3T bary(VecCM vss,const Vec3T& pt,bool proj=false) const=0;
  virtual bool isInside(VecCM vss,const Vec3T& pt) const=0;
  virtual Vec3T operator()(VecCM vss,const Vec3T& b) const=0;
  virtual Mat3T F(VecCM vss,const Vec3T& b) const=0;
  virtual BBoxExact getBB(VecCM vss) const=0;
#endif
  virtual T volume0() const=0;
  virtual MatT massMatrix() const=0;
  virtual SMatT getDFDV(const Vec3T&) const=0;
  virtual void integrate(std::function<MatT(Vec3T,Vec)> func,int deg,MatT& ret) const=0;
  virtual std::vector<std::pair<Vec3T,T>> stencilIntegrateQuadratic() const=0;
  virtual std::vector<std::pair<Vec3T,T>> stencilIntegrateQuartic() const=0;
  std::vector<std::pair<Vec3T,T>> getStencil(int deg) const;
};
template <typename T>
struct FEMMesh : public SerializableBase {
  friend class FEMOctreeMesh<T>;
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  FEMMesh();
  FEMMesh(const std::string& surfaceMesh,T res,bool makeUnit);
  FEMMesh(bool verbose,const std::string& tetABQ,bool makeUnit);
  FEMMesh(const std::string& tetMesh,bool makeUnit);
  FEMMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
          std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,T res); //initialization by surface mesh
  FEMMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
          std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>>& iss); //initialization by tetrahedron mesh
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
#ifndef SWIG
  void getSBVHSubspace(const FEMGradientInfo<T>& I,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans=NULL) const;
  void getSBVHSubspaceLocal(const FEMGradientInfo<T>& I,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans=NULL) const;
  std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>> getSBVH(const Vec& p,std::vector<Node<int,BBoxExact>>& bvh,const Mat3X4T* trans=NULL) const;
  const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& getSC() const;
#endif
  std::shared_ptr<FEMVertex<T>> getV(int i) const;
  std::shared_ptr<FEMCell<T>> getC(int i) const;
  std::shared_ptr<FEMVertex<T>> getSV(int i) const;
  Eigen::Matrix<int,3,1> getSC(int i) const;
  SMatT getMassMatrix() const;
  Vec3T getCenterOfMass() const;
  void moveToCenterOfMass();
  bool isFixed() const;
  void fixDOF(int off);
  void fixDOF(const FixDOFCallback& func);
  void fixDOF(std::function<bool(const Vec3T& pos)> func);
#ifndef SWIG
  VecCM pos0() const;
#endif
  Vec restPos() const;
  int nrDOF() const;
  int nrV() const;
  int nrC() const;
  int nrSV() const;
  int nrSC() const;
 private:
  void buildVoxel(const MeshExact& mesh,T res);
  void buildMesh(const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>& css,
                 std::unordered_map<Eigen::Matrix<int,3,1>,std::shared_ptr<FEMVertex<T>>,TriangleHash>& vss,T res);
  void buildTet(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                std::vector<Eigen::Matrix<int,4,1>,Eigen::aligned_allocator<Eigen::Matrix<int,4,1>>>& iss);
  void buildBVH();
  void makeUniformSurface();
  //volume
  std::vector<std::shared_ptr<FEMVertex<T>>> _vertices;
  std::vector<std::shared_ptr<FEMCell<T>>> _cells;
  Vec _pos0;
  //surface
  std::vector<std::shared_ptr<FEMVertex<T>>> _surfaceVss;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> _surfaceIss;
  std::vector<Node<int,BBoxExact>> _bvh;
};
}

#endif
