#ifndef FEM_OCTREE_MESH_H
#define FEM_OCTREE_MESH_H

#include "FEMMesh.h"
#include <Environment/EnvironmentUtils.h>
#include <Utils/SparseUtils.h>
#include <unordered_set>

namespace PHYSICSMOTION {
template <typename T>
class FEMOctreeMesh {
 public:
  struct OctreeLevel {
    std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> _protected;
    std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> _css;
    int _cellSz;
  };
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  FEMOctreeMesh(const FEMMesh<T>& body,int protectedLayers=1,int searchRange=8,int maxLv=1000);
  int build(int protectedLayers,const Eigen::Matrix<int,3,1>& shift,int maxLv);
  OctreeLevel coarsen(OctreeLevel& lv) const;
  std::shared_ptr<FEMMesh<T>> getMesh();
  void buildProtected(OctreeLevel& lv,int protectedLayers) const;
  std::shared_ptr<FEMVertex<T>> getVert(const FEMVertex<T>& v) const;
  std::shared_ptr<FEMVertex<T>> getVert(const Eigen::Matrix<int,3,1>& c);
 protected:
  OctreeLevel _base;
  std::unordered_map<Eigen::Matrix<int,3,1>,std::shared_ptr<FEMVertex<T>>,TriangleHash> _vssOut;
  std::unordered_map<Eigen::Matrix<int,3,1>,std::shared_ptr<FEMVertex<T>>,TriangleHash> _vss;
  std::vector<OctreeLevel> _lss;
  Eigen::Matrix<int,3,1> _shift;
  const FEMMesh<T>& _body;
  std::vector<T> _pos0Out;
  Vec3T _d;
};
}

#endif
