#ifndef FEM_NONINERTIAL_FRAME_H
#define FEM_NONINERTIAL_FRAME_H

#include "FEMEnergy.h"
#include <Utils/SparseUtils.h>
#include <Utils/RotationUtils.h>
#include <Utils/CrossSpatialUtils.h>

namespace PHYSICSMOTION {
template <typename T>
class FEMNoninertialFrame {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  FEMNoninertialFrame(const MatT& B,const Vec& pos0,const SMatT& mass);
  T LagrangianRef(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  T Lagrangian(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  Vec DLDdu(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  Vec3T DLDv(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  Vec3T DLDw(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  Vec DLDu(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const;
  T DLDt(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const;
  Vec DDLDduDt(const Vec3T&,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const;
  Vec DDLDvDt(const Vec3T&,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const;
  Vec DDLDwDt(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const;
  void assembleSystem(const Vec3T* v,const Vec3T* w,const Vec& u,const Vec& DuDt,MatT& GMass,Vec& GForce) const;
  void assembleSystem(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,MatT& GMass,Vec& GForce) const;
  Vec assembleSystemRef(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const;
  void debug() const;
 private:
  Mat3T I(const Vec& u) const;
  Mat3T DIDt(const Vec& u,const Vec& DuDt) const;
  Vec DIDu(const Vec& u,const Vec& w) const;
  Mat3T Ip(const Vec& u) const;
  Mat3T DIpDt(const Vec& DuDt) const;
  Vec DIpDu(const Vec& w,const Vec& v) const;
  Mat3XT Ipp(const Vec& u) const;
  Mat3XT DIppDt(const Vec& du) const;
  Vec DIppDu(const Vec& w,const Vec& DuDt) const;
  SMatT crossDiag(const Vec& v) const;
  const MatT& _B;
  const Vec& _pos0;
  const SMatT& _mass;
  Mat3XT _IMB;
  MatT _BTMB;
  Mat3T _m;
  Eigen::Matrix<MatT,3,3> _Iu;
  Eigen::Matrix<Vec,3,3> _Ipu;
  Eigen::Matrix<Vec,3,-1> _Ippu;
};
template <typename T>
class FEMLinearFST {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  FEMLinearFST(const MatT& B,const Vec& pos0);
  FEMLinearFST(const MatT& B,const Vec& pos0,const Vec& f);
  void addEnergy(int off,const Vec3T& fBlk);
  void addEnergy(std::shared_ptr<FEMEnergy<T>> e);
  T ERef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec3T DEDtvRef(const Mat3T& R) const;
  Vec3T DEDtwRef(const Mat3T& R,const Vec& u) const;
  Vec DEDtuRef(const Mat3T& R) const;
  Vec3T DEDtv(const Mat3T& R) const;
  Vec3T DEDtw(const Vec& u,const Mat3T& R) const;
  Vec DEDtu(const Mat3T& R) const;
  Vec assembleSystem(const Vec& u,const Mat3T* R) const;
  Vec assembleSystem(const Vec& u,const Mat3T& R) const;
  void debug() const;
 private:
  Vec3T DEDv(const Mat3T& R,const Vec3T& fBlk) const;
  Mat3XT DEDw(const Mat3T& R,const MatT& B,const Vec& pos0,int off,const Vec3T& fBlk);
  Vec DEDu(const Mat3T& R,const MatT& B,int off,const Vec3T& fBlk);
  const MatT& _B;
  const Vec& _pos0;
  Eigen::Matrix<Vec3T,3,3> _Ev;
  Eigen::Matrix<Mat3XT,3,3> _Ew;
  Eigen::Matrix<Vec,3,3> _Eu;
  Vec _f;
};
template <typename T>
class FEMQuadraticFST {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  FEMQuadraticFST(const MatT& B,const Vec& pos0);
  FEMQuadraticFST(const MatT& B,const Vec& pos0,const SMatT& H);
  void addEnergy(int offR,int offC,const Mat3T& HBlk);
  void addEnergy(std::shared_ptr<FEMEnergy<T>> e);
  T ERef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec3T DEDtvRef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec3T DEDtwRef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec DEDtuRef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  MatT DDEDDtRef(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec3T DEDtv(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec3T DEDtw(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  Vec DEDtu(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  MatT DDEDDt(const Vec3T& t,const Mat3T& R,const Vec& u) const;
  void assembleSystem(const Vec3T* t,const Mat3T* R,const Vec& u,MatT& GMass,Vec& GForce,T dt) const;
  void assembleSystem(const Vec3T& t,const Mat3T& R,const Vec& u,MatT& GMass,Vec& GForce,T dt) const;
  void debug() const;
 private:
  Mat3XT IRHB(const Mat3T& R) const;
  Vec3T pRH1(const Mat3T& R,const Vec& u) const;
  Vec BRHp(const Mat3T& R,const Vec& u) const;
  T pRHp(const Mat3T& R,const Vec& u) const;
  const MatT& _B;
  const Vec& _pos0;
  SMatT _H;
  Mat3T _IHI;
  MatT _BTHB;
  Mat3XT _IHB;
  Eigen::Matrix<Mat3XT,3,3> _IRHB;
  Eigen::Matrix<MatX3T,3,3> _pRH1;
  Eigen::Matrix<MatT,3,3> _pRHp;
};
}

#endif
