#ifndef FEM_COLLISION_H
#define FEM_COLLISION_H

#include "FEMMesh.h"
#include "Tetrahedron.h"
#include <functional>

namespace PHYSICSMOTION {
template <typename T>
struct FEMCollision {
  DECL_MAT_VEC_MAP_TYPES_T
  struct CollisionInfo {
    Vec _pos,_n,_t1,_t2;
    int _DOFId;
    T _phi,_coef;
  };
  FEMCollision();
  FEMCollision(int id,const Mat3XT& jac,const Vec3T& pos,const Vec3T& n,T phi);
  FEMCollision(std::shared_ptr<FEMVertex<T>> V,int id,const Vec3T& pos,const Vec3T& n,T phi);
  FEMCollision<T> operator+(const FEMCollision<T>& coll) const;
  FEMCollision<T>& operator+=(const FEMCollision<T>& coll);
  void visit(std::function<void(CollisionInfo&)> func) const;
  void buildFrame();
  const Vec& normal() const;
  const Vec& pos() const;
  int SVId() const;
  T phi() const;
 private:
  std::unordered_map<int,T> _jacobianFullspace;
  Mat3XT _jacobianSubspace;
  Vec _pos,_n,_t1,_t2;
  int _id,_nColl;
  T _phi;
};
}

#endif
