﻿#include "FEMCollision.h"

namespace PHYSICSMOTION {
template <typename T>
FEMCollision<T>::FEMCollision() {}
template <typename T>
FEMCollision<T>::FEMCollision(int id,const Mat3XT& jac,const Vec3T& pos,const Vec3T& n,T phi) {
  //mask out jacobian fullspace
  _jacobianFullspace[0]=1;
  //setup jacobian subspace
  _jacobianSubspace=jac;
  //setup collision
  _pos=pos;
  _n=n;
  _id=id;
  _nColl=1;
  _phi=phi;
}
template <typename T>
FEMCollision<T>::FEMCollision(std::shared_ptr<FEMVertex<T>> V,int id,const Vec3T& pos,const Vec3T& n,T phi) {
  //setup jacobian fullspace
  _jacobianFullspace=V->jacobian();
  //mask out jacobian subspace
  _jacobianSubspace.setZero(3,0);
  //setup collision
  _pos=pos;
  _n=n;
  _id=id;
  _nColl=1;
  _phi=phi;
}
template <typename T>
FEMCollision<T> FEMCollision<T>::operator+(const FEMCollision<T>& coll) const {
  FEMCollision<T> ret=*this;
  return ret+=coll;
}
template <typename T>
FEMCollision<T>& FEMCollision<T>::operator+=(const FEMCollision<T>& coll) {
  //sum up jacobian fullspace
  if(!_jacobianFullspace.empty()) {
    for(const std::pair<int,T>& v:coll._jacobianFullspace)
      if(_jacobianFullspace.find(v.first)==_jacobianFullspace.end())
        _jacobianFullspace[v.first]=v.second;
      else _jacobianFullspace[v.first]+=v.second;
  }
  //sum up jacobian subspace
  if(_jacobianSubspace.size()>0)
    _jacobianSubspace+=coll._jacobianSubspace;
  //add position
  _pos+=coll._pos;
  _n+=coll._n;
  _phi+=coll._phi;
  _nColl++;
  _id=-1; //this indicates a compound collision that is not associated with a certain SVId
  return *this;
}
template <typename T>
void FEMCollision<T>::visit(std::function<void(CollisionInfo&)> func) const {
  CollisionInfo info;
  info._pos=_pos;
  info._n=_n;
  info._t1=_t1;
  info._t2=_t2;
  info._phi=_phi;
  //iterate jacobian fullspace
  if(!_jacobianFullspace.empty())
    for(const std::pair<int,T>& v:_jacobianFullspace) {
      info._DOFId=v.first;
      info._coef=v.second;
      func(info);
    }
  //iterate jacobian subspace
  if(_jacobianSubspace.size()>0)
    func(info);
}
template <typename T>
void FEMCollision<T>::buildFrame() {
  //average collision and depth
  _pos/=_nColl;
  _phi/=_nColl;
  //build frame
  int id;
  _n.normalize();
  _n.cwiseAbs().minCoeff(&id);
  _t1=Vec3T::Unit(id).cross(Vec3T(_n)).normalized();
  _t2=Vec3T(_n).cross(Vec3T(_t1));
  //apply jacobian subspace
  if(_jacobianSubspace.size()>0) {
    _n=_jacobianSubspace.transpose()*_n;
    _t1=_jacobianSubspace.transpose()*_t1;
    _t2=_jacobianSubspace.transpose()*_t2;
  }
}
template <typename T>
const typename FEMCollision<T>::Vec& FEMCollision<T>::normal() const {
  return _n;
}
template <typename T>
const typename FEMCollision<T>::Vec& FEMCollision<T>::pos() const {
  return _pos;
}
template <typename T>
int FEMCollision<T>::SVId() const {
  return _id;
}
template <typename T>
T FEMCollision<T>::phi() const {
  return _phi;
}
template class FEMCollision<FLOAT>;
}
