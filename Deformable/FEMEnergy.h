#ifndef FEM_ENERGY_H
#define FEM_ENERGY_H

#include "FEMMesh.h"
#include "Tetrahedron.h"
#include "Hexahedron.h"
#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
template <typename T>
struct FEMEnergyContext {
  DECL_MAT_VEC_MAP_TYPES_T
  //FEMFluidEnergy
  Vec3T _vN[3],_vNN[3],_vRelN[3],_n,_nUnit;
  T _A,_dt;
};
template <typename T>
struct FEMEnergy : public SerializableBase {
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef std::function<void(T)> EFunc;
  typedef std::function<void(int,const Vec3T&)> GFunc;
  typedef std::function<void(int,int,const Mat3T&)> HFunc;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const=0;
  virtual bool eval(const Vec& x,ParallelMatrix<T>* E,ParallelMatrix<Vec>* G,ParallelVector<Eigen::Triplet<T,int>>* H,const FEMEnergyContext<T>* ctx=NULL) const;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const=0;
  virtual int polyOrder() const;
};
template <typename T>
struct FEMPointConstraintEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMPointConstraintEnergy();
  FEMPointConstraintEnergy(const Vec3T& pos,T coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
  void add(std::shared_ptr<FEMVertex<T>> vert,T coef);
 private:
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,T> _vert;
  Vec3T _pos;
  T _coef;
};
template <typename T>
struct FEMConstantForceEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMConstantForceEnergy();
  FEMConstantForceEnergy(std::shared_ptr<FEMCell<T>> cell,const Vec3T& f);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
 private:
  std::shared_ptr<FEMCell<T>> _cell;
  Vec _f;
};
template <typename T>
struct FEMLinearElasticEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMLinearElasticEnergy();
  FEMLinearElasticEnergy(std::shared_ptr<FEMCell<T>> cell,VecCM pos0,T lambda,T mu);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
  static Mat9T computeH(T lambda,T mu);
 private:
  std::shared_ptr<FEMCell<T>> _cell;
  Vec _pos0;
  MatT _H;
};
template <typename T>
struct FEMStVKElasticEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMStVKElasticEnergy();
  FEMStVKElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
  std::shared_ptr<FEMCell<T>> getCell() const;
  void DHDU(const Vec& x,const Vec& dir,ParallelVector<Eigen::Triplet<T,int>>& H) const;
  const std::vector<std::pair<Vec3T,T>>& coefs() const;
  T lambda() const;
  T mu() const;
 protected:
  std::shared_ptr<FEMCell<T>> _cell;
  std::vector<std::pair<Vec3T,T>> _coefs;
  T _lambda,_mu;
  int _nR;
};
template <typename T>
struct FEMCorotatedElasticEnergy : public FEMStVKElasticEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
  using FEMStVKElasticEnergy<T>::_cell;
  using FEMStVKElasticEnergy<T>::_coefs;
  using FEMStVKElasticEnergy<T>::_lambda;
  using FEMStVKElasticEnergy<T>::_mu;
  using FEMStVKElasticEnergy<T>::_nR;
#endif
  FEMCorotatedElasticEnergy();
  FEMCorotatedElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
 private:
  void computeR(const Mat3T& F,Mat3T& R) const;
  void computeRFast(const Mat3T& F,Mat3T& R) const;
  std::vector<MatT,Eigen::aligned_allocator<MatT>> _H;
};
template <typename T>
struct FEMNonHookeanElasticEnergy : public FEMStVKElasticEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
  using FEMStVKElasticEnergy<T>::_cell;
  using FEMStVKElasticEnergy<T>::_coefs;
  using FEMStVKElasticEnergy<T>::_lambda;
  using FEMStVKElasticEnergy<T>::_mu;
  using FEMStVKElasticEnergy<T>::_nR;
#endif
  FEMNonHookeanElasticEnergy();
  FEMNonHookeanElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu);
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  virtual int polyOrder() const override;
};
template <typename T>
struct FEMFluidEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMFluidEnergy();
  FEMFluidEnergy(std::shared_ptr<FEMVertex<T>> V[3],T level,T rho,T G,T dragMult,int deg=1);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
  FEMEnergyContext<T> updateFluid(const Vec& pNN,const Vec& pN,T dt) const;
  FEMEnergyContext<T> updateFluid(const Vec& pN,const Vec& DpNDt) const;
  void setParameters(T level,T rho,T G,T dragMult,int deg=1);
  std::shared_ptr<FEMVertex<T>> V(int d) const;
  const std::vector<std::pair<Vec3T,T>>& coefs() const;
  T level() const;
  T rho() const;
  T G() const;
  T rhoG() const;
  T dragMult() const;
 private:
  std::shared_ptr<FEMVertex<T>> _V[3];
  std::vector<std::pair<Vec3T,T>> _coefs;
  T _level,_rho,_G,_rhoG,_dragMult;
};
template <typename T>
struct FEMPenetrationEnergy : public FEMEnergy<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMEnergy<T>)
#ifndef SWIG
  using typename FEMEnergy<T>::STrip;
  using typename FEMEnergy<T>::STrips;
  using typename FEMEnergy<T>::SMatT;
  using typename FEMEnergy<T>::EFunc;
  using typename FEMEnergy<T>::GFunc;
  using typename FEMEnergy<T>::HFunc;
  using FEMEnergy<T>::eval;
#endif
  FEMPenetrationEnergy();
  FEMPenetrationEnergy(std::shared_ptr<FEMVertex<T>> vert,const Vec3T& pos,const Vec3T& n,T phi,T coef);
  FEMPenetrationEnergy(std::shared_ptr<FEMVertex<T>> vert,std::shared_ptr<Tetrahedron<T>> tet,const Vec4T& depth,T coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual std::shared_ptr<FEMEnergy<T>> toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const override;
  virtual bool eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>* ctx=NULL) const override;
 private:
  virtual bool evalEnvCollision(const Vec& x,EFunc* E,GFunc* G,HFunc* H) const;
  virtual bool evalSelfCollision(const Vec& x,EFunc* E,GFunc* G,HFunc* H) const;
  std::shared_ptr<FEMVertex<T>> _vert;
  std::shared_ptr<Tetrahedron<T>> _tet;
  Vec4T _depth;
  T _coef;
};
}

#endif
