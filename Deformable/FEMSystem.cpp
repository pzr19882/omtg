﻿#include "FEMSystem.h"
#include "MobyLCPSolver.h"
#include "InteriorQCPSolver.h"
#include <Utils/IO.h>
#include <Utils/SparseUtils.h>
#include <Environment/Environment.h>
#include <stack>

namespace PHYSICSMOTION {
//FEMSystem
template <typename T>
FEMSystem<T>::FEMSystem() {}
template <typename T>
FEMSystem<T>::FEMSystem(std::shared_ptr<FEMMesh<T>> mesh):_mesh(mesh),_dampingCoef(0.1f),_dt(0.01f),_mu(0.7f),_maxContact(std::numeric_limits<int>::max()) {
  if(!_mesh->isFixed())
    _mesh->moveToCenterOfMass();
  _mass=_mesh->getMassMatrix();
}
template <typename T>
bool FEMSystem<T>::read(std::istream& is,IOData* dat) {
  registerType<FEMMesh<T>>(dat);
  registerType<FEMPointConstraintEnergy<T>>(dat);
  registerType<FEMConstantForceEnergy<T>>(dat);
  registerType<FEMLinearElasticEnergy<T>>(dat);
  registerType<FEMStVKElasticEnergy<T>>(dat);
  registerType<FEMCorotatedElasticEnergy<T>>(dat);
  registerType<FEMNonHookeanElasticEnergy<T>>(dat);
  registerType<FEMFluidEnergy<T>>(dat);
  registerType<EnvironmentExact<T>>(dat);
  registerType<EnvironmentHeight<T>>(dat);

  readBinaryData(_mesh,is,dat);
  readBinaryData(_energies,is,dat);
  readBinaryData(_env,is,dat);
  readBinaryData(_dampingCoef,is);
  readBinaryData(_dt,is);
  readBinaryData(_mu,is);
  readBinaryData(_maxContact,is);
  readBinaryData(_mass,is);
  return is.good();
}
template <typename T>
bool FEMSystem<T>::write(std::ostream& os,IOData* dat) const {
  registerType<FEMMesh<T>>(dat);
  registerType<FEMPointConstraintEnergy<T>>(dat);
  registerType<FEMConstantForceEnergy<T>>(dat);
  registerType<FEMLinearElasticEnergy<T>>(dat);
  registerType<FEMStVKElasticEnergy<T>>(dat);
  registerType<FEMCorotatedElasticEnergy<T>>(dat);
  registerType<FEMNonHookeanElasticEnergy<T>>(dat);
  registerType<FEMFluidEnergy<T>>(dat);
  registerType<EnvironmentExact<T>>(dat);
  registerType<EnvironmentHeight<T>>(dat);

  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_energies,os,dat);
  writeBinaryData(_env,os,dat);
  writeBinaryData(_dampingCoef,os);
  writeBinaryData(_dt,os);
  writeBinaryData(_mu,os);
  writeBinaryData(_maxContact,os);
  writeBinaryData(_mass,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMSystem<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMSystem<T>);
}
template <typename T>
std::string FEMSystem<T>::type() const {
  return typeid(FEMSystem<T>).name();
}
//setup
template <typename T>
T FEMSystem<T>::mu(T young,T poisson) {
  return young/(2.0f*(1.0f+poisson));
}
template <typename T>
T FEMSystem<T>::lambda(T young,T poisson) {
  return poisson*young/((1.0f+poisson)*(1.0f-2.0f*poisson));
}
template <typename T>
std::shared_ptr<FEMMesh<T>> FEMSystem<T>::getBody() const {
  return _mesh;
}
template <typename T>
std::shared_ptr<Environment<T>> FEMSystem<T>::getEnv() const {
  return _env;
}
template <typename T>
std::vector<std::shared_ptr<FEMEnergy<T>>> FEMSystem<T>::getEnergy() const {
  return _energies;
}
template <typename T>
void FEMSystem<T>::setEnv(std::shared_ptr<Environment<T>> env) {
  _env=env;
}
template <typename T>
void FEMSystem<T>::addPointConstraint(const Vec& p,Vec3T pos,T coef,bool constraintToPos) {
  Vec3T closestCP;
  int closestTid=-1;
  TriangleExact::Vec3T closestBary;
  TriangleExact::T closestDistance;
  for(int i=0; i<_mesh->nrSC(); i++) {
    Vec3T a=_mesh->getSV(_mesh->getSC(i)[0])->operator()(mapCV(p));
    Vec3T b=_mesh->getSV(_mesh->getSC(i)[1])->operator()(mapCV(p));
    Vec3T c=_mesh->getSV(_mesh->getSC(i)[2])->operator()(mapCV(p));
    TriangleExact t(a.template cast<TriangleExact::T>(),
                    b.template cast<TriangleExact::T>(),
                    c.template cast<TriangleExact::T>());

    Eigen::Matrix<int,2,1> feat;
    TriangleExact::T sqrDistance;
    TriangleExact::Vec3T cp,bary;
    t.calcPointDist(pos.template cast<TriangleExact::T>(),sqrDistance,cp,bary,feat);
    if(closestTid<0 || sqrDistance<closestDistance) {
      closestBary=bary;
      closestTid=i;
      closestCP=cp.template cast<T>();
      closestDistance=sqrDistance;
    }
  }
  if(!constraintToPos)
    pos=closestCP;
  std::shared_ptr<FEMPointConstraintEnergy<T>> c(new FEMPointConstraintEnergy<T>(pos,coef));
  for(int d=0; d<3; d++)
    c->add(_mesh->getSV(_mesh->getSC(closestTid)[d]),T(closestBary[d]));
  _energies.push_back(c);
}
template <typename T>
void FEMSystem<T>::setFluidEnergy(T level,T rho,T G,T dragMult,int deg) {
  for(std::shared_ptr<FEMEnergy<T>> e:_energies)
    if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(e)) {
      std::shared_ptr<FEMFluidEnergy<T>> fe=std::dynamic_pointer_cast<FEMFluidEnergy<T>>(e);
      fe->setParameters(level,rho,G,dragMult,deg);
    }
}
template <typename T>
void FEMSystem<T>::addFluidEnergy(T level,T rho,T G,T dragMult,int deg) {
  std::shared_ptr<FEMVertex<T>> V[3];
  for(int i=0; i<_mesh->nrSC(); i++) {
    for(int d=0; d<3; d++)
      V[d]=_mesh->getSV(_mesh->getSC(i)[d]);
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMFluidEnergy<T>(V,level,rho,G,dragMult,deg)));
  }
}
template <typename T>
void FEMSystem<T>::addRandomPenetrationEnergy(T coef) {
  Vec4T depth=Vec4T::Random();
  int svid=rand()%_mesh->nrSV();
  int cid=rand()%_mesh->nrC();
  _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMPenetrationEnergy<T>(_mesh->getSV(svid),std::dynamic_pointer_cast<Tetrahedron<T>>(_mesh->getC(cid)),depth,coef)));
  _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMPenetrationEnergy<T>(_mesh->getSV(svid),Vec3T::Random(),depth.template segment<3>(0),depth[3],coef)));
}
template <typename T>
void FEMSystem<T>::addLinearElasticEnergy(T lambda,T mu) {
  for(int i=0; i<_mesh->nrC(); i++)
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMLinearElasticEnergy<T>(_mesh->getC(i),_mesh->pos0(),lambda,mu)));
}
template <typename T>
void FEMSystem<T>::addStVKElasticEnergy(T lambda,T mu) {
  for(int i=0; i<_mesh->nrC(); i++)
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMStVKElasticEnergy<T>(_mesh->getC(i),lambda,mu)));
}
template <typename T>
void FEMSystem<T>::addCorotatedElasticEnergy(T lambda,T mu) {
  for(int i=0; i<_mesh->nrC(); i++)
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMCorotatedElasticEnergy<T>(_mesh->getC(i),lambda,mu)));
}
template <typename T>
void FEMSystem<T>::addNonHookeanElasticEnergy(T lambda,T mu) {
  for(int i=0; i<_mesh->nrC(); i++)
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMNonHookeanElasticEnergy<T>(_mesh->getC(i),lambda,mu)));
}
template <typename T>
void FEMSystem<T>::addGravitationalEnergy(T G) {
  for(int i=0; i<_mesh->nrC(); i++)
    _energies.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMConstantForceEnergy<T>(_mesh->getC(i),-Vec3T::UnitZ()*G)));
}
template <typename T>
const typename FEMSystem<T>::SMatT& FEMSystem<T>::getMass() const {
  return _mass;
}
template <typename T>
void FEMSystem<T>::clearEnergy() {
  _energies.clear();
}
template <typename T>
int FEMSystem<T>::maxContact() const {
  return _maxContact;
}
template <typename T>
void FEMSystem<T>::maxContact(int n) {
  _maxContact=n;
}
template <typename T>
void FEMSystem<T>::setSelfCCDebugOutputPath(const std::string& path) {
  _selfCCDebugOutputPath=path;
}
template <typename T>
void FEMSystem<T>::addSelfCC(T coef) {
  _selfCC.reset(new FEMSelfCollision<T>(_mesh));
  _selfCC->writeDepthVTK(_mesh->pos0(),"depth.vtk");
  _selfCCoef=coef;
}
template <typename T>
T FEMSystem<T>::dampingCoef() const {
  return _dampingCoef;
}
template <typename T>
void FEMSystem<T>::dampingCoef(T d) {
  _dampingCoef=d;
}
template <typename T>
T FEMSystem<T>::dt() const {
  return _dt;
}
template <typename T>
void FEMSystem<T>::dt(T dt) {
  _dt=dt;
}
template <typename T>
T FEMSystem<T>::mu() const {
  return _mu;
}
template <typename T>
void FEMSystem<T>::mu(T mu) {
  _mu=mu;
}
//simulate
template <typename T>
void FEMSystem<T>::updateFluid(const Vec& pNN,const Vec& pN,std::vector<FEMEnergyContext<T>>& context) const {
  context.resize(_energies.size());
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_energies.size(); i++)
    if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(_energies[i]))
      context[i]=std::dynamic_pointer_cast<FEMFluidEnergy<T>>(_energies[i])->updateFluid(pNN,pN,dt());
}
template <typename T>
void FEMSystem<T>::detectCollision(const Vec& p,std::vector<FEMCollision<T>>& colls) const {
  if(!_env)
    return;
  colls.clear();
  std::vector<Node<int,BBoxExact>> bvhFEM;
  const std::vector<Node<int,BBoxExact>>& bvhEnv=_env->getBVH();
  std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>> SVPos=_mesh->getSBVH(p,bvhFEM);
  //main loop
  std::stack<std::pair<int,int>> ss;
  ss.push(std::make_pair((int)bvhFEM.size()-1,(int)bvhEnv.size()-1));
  while(!ss.empty()) {
    int l=ss.top().first;
    int r=ss.top().second;
    ss.pop();
    if(!bvhFEM[l]._bb.intersect(bvhEnv[r]._bb))
      continue;
    else if(bvhFEM[l]._l==-1 && bvhEnv[r]._l==-1) {
      T phi=_env->phi(SVPos[l]);
      if(phi<0)
        colls.push_back(FEMCollision<T>(_mesh->getSV(l),l,SVPos[l],_env->phiGrad(SVPos[l]),phi));
    } else if(bvhFEM[l]._l==-1) {
      ss.push(std::make_pair(l,bvhEnv[r]._l));
      ss.push(std::make_pair(l,bvhEnv[r]._r));
    } else if(bvhEnv[r]._l==-1) {
      ss.push(std::make_pair(bvhFEM[l]._l,r));
      ss.push(std::make_pair(bvhFEM[l]._r,r));
    } else {
      ss.push(std::make_pair(bvhFEM[l]._l,bvhEnv[r]._l));
      ss.push(std::make_pair(bvhFEM[l]._r,bvhEnv[r]._l));
      ss.push(std::make_pair(bvhFEM[l]._l,bvhEnv[r]._r));
      ss.push(std::make_pair(bvhFEM[l]._r,bvhEnv[r]._r));
    }
  }
}
template <typename T>
bool FEMSystem<T>::step(const Vec& pNN,const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver,std::vector<FEMCollision<T>>* collsRet) const {
  //argmin_{p} 0.5*M/(dt*dt)*|p-2*pN+pNN|+
  //           0.5*M*_dampingCoef/dt*|p-pN|+
  //           \sum_i E_i(p)
  Vec G;
  SMatT H;
  std::vector<std::shared_ptr<FEMEnergy<T>>> ess=_energies;
  if(_selfCC)
    _selfCC->detectCollision(pN,_mesh,ess,_selfCCoef,_selfCCDebugOutputPath);
  if(!assemble(NULL,&G,&H,pNN,pN,p=pN,ctrl,&ess))
    return false;
  //collision check
  std::vector<FEMCollision<T>> colls;
  detectCollision(pN,colls);
  if((int)colls.size()>_maxContact)
    getContactSimplifier()->simplify(colls,_maxContact);
  if(collsRet)
    *collsRet=colls;
  //solve
  for(FEMCollision<T>& c:colls)
    c.buildFrame();
  SolverSPD<SMatT> invH(H);
  if(!invH.succ())
    return false;
  else if(colls.empty()) {
    p=pN-invH.solve(G);
    return true;
  } else if(solver==SP) {
    if(!staggeredProjectionS(p-pN,p,H,invH,G,_mu,colls,NULL)) //differentiable feature is not supported in fullspace
      return false;
    p+=pN;
    return true;
  } else if(solver==LCP) {
    if(!LCPProjectionS(p-pN,p,H,invH,G,_mu,colls,NULL)) //differentiable feature is not supported in fullspace
      return false;
    p+=pN;
    return true;
  } else {
    ASSERT_MSG(false,"Unsupported solver!")
    return solver;
  }
}
template <typename T>
typename FEMSystem<T>::Vec FEMSystem<T>::step(const Vec& pNN,const Vec& pN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver) const {
  Vec ret;
  bool retSucc=step(pNN,pN,ret,ctrl,solver);
  if(succ)
    *succ=retSucc;
  return ret;
}
template <typename T>
bool FEMSystem<T>::assemble(T* E,Vec* G,SMatT* H,const Vec& pNN,const Vec& pN,const Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,std::vector<std::shared_ptr<FEMEnergy<T>>>* ess) const {
  std::vector<FEMEnergyContext<T>> context;
  updateFluid(pNN,pN,context);
  Vec v=p-pN;
  Vec a=p-2*pN+pNN;
  //build system: inertial/damping
  if(E)
    *E=a.dot(_mass*a)*(.5f/(_dt*_dt))+v.dot(_mass*v)*(_dampingCoef*.5f/_dt);
  if(G)
    *G=_mass*(a*(1/(_dt*_dt))+v*(_dampingCoef/_dt));
  //build system: energies
  ParallelMatrix<T> EP(0);
  ParallelMatrix<Vec> GP(G?Vec::Zero(p.size()):Vec::Zero(0));
  ParallelVector<Eigen::Triplet<T,int>> HTrips;
  const std::vector<std::shared_ptr<FEMEnergy<T>>>& essRef=ess?*ess:_energies;
  bool valid=true;
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)essRef.size(); i++)
    if(!essRef[i]->eval(p,E?&EP:NULL,G?&GP:NULL,H?&HTrips:NULL,&context[i]))
      valid=false;
  if(E)
    *E+=EP.getValue();
  if(G)
    *G+=GP.getMatrix();
  if(H) {
    H->resize(p.size(),p.size());
    H->setFromTriplets(HTrips.begin(),HTrips.end());
    (*H)+=_mass*(1/(_dt*_dt)+_dampingCoef/_dt);
  }
  if(ctrl)
    ctrl->assemble(E,G,H,pN,p,_dt);
  return valid;
}
template <typename T>
void FEMSystem<T>::debugAssemble(int iter,T deltaPos) const {
  T E,E2;
  Vec G,G2;
  SMatT H;
  DEFINE_NUMERIC_DELTA_T(T)
  std::cout << "DebugSystem: " << typeid(*this).name() << std::endl;
  for(int i=0; i<iter; i++)
    while(true) {
      Vec pNN=_mesh->pos0()+Vec::Random(_mesh->pos0().size())*deltaPos;
      Vec pN=_mesh->pos0()+Vec::Random(_mesh->pos0().size())*deltaPos;
      Vec p=_mesh->pos0()+Vec::Random(_mesh->pos0().size())*deltaPos;
      std::shared_ptr<FEMPDController<T>> ctrl(new FEMPDController<T>(Vec::Random(_mesh->pos0().size()),100,Vec::Random(_mesh->pos0().size()),100));
      if(!assemble(&E,&G,&H,pNN,pN,p,ctrl))
        continue;
      Vec dx=Vec::Random(_mesh->pos0().size());
      if(!assemble(&E2,&G2,NULL,pNN,pN,p+dx*DELTA,ctrl))
        continue;
      DEBUG_GRADIENT("E",G.dot(dx),G.dot(dx)-(E2-E)/DELTA)
      DEBUG_GRADIENT("G",(H*dx).norm(),(H*dx-(G2-G)/DELTA).norm())
      break;
    }
}
//helper
template <typename T>
std::shared_ptr<ContactSimplifier> FEMSystem<T>::getContactSimplifier() const {
  OMP_CRITICAL_ {
    if(!_simplifier)
      const_cast<FEMSystem<T>&>(*this)._simplifier.reset(new ContactSimplifier(_mesh->nrSV(),_mesh->getSC()));
  }
  return _simplifier;
}
template <typename T>
bool FEMSystem<T>::staggeredProjectionD(const Vec& pN,Vec& p,const MatT& H,const SolverSPD<MatT>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter,T eps,bool cb) {
  return staggeredProjection(pN,p,H,invH,G,mu,colls,AS,maxIter,eps,cb);
}
template <typename T>
bool FEMSystem<T>::staggeredProjectionS(const Vec& pN,Vec& p,const SMatT& H,const SolverSPD<SMatT>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter,T eps,bool cb) {
  return staggeredProjection(pN,p,H,invH,G,mu,colls,AS,maxIter,eps,cb);
}
template <typename T>
bool FEMSystem<T>::LCPProjectionD(const Vec& pN,Vec& p,const MatT& H,const SolverSPD<MatT>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir,bool cb) {
  return LCPProjection(pN,p,H,invH,G,mu,colls,AS,nDir,cb);
}
template <typename T>
bool FEMSystem<T>::LCPProjectionS(const Vec& pN,Vec& p,const SMatT& H,const SolverSPD<SMatT>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir,bool cb) {
  return LCPProjection(pN,p,H,invH,G,mu,colls,AS,nDir,cb);
}
template <typename T>
template <typename MATH>
bool FEMSystem<T>::staggeredProjection(const Vec& pN,Vec& p,const MATH&,const SolverSPD<MATH>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter,T eps,bool cb) {
  p=pN-invH.solve(G);
  int nC=(int)colls.size();
  MatT N=MatT::Zero(nC,p.size()),M;
  MatT TT=MatT::Zero(nC*2,p.size());
  T dt=1; //the problem is invariant to scaling of dt
  //initialize force
  Vec fn=Vec::Zero(nC),fnLast=fn,q;
  Vec ft=Vec::Zero(nC*2),ftLast=ft;
  //compute the effect of force on position:
  //p=(pN-H^{-1}G)+DpDfn*fn+DpDft*ft
  //we assemble virtual-work matrix: N^{nC*3 X |p|}
  for(int i=0; i<nC; i++)
    colls[i].visit([&](const typename FEMCollision<T>::CollisionInfo& info) {
    N.block(i,info._DOFId,1,info._n.size())=info._n.transpose()*info._coef;
    if(mu>0) {
      TT.block(i*2+0,info._DOFId,1,info._t1.size())=info._t1.transpose()*info._coef;
      TT.block(i*2+1,info._DOFId,1,info._t2.size())=info._t2.transpose()*info._coef;
    }
  });
  MatT DpDfn=invH.solve(N.transpose());
  MatT DpDft=mu>0?invH.solve(TT.transpose()):MatT();
  for(int iter=0; iter<maxIter; iter++) {
    //normal constraint
    //we use shorthand notation dpt=p+DpDft*ft
    //0 <= N*(dpt+DpDfn*fn)/dt \perp fn >= 0
    Vec dpt=p;
    if(mu>0)
      dpt+=DpDft*ft;
    if(!LCPSolve(M=N*DpDfn/dt,q=N*dpt/dt,fn,true)) {
      if(cb)
        std::cout << "LCPSolve failed!" << std::endl;
      break;
    }
    if(iter==0 && AS)
      AS->reset(M,q,fn,N,colls);
    if(mu<=0)
      break;
    //tangential constraint
    //|ft(i)|<=mu*fn(i)
    //we use shorthand notation dpn=p+DpDfn*fn
    //
    //the work of frictional force (the objective function) is:
    // 0.5*ft^T*TT*(dpn+DpDft*ft)/dt
    //=0.5*ft^T*TT*DpDft*ft/dt+
    // 0.5*ft^T*TT*dpn/dt
    Vec dpn=p+DpDfn*fn;
    if(!QCPSolve(TT*DpDft/dt,TT*dpn/(dt*2),fn*mu,ft)) {
      if(cb)
        std::cout << "QCPSolve failed!" << std::endl;
      break;
    }
    //stopping
    bool fnConverge=(fnLast-fn).cwiseAbs().maxCoeff()<eps*fn.cwiseAbs().maxCoeff();
    bool ftConverge=(ftLast-ft).cwiseAbs().maxCoeff()<eps*ft.cwiseAbs().maxCoeff();
    if(iter>0 && cb) {
      std::cout << "fnRelDiff=" << (fnLast-fn).cwiseAbs().maxCoeff()/std::max(Epsilon<T>::defaultEps(),fn.cwiseAbs().maxCoeff()) << " ";
      std::cout << "ftRelDiff=" << (ftLast-ft).cwiseAbs().maxCoeff()/std::max(Epsilon<T>::defaultEps(),ft.cwiseAbs().maxCoeff()) << std::endl;
    }
    if(iter>0 && fnConverge && ftConverge)
      break;
    fnLast=fn;
    ftLast=ft;
  }
  p+=DpDfn*fn;
  if(mu>0)
    p+=DpDft*ft;
  return true;
}
template <typename T>
template <typename MATH>
bool FEMSystem<T>::LCPProjection(const Vec& pN,Vec& p,const MATH&,const SolverSPD<MATH>& invH,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir,bool cb) {
#define FNOFF(i) ((i)*nVarPerColl)
#define LAMBDAOFF(i) ((i)*nVarPerColl+1)
#define BETAOFF(i) ((i)*nVarPerColl+2)
  //the LCP formulation uses the two following constraints:
  //FN:     0 <= N*v \perp fn >= 0
  //BETA:   0 <= \lambda*e+T^T*v \perp \beta >= 0
  //LAMBDA: 0 <= mu*fn - e^T*\beta \perp \lambda>= 0
  T dt=1; //the problem is invariant to scaling of dt
  int nC=(int)colls.size();
  int nVarPerColl=mu<=0?1:nDir+2;
  MatT M=MatT::Zero(nC*nVarPerColl,nC*nVarPerColl);
  Vec q=Vec::Zero(nC*nVarPerColl),z=q;

  //build linear frictional cone
  Mat2XT D;
  D.resize(2,nDir);
  if(mu>0)
    for(int i=0; i<nDir; i++) {
      T angle=M_PI*2*i/nDir;
      D(0,i)=cos(angle);
      D(1,i)=sin(angle);
    }

  //compute the effect of force on position:
  //p=(pN-H^{-1}G)+DpDfn*fn+DpDft*ft
  p=pN-invH.solve(G);
  MatT DpDfn=MatT::Zero(p.size(),nC);
  MatT DpDft=MatT::Zero(p.size(),nC*2);
  for(int i=0; i<nC; i++)
    colls[i].visit([&](const typename FEMCollision<T>::CollisionInfo& info) {
    DpDfn.block(info._DOFId,i,info._n.size(),1)=info._n*info._coef;
    if(mu>0) {
      DpDft.block(info._DOFId,i*2+0,info._t1.size(),1)=info._t1*info._coef;
      DpDft.block(info._DOFId,i*2+1,info._t2.size(),1)=info._t2*info._coef;
    }
  });
  DpDfn=invH.solve(DpDfn);
  if(mu>0)
    DpDft=invH.solve(DpDft);

  //assemble system
  const Vec& v0=p;
  OMP_PARALLEL_FOR_
  for(int i=0; i<nC; i++) {
    //FN:     0 <= N*v \perp fn >= 0
    //N*v = n^T*(p+DpDfn*fn+DpDft*ft-pN)/dt
    //    = n^T*v0 + n^T*DpDfn*fn/dt + n^T*DpDft*(ft=D*beta)/dt
    colls[i].visit([&](const typename FEMCollision<T>::CollisionInfo& info) {
      for(int j=0; j<nC; j++) {
        M(FNOFF(i),FNOFF(j))+=info._n.dot(DpDfn.col(j).segment(info._DOFId,info._n.size()))*info._coef/dt;
        if(mu>0)
          M.block(FNOFF(i),BETAOFF(j),1,nDir)+=(info._n.transpose()*DpDft.block(info._DOFId,j*2,info._n.size(),2)*D)*info._coef/dt;
      }
      q[FNOFF(i)]+=info._n.dot(v0.segment(info._DOFId,info._n.size()))*info._coef;
    });
    if(mu<=0)
      continue;
    //BETA:   0 <= \lambda*e+T^T*v \perp \beta >= 0
    M.block(BETAOFF(i),LAMBDAOFF(i),nDir,1)+=Vec::Ones(nDir);
    colls[i].visit([&](const typename FEMCollision<T>::CollisionInfo& info) {
      for(int d=0; d<nDir; d++) {
        Vec t=info._t1*D(0,d)+info._t2*D(1,d);
        for(int j=0; j<nC; j++) {
          M(BETAOFF(i)+d,FNOFF(j))+=t.dot(DpDfn.col(j).segment(info._DOFId,info._n.size()))*info._coef/dt;
          M.block(BETAOFF(i)+d,BETAOFF(j),1,nDir)+=(t.transpose()*DpDft.block(info._DOFId,j*2,t.size(),2)*D)*info._coef/dt;
        }
        q[BETAOFF(i)+d]+=t.dot(v0.segment(info._DOFId,t.size()))*info._coef;
      }
    });
    //LAMBDA: 0 <= mu*fn - e^T*\beta \perp \lambda>= 0
    M(LAMBDAOFF(i),FNOFF(i))+=mu;
    M.block(LAMBDAOFF(i),BETAOFF(i),1,nDir)-=Vec::Ones(nDir).transpose();
  }
  //if(!LCPSolve(M,q,z))
  //  return false;
  //even if LCP fails, we let it through
  if(LCPSolve(M,q,z)) {
    if(cb)
      std::cout << "QPSolve failed!" << std::endl;
  }
  if(AS)
    AS->reset(M,q,z,DpDfn.transpose(),colls);
  for(int i=0; i<nC; i++) {
    p+=DpDfn.col(i)*z[FNOFF(i)];
    if(mu>0) {
      p+=DpDft.col(i*2+0)*D.row(0).dot(z.segment(BETAOFF(i),nDir));
      p+=DpDft.col(i*2+1)*D.row(1).dot(z.segment(BETAOFF(i),nDir));
    }
  }
  return true;
#undef FNOFF
#undef LAMBDAOFF
#undef BETAOFF
}
template <typename T>
bool FEMSystem<T>::QCPSolve(const MatT& Ht,const Vec& Gt,const Vec& Bt,Vec& x) {
  bool succ;
  QCPSolver<T> sol;
  x=sol.solve(QCPSolver<T>::setupQCPProb(Ht,Gt,Bt),&x,succ);
  return succ;
}
template <typename T>
bool FEMSystem<T>::LCPSolve(const MatT& M,const Vec& q,Vec& z,bool useInterior) {
  if(useInterior) {
    bool succ;
    QCPSolver<T> sol;
    //sol.callback(true);
    z=sol.solve(QCPSolver<T>::setupQPProb(M,q),&z,succ);
    return succ;
  } else {
    Eigen::VectorXd qd=q.template cast<double>();
    Eigen::VectorXd zd=z.template cast<double>();
    MobyLCPSolver sol;
    bool ret=sol.SolveLcpFastRegularized(M.template cast<double>(),qd,&zd);
    z=zd.template cast<T>();
    return ret;
  }
}
template class FEMSystem<FLOAT>;
}
