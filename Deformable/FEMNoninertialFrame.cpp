#include "FEMNoninertialFrame.h"

namespace PHYSICSMOTION {
//FEMNoninertialFrame
template <typename T>
FEMNoninertialFrame<T>::FEMNoninertialFrame(const MatT& B,const Vec& pos0,const SMatT& mass):_B(B),_pos0(pos0),_mass(mass) {
  MatX3T I;
  I.resize(B.rows(),3);
  for(int i=0; i<B.rows(); i+=3)
    I.template block<3,3>(i,0).setIdentity();
  _IMB=I.transpose()*(mass*B);
  _BTMB=B.transpose()*(mass*B);
  _m=I.transpose()*(mass*I);
  //_Iu,_Ipu,_Ippu
  _Iu.setConstant(MatT::Zero(B.cols()+1,B.cols()+1));
  _Ipu.setConstant(Vec::Zero(B.cols()+1));
  _Ippu.setConstant(3,B.cols(),Vec::Zero(B.cols()+1));
  for(int r=0; r<=B.cols(); r++) {
    MatX3T CBIr=crossDiag(r<B.cols()?Vec(B.col(r)):pos0)*I;
    //_Iu
    for(int c=0; c<=B.cols(); c++) {
      MatX3T CBIc=crossDiag(c<B.cols()?Vec(B.col(c)):pos0)*I;
      Mat3T rc=CBIr.transpose()*(mass*CBIc);
      for(int rr=0; rr<3; rr++)
        for(int cc=0; cc<3; cc++)
          _Iu(rr,cc)(r,c)=rc(rr,cc);
    }
    //_Ipu
    {
      Mat3T rc=-CBIr.transpose()*(_mass*I);
      for(int rr=0; rr<3; rr++)
        for(int cc=0; cc<3; cc++)
          _Ipu(rr,cc)[r]=rc(rr,cc);
    }
    //_Ippu
    {
      Mat3XT rc=-CBIr.transpose()*(_mass*B);
      for(int rr=0; rr<3; rr++)
        for(int cc=0; cc<B.cols(); cc++)
          _Ippu(rr,cc)[r]=rc(rr,cc);
    }
  }
}
template <typename T>
T FEMNoninertialFrame<T>::LagrangianRef(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  Vec xLocal=_B*u+_pos0,vLocal=_B*DuDt;
  for(int i=0; i<xLocal.size(); i+=3)
    vLocal.template segment<3>(i)+=v+w.cross(xLocal.template segment<3>(i));
  return 0.5*vLocal.dot(_mass*vLocal);
}
template <typename T>
T FEMNoninertialFrame<T>::Lagrangian(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  return 0.5*v.dot(_m*v)+0.5*w.dot(I(u)*w)+0.5*DuDt.dot(_BTMB*DuDt)+
         v.dot(_IMB*DuDt)+w.dot(Ip(u)*v)+w.dot(Ipp(u)*DuDt);
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DLDdu(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  return _BTMB*DuDt+_IMB.transpose()*v+Ipp(u).transpose()*w;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec3T FEMNoninertialFrame<T>::DLDv(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  return _m*v+_IMB*DuDt+Ip(u).transpose()*w;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec3T FEMNoninertialFrame<T>::DLDw(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  return I(u)*w+Ip(u)*v+Ipp(u)*DuDt;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DLDu(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt) const {
  return 0.5*DIDu(u,w)+DIpDu(w,v)+DIppDu(w,DuDt);
}
template <typename T>
T FEMNoninertialFrame<T>::DLDt(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const {
  T ret=0;
  ret+=DLDdu(v,w,u,DuDt).dot(DDuDDt);
  ret+=DLDv(v,w,u,DuDt).dot(DvDt);
  ret+=DLDw(v,w,u,DuDt).dot(DwDt);
  ret+=DLDu(v,w,u,DuDt).dot(DuDt);
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DDLDduDt(const Vec3T&,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const {
  return _BTMB*DDuDDt+_IMB.transpose()*DvDt+DIppDt(DuDt).transpose()*w+Ipp(u).transpose()*DwDt;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DDLDvDt(const Vec3T&,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const {
  return _m*DvDt+_IMB*DDuDDt+DIpDt(DuDt).transpose()*w+Ip(u).transpose()*DwDt;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DDLDwDt(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const {
  return DIDt(u,DuDt)*w+I(u)*DwDt+DIpDt(DuDt)*v+Ip(u)*DvDt+DIppDt(DuDt)*DuDt+Ipp(u)*DDuDDt;
}
template <typename T>
void FEMNoninertialFrame<T>::assembleSystem(const Vec3T* v,const Vec3T* w,const Vec& u,const Vec& DuDt,MatT& GMass,Vec& GForce) const {
  if(v && w)
    assembleSystem(*v,*w,u,DuDt,GMass,GForce);
  else {
    GMass=_BTMB;
    GForce.setZero(_B.cols());
  }
}
template <typename T>
void FEMNoninertialFrame<T>::assembleSystem(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,MatT& GMass,Vec& GForce) const {
  Mat3T IVal=I(u);
  Mat3T DIDtVal=DIDt(u,DuDt);
  Mat3T IpVal=Ip(u);
  Mat3T DIpDtVal=DIpDt(DuDt);
  Mat3XT IppVal=Ipp(u);
  Mat3XT DIppDtVal=DIppDt(DuDt);

  //GMass
  //0,0
  GMass.block(0,0,_B.cols(),_B.cols())+=_BTMB;
  //1,1
  GMass.template block<3,3>(_B.cols(),_B.cols())+=_m;
  //1,0/0,1
  GMass.block(_B.cols(),0,3,_B.cols())+=_IMB;
  GMass.block(0,_B.cols(),_B.cols(),3).transpose()+=_IMB;
  //2,2
  GMass.template block<3,3>(_B.cols()+3,_B.cols()+3)+=IVal;
  //2,1/1,2
  GMass.template block<3,3>(_B.cols()+3,_B.cols())+=IpVal;
  GMass.template block<3,3>(_B.cols(),_B.cols()+3).transpose()+=IpVal;
  //2,0/0,2
  GMass.block(_B.cols()+3,0,3,_B.cols())+=IppVal;
  GMass.block(0,_B.cols()+3,_B.cols(),3).transpose()+=IppVal;

  //GForce
  GForce.segment(0,_B.cols())+=DIppDtVal.transpose()*w-DLDu(v,w,u,DuDt);
  GForce.template segment<3>(_B.cols())+=DIpDtVal.transpose()*w;
  GForce.template segment<3>(_B.cols()+3)+=DIDtVal*w+DIpDtVal*v+DIppDtVal*DuDt;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::assembleSystemRef(const Vec3T& v,const Vec3T& w,const Vec& u,const Vec& DuDt,const Vec& DvDt,const Vec& DwDt,const Vec& DDuDDt) const {
  Vec uSys=DDLDduDt(v,w,u,DuDt,DvDt,DwDt,DDuDDt)-DLDu(v,w,u,DuDt);
  Vec vSys=DDLDvDt(v,w,u,DuDt,DvDt,DwDt,DDuDDt);
  Vec wSys=DDLDwDt(v,w,u,DuDt,DvDt,DwDt,DDuDDt);
  return concat<Vec>(uSys,concat<Vec>(vSys,wSys));
}
template <typename T>
void FEMNoninertialFrame<T>::debug() const {
  Vec3T v=Vec3T::Random(),dv=Vec3T::Random(),DvDt=Vec3T::Random();
  Vec3T w=Vec3T::Random(),dw=Vec3T::Random(),DwDt=Vec3T::Random();
  Vec u=Vec::Random(_B.cols()),DuDt=Vec::Random(_B.cols()),DDuDDt=Vec::Random(_B.cols());
  Vec delta;

  DEFINE_NUMERIC_DELTA_T(T)
  //Lagrangian
  T LRef=LagrangianRef(v,w,u,DuDt);
  T L=Lagrangian(v,w,u,DuDt);
  DEBUG_GRADIENT("Lagrangian",LRef,L-LRef)
  //DLDt
  T Lt=DLDt(v,w,u,DuDt,DvDt,DwDt,DDuDDt);
  T L2=Lagrangian(v+DvDt*DELTA,w+DwDt*DELTA,u+DuDt*DELTA,DuDt+DDuDDt*DELTA);
  DEBUG_GRADIENT("DLDt",Lt,Lt-(L2-L)/DELTA)
  //DLDu
  delta.setRandom(_B.cols());
  L2=Lagrangian(v,w,u+delta*DELTA,DuDt);
  DEBUG_GRADIENT("DLDu",DLDu(v,w,u,DuDt).dot(delta),DLDu(v,w,u,DuDt).dot(delta)-(L2-L)/DELTA)
  //DLDduvw
  delta.setRandom(_B.cols());
  L2=Lagrangian(v,w,u,DuDt+delta*DELTA);
  DEBUG_GRADIENT("DLDdu",DLDdu(v,w,u,DuDt).dot(delta),DLDdu(v,w,u,DuDt).dot(delta)-(L2-L)/DELTA)
  delta.setRandom(3);
  L2=Lagrangian(v+delta*DELTA,w,u,DuDt);
  DEBUG_GRADIENT("DLDv",DLDv(v,w,u,DuDt).dot(delta),DLDv(v,w,u,DuDt).dot(delta)-(L2-L)/DELTA)
  delta.setRandom(3);
  L2=Lagrangian(v,w+delta*DELTA,u,DuDt);
  DEBUG_GRADIENT("DLDw",DLDw(v,w,u,DuDt).dot(delta),DLDw(v,w,u,DuDt).dot(delta)-(L2-L)/DELTA)
  //DDLDduvwDt
  Vec DLDduVal=DLDdu(v,w,u,DuDt);
  Vec DLDduVal2=DLDdu(v+dv*DELTA,w+dw*DELTA,u+DuDt*DELTA,DuDt+DDuDDt*DELTA);
  DEBUG_GRADIENT("DDLDduDt",DDLDduDt(v,w,u,DuDt,dv,dw,DDuDDt).norm(),(DDLDduDt(v,w,u,DuDt,dv,dw,DDuDDt)-(DLDduVal2-DLDduVal)/DELTA).norm())
  Vec DLDvVal=DLDv(v,w,u,DuDt);
  Vec DLDvVal2=DLDv(v+dv*DELTA,w+dw*DELTA,u+DuDt*DELTA,DuDt+DDuDDt*DELTA);
  DEBUG_GRADIENT("DDLDvDt",DDLDvDt(v,w,u,DuDt,dv,dw,DDuDDt).norm(),(DDLDvDt(v,w,u,DuDt,dv,dw,DDuDDt)-(DLDvVal2-DLDvVal)/DELTA).norm())
  Vec DLDwVal=DLDw(v,w,u,DuDt);
  Vec DLDwVal2=DLDw(v+dv*DELTA,w+dw*DELTA,u+DuDt*DELTA,DuDt+DDuDDt*DELTA);
  DEBUG_GRADIENT("DDLDwDt",DDLDwDt(v,w,u,DuDt,dv,dw,DDuDDt).norm(),(DDLDwDt(v,w,u,DuDt,dv,dw,DDuDDt)-(DLDwVal2-DLDwVal)/DELTA).norm())
  //assemble
  MatT GMass;
  Vec GForce;
  GMass.setZero(_B.cols()+6,_B.cols()+6);
  GForce.setZero(_B.cols()+6);
  assembleSystem(v,w,u,DuDt,GMass,GForce);
  Vec sys=GMass*concat<Vec>(DDuDDt,concat<Vec>(DvDt,DwDt))+GForce;
  Vec sysRef=assembleSystemRef(v,w,u,DuDt,DvDt,DwDt,DDuDDt);
  DEBUG_GRADIENT("assembleSystem",sysRef.norm(),(sysRef-sys).norm())
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3T FEMNoninertialFrame<T>::I(const Vec& u) const {
  Mat3T ret;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret(r,c)=u.dot(_Iu(r,c).block(0,0,_B.cols(),_B.cols())*u)+
               u.dot(_Iu(r,c).col(_B.cols()).segment(0,_B.cols()))+
               u.dot(_Iu(r,c).row(_B.cols()).segment(0,_B.cols()))+
               _Iu(r,c)(_B.cols(),_B.cols());
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3T FEMNoninertialFrame<T>::DIDt(const Vec& u,const Vec& DuDt) const {
  Mat3T ret;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret(r,c)=DuDt.dot(_Iu(r,c).block(0,0,_B.cols(),_B.cols())*u)+
               u.dot(_Iu(r,c).block(0,0,_B.cols(),_B.cols())*DuDt)+
               DuDt.dot(_Iu(r,c).col(_B.cols()).segment(0,_B.cols()))+
               DuDt.dot(_Iu(r,c).row(_B.cols()).segment(0,_B.cols()));
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DIDu(const Vec& u,const Vec& w) const {
  Vec ret=Vec::Zero(_B.cols()),tmp;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++) {
      tmp=_Iu(r,c).block(0,0,_B.cols(),_B.cols())*u+
          _Iu(r,c).block(0,0,_B.cols(),_B.cols()).transpose()*u+
          _Iu(r,c).col(_B.cols()).segment(0,_B.cols())+
          _Iu(r,c).row(_B.cols()).segment(0,_B.cols()).transpose();
      ret+=tmp*w[r]*w[c];
    }
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3T FEMNoninertialFrame<T>::Ip(const Vec& u) const {
  Mat3T ret;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret(r,c)=_Ipu(r,c).segment(0,_B.cols()).dot(u)+_Ipu(r,c)[_B.cols()];
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3T FEMNoninertialFrame<T>::DIpDt(const Vec& DuDt) const {
  Mat3T ret;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret(r,c)=_Ipu(r,c).segment(0,_B.cols()).dot(DuDt);
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DIpDu(const Vec& w,const Vec& v) const {
  Vec ret=Vec::Zero(_B.cols());
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=_Ipu(r,c).segment(0,_B.cols())*w[r]*v[c];
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3XT FEMNoninertialFrame<T>::Ipp(const Vec& u) const {
  Mat3XT ret=Mat3XT::Zero(3,_B.cols());
  for(int r=0; r<3; r++)
    for(int c=0; c<_B.cols(); c++)
      ret(r,c)=_Ippu(r,c).segment(0,_B.cols()).dot(u)+_Ippu(r,c)[_B.cols()];
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Mat3XT FEMNoninertialFrame<T>::DIppDt(const Vec& du) const {
  Mat3XT ret=Mat3XT::Zero(3,_B.cols());
  for(int r=0; r<3; r++)
    for(int c=0; c<_B.cols(); c++)
      ret(r,c)=_Ippu(r,c).segment(0,_B.cols()).dot(du);
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::Vec FEMNoninertialFrame<T>::DIppDu(const Vec& w,const Vec& DuDt) const {
  Vec ret=Vec::Zero(_B.cols());
  for(int r=0; r<3; r++)
    for(int c=0; c<_B.cols(); c++)
      ret+=_Ippu(r,c).segment(0,_B.cols())*w[r]*DuDt[c];
  return ret;
}
template <typename T>
typename FEMNoninertialFrame<T>::SMatT FEMNoninertialFrame<T>::crossDiag(const Vec& v) const {
  SMatT ret;
  STrips trips;
  ret.resize(v.size(),v.size());
  for(int i=0; i<v.size(); i+=3)
    addBlock<Mat3T>(trips,i,i,cross<T>(v.template segment<3>(i)));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
//FEMLinearFST
template <typename T>
FEMLinearFST<T>::FEMLinearFST(const MatT& B,const Vec& pos0):_B(B),_pos0(pos0) {
  _Ev.setConstant(Vec3T::Zero());
  _Eu.setConstant(Vec::Zero(B.cols()));
  _Ew.setConstant(Mat3XT::Zero(3,B.cols()+1));
  _f.setZero(pos0.size());
}
template <typename T>
FEMLinearFST<T>::FEMLinearFST(const MatT& B,const Vec& pos0,const Vec& f):_B(B),_pos0(pos0) {
  _Ev.setConstant(Vec3T::Zero());
  _Eu.setConstant(Vec::Zero(B.cols()));
  _Ew.setConstant(Mat3XT::Zero(3,B.cols()+1));
  _f.setZero(pos0.size());
  for(int off=0; off<f.size(); off+=3)
    addEnergy(off,f.template segment<3>(off));
}
template <typename T>
void FEMLinearFST<T>::addEnergy(int off,const Vec3T& fBlk) {
  _f.template segment<3>(off)+=fBlk;
  for(int rr=0; rr<3; rr++)
    for(int cc=0; cc<3; cc++) {
      _Ev(rr,cc)+=DEDv(Vec3T::Unit(rr)*Vec3T::Unit(cc).transpose(),fBlk);
      _Ew(rr,cc)+=DEDw(Vec3T::Unit(rr)*Vec3T::Unit(cc).transpose(),_B,_pos0,off,fBlk);
      _Eu(rr,cc)+=DEDu(Vec3T::Unit(rr)*Vec3T::Unit(cc).transpose(),_B,off,fBlk);
    }
}
template <typename T>
void FEMLinearFST<T>::addEnergy(std::shared_ptr<FEMEnergy<T>> e) {
  typename FEMEnergy<T>::GFunc GFunc=[&](int off,const Vec3T& g) {
    addEnergy(off,-g);
  };
  e->eval(Vec::Zero(_pos0.size()),NULL,&GFunc,NULL);
}
template <typename T>
T FEMLinearFST<T>::ERef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  for(int i=0; i<_f.size(); i+=3)
    pos.template segment<3>(i)=t+R*pos.template segment<3>(i);
  return -pos.dot(_f);
}
template <typename T>
typename FEMLinearFST<T>::Vec3T FEMLinearFST<T>::DEDtvRef(const Mat3T& R) const {
  Vec3T ret=Vec3T::Zero();
  for(int i=0; i<_f.size(); i+=3)
    ret-=R.transpose()*_f.template segment<3>(i);
  return ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec3T FEMLinearFST<T>::DEDtwRef(const Mat3T& R,const Vec& u) const {
  Vec posL=_B*u+_pos0;
  Vec3T ret=Vec3T::Zero();
  for(int i=0; i<_f.size(); i+=3)
    ret-=posL.template segment<3>(i).cross(R.transpose()*_f.template segment<3>(i));
  return ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec FEMLinearFST<T>::DEDtuRef(const Mat3T& R) const {
  Vec f=_f;
  for(int i=0; i<f.size(); i+=3)
    f.template segment<3>(i)=R.transpose()*f.template segment<3>(i);
  return -_B.transpose()*f;
}
template <typename T>
typename FEMLinearFST<T>::Vec3T FEMLinearFST<T>::DEDtv(const Mat3T& R) const {
  Vec3T ret=Vec3T::Zero();
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=_Ev(r,c)*R(r,c);
  return ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec3T FEMLinearFST<T>::DEDtw(const Vec& u,const Mat3T& R) const {
  Vec3T ret=Vec3T::Zero();
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=(_Ew(r,c).block(0,0,3,u.size())*u+_Ew(r,c).col(u.size()))*R(r,c);
  return ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec FEMLinearFST<T>::DEDtu(const Mat3T& R) const {
  Vec ret=Vec::Zero(_Eu(0,0).size());
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=_Eu(r,c)*R(r,c);
  return ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec FEMLinearFST<T>::assembleSystem(const Vec& u,const Mat3T* R) const {
  if(R)
    return assembleSystem(u,*R);
  else {
    Mat3T I=Mat3T::Identity();
    return DEDtu(I);
  }
}
template <typename T>
typename FEMLinearFST<T>::Vec FEMLinearFST<T>::assembleSystem(const Vec& u,const Mat3T& R) const {
  return concat<Vec>(DEDtu(R),concat<Vec>(DEDtv(R),DEDtw(u,R)));
}
template <typename T>
void FEMLinearFST<T>::debug() const {
  Vec3T t=Vec3T::Random();
  Mat3T R=expWGradV<T,Vec3T>(Vec3T::Random()*M_PI);
  Vec u=Vec::Random(_B.cols());
  Vec3T v=Vec3T::Random();
  Vec3T w=Vec3T::Random();
  Vec du=Vec::Random(_B.cols());

  DEFINE_NUMERIC_DELTA_T(T)
  T EVal=ERef(t,R,u);
  T E2Val=ERef(t+R*v*DELTA,R,u);
  DEBUG_GRADIENT("DEDtv",DEDtv(R).dot(v),DEDtv(R).dot(v)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtvRef",DEDtvRef(R).dot(v),DEDtvRef(R).dot(v)-(E2Val-EVal)/DELTA)
  E2Val=ERef(t,expWGradV<T,Vec3T>(R*w*DELTA)*R,u);
  DEBUG_GRADIENT("DEDtw",DEDtw(u,R).dot(w),DEDtw(u,R).dot(w)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtwRef",DEDtwRef(R,u).dot(w),DEDtwRef(R,u).dot(w)-(E2Val-EVal)/DELTA)
  E2Val=ERef(t,R,u+du*DELTA);
  DEBUG_GRADIENT("DEDtu",DEDtu(R).dot(du),DEDtu(R).dot(du)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtuRef",DEDtuRef(R).dot(du),DEDtuRef(R).dot(du)-(E2Val-EVal)/DELTA)
}
template <typename T>
typename FEMLinearFST<T>::Vec3T FEMLinearFST<T>::DEDv(const Mat3T& R,const Vec3T& fBlk) const {
  return -R.transpose()*fBlk;
}
template <typename T>
typename FEMLinearFST<T>::Mat3XT FEMLinearFST<T>::DEDw(const Mat3T& R,const MatT& B,const Vec& pos0,int off,const Vec3T& fBlk) {
  Mat3T IC;
  Vec3T fL=R.transpose()*fBlk;
  Mat3XT ret=Mat3XT::Zero(3,B.cols()+1);
  for(int c=0; c<=B.cols(); c++) {
    IC=cross(c==B.cols()?Vec3T(pos0.template segment<3>(off)):Vec3T(B.col(c).template segment<3>(off)));
    ret.col(c)=IC*fL;
  }
  return -ret;
}
template <typename T>
typename FEMLinearFST<T>::Vec FEMLinearFST<T>::DEDu(const Mat3T& R,const MatT& B,int off,const Vec3T& fBlk) {
  return -B.block(off,0,3,B.cols()).transpose()*R.transpose()*fBlk;
}
//FEMQuadaticFST
template <typename T>
FEMQuadraticFST<T>::FEMQuadraticFST(const MatT& B,const Vec& pos0):_B(B),_pos0(pos0) {
  _H.resize(pos0.size(),pos0.size());
  _IHI.setZero();
  _BTHB.setZero(B.cols(),B.cols()+1);
  _IHB.setZero(3,B.cols()+1);
  _IRHB.setConstant(Mat3XT::Zero(3,B.cols()));
  _pRH1.setConstant(MatX3T::Zero(B.cols()+1,3));
  _pRHp.setConstant(MatT::Zero(B.cols()+1,B.cols()+1));
}
template <typename T>
FEMQuadraticFST<T>::FEMQuadraticFST(const MatT& B,const Vec& pos0,const SMatT& H):_B(B),_pos0(pos0) {
  _H.resize(pos0.size(),pos0.size());
  _IHI.setZero();
  _BTHB.setZero(B.cols(),B.cols()+1);
  _IHB.setZero(3,B.cols()+1);
  _IRHB.setConstant(Mat3XT::Zero(3,B.cols()));
  _pRH1.setConstant(MatX3T::Zero(B.cols()+1,3));
  _pRHp.setConstant(MatT::Zero(B.cols()+1,B.cols()+1));
  for(int k=0; k<H.outerSize(); ++k)
    for(typename SMatT::InnerIterator it(H,k); it; ++it)
      if(it.row()%3==0 && it.col()%3==0)
        //assuming block diagonal
        addEnergy(it.row(),it.col(),Mat3T::Identity()*it.value());
}
template <typename T>
void FEMQuadraticFST<T>::addEnergy(int offR,int offC,const Mat3T& HBlk) {
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      _H.coeffRef(offR+r,offC+c)+=HBlk(r,c);
  _IHI+=HBlk;
  _BTHB.block(0,0,_B.cols(),_B.cols())+=_B.block(offR,0,3,_B.cols()).transpose()*(HBlk*_B.block(offC,0,3,_B.cols()));
  _BTHB.col(_B.cols())+=_B.block(offR,0,3,_B.cols()).transpose()*(HBlk*_pos0.template segment<3>(offC));
  _IHB.block(0,0,3,_B.cols())+=HBlk*_B.block(offC,0,3,_B.cols());
  _IHB.col(_B.cols())+=HBlk*_pos0.template segment<3>(offC);
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++) {
      _IRHB(r,c)+=Vec3T::Unit(r)*Vec3T::Unit(c).transpose()*HBlk*_B.block(offC,0,3,_B.cols());
      for(int d=0; d<=_B.cols(); d++) {
        Vec3T row=d<_B.cols()?Vec3T(_B.template block<3,1>(offR,d)):Vec3T(_pos0.template segment<3>(offR));
        _pRH1(r,c).row(d)+=row.transpose()*Vec3T::Unit(r)*Vec3T::Unit(c).transpose()*HBlk;
        for(int d2=0; d2<=_B.cols(); d2++) {
          Vec3T row2=d2<_B.cols()?Vec3T(_B.template block<3,1>(offC,d2)):Vec3T(_pos0.template segment<3>(offC));
          _pRHp(r,c)(d,d2)+=row.dot(Vec3T::Unit(r)*Vec3T::Unit(c).transpose()*HBlk*row2);
        }
      }
    }
}
template <typename T>
void FEMQuadraticFST<T>::addEnergy(std::shared_ptr<FEMEnergy<T>> e) {
  typename FEMEnergy<T>::HFunc HFunc=[&](int offR,int offC,const Mat3T& h) {
    addEnergy(offR,offC,h);
  };
  e->eval(Vec::Zero(_pos0.size()),NULL,NULL,&HFunc);
}
template <typename T>
T FEMQuadraticFST<T>::ERef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  for(int i=0; i<pos.size(); i+=3)
    pos.template segment<3>(i)=R*pos.template segment<3>(i)+t;
  return pos.dot(_H*pos)/2;
}
template <typename T>
typename FEMQuadraticFST<T>::Vec3T FEMQuadraticFST<T>::DEDtvRef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  MatX3T DposDv=MatX3T::Zero(_pos0.size(),3);
  for(int i=0; i<pos.size(); i+=3) {
    pos.template segment<3>(i)=R*pos.template segment<3>(i)+t;
    DposDv.template block<3,3>(i,0)=R;
  }
  return DposDv.transpose()*(_H*pos);
}
template <typename T>
typename FEMQuadraticFST<T>::Vec3T FEMQuadraticFST<T>::DEDtwRef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  MatX3T DposDw=MatX3T::Zero(_pos0.size(),3);
  for(int i=0; i<pos.size(); i+=3) {
    for(int d=0; d<3; d++)
      DposDw.template block<3,1>(i,d)=R*cross<T>(Vec3T::Unit(d))*pos.template segment<3>(i);
    pos.template segment<3>(i)=R*pos.template segment<3>(i)+t;
  }
  return DposDw.transpose()*(_H*pos);
}
template <typename T>
typename FEMQuadraticFST<T>::Vec FEMQuadraticFST<T>::DEDtuRef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  MatT DposDu=MatT::Zero(_pos0.size(),_B.cols());
  for(int i=0; i<pos.size(); i+=3) {
    pos.template segment<3>(i)=R*pos.template segment<3>(i)+t;
    DposDu.block(i,0,3,_B.cols())=R*_B.block(i,0,3,_B.cols());
  }
  return DposDu.transpose()*(_H*pos);
}
template <typename T>
typename FEMQuadraticFST<T>::MatT FEMQuadraticFST<T>::DDEDDtRef(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec pos=_B*u+_pos0;
  MatX3T DposDv=MatX3T::Zero(_pos0.size(),3);
  MatX3T DposDw=MatX3T::Zero(_pos0.size(),3);
  MatT DposDu=MatT::Zero(_pos0.size(),_B.cols());
  for(int i=0; i<pos.size(); i+=3) {
    DposDv.template block<3,3>(i,0)=R;
    for(int d=0; d<3; d++)
      DposDw.template block<3,1>(i,d)=R*cross<T>(Vec3T::Unit(d))*pos.template segment<3>(i);
    DposDu.block(i,0,3,_B.cols())=R*_B.block(i,0,3,_B.cols());
    pos.template segment<3>(i)=R*pos.template segment<3>(i)+t;
  }
  MatT DposDuvw=concatCol<MatT>(DposDu,concatCol<MatT>(DposDv,DposDw));
  return DposDuvw.transpose()*(_H*DposDuvw);
}
template <typename T>
typename FEMQuadraticFST<T>::Vec3T FEMQuadraticFST<T>::DEDtv(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  return R.transpose()*_IHI*t+_IHB.block(0,0,3,_B.cols())*u+_IHB.col(_B.cols());
}
template <typename T>
typename FEMQuadraticFST<T>::Vec3T FEMQuadraticFST<T>::DEDtw(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  Vec3T ret=Vec3T::Zero();
  for(int d=0; d<3; d++) {
    ret[d]+=pRH1((R*cross<T>(Vec3T::Unit(d))).transpose(),u).dot(t);
    ret[d]+=pRHp(cross<T>(Vec3T::Unit(d)).transpose(),u);
  }
  return ret;
}
template <typename T>
typename FEMQuadraticFST<T>::Vec FEMQuadraticFST<T>::DEDtu(const Vec3T& t,const Mat3T& R,const Vec& u) const {
  return IRHB(R).transpose()*t+_BTHB.block(0,0,_B.cols(),_B.cols())*u+_BTHB.col(_B.cols());
}
template <typename T>
typename FEMQuadraticFST<T>::MatT FEMQuadraticFST<T>::DDEDDt(const Vec3T&,const Mat3T&,const Vec& u) const {
  MatT ret=MatT::Zero(u.size()+6,u.size()+6);
  //uu
  ret.block(0,0,u.size(),u.size())=_BTHB.block(0,0,u.size(),u.size());
  //uv
  ret.block(u.size(),0,3,u.size())=
    ret.block(0,u.size(),u.size(),3).transpose()=_IHB.block(0,0,3,u.size());
  //vv
  ret.template block<3,3>(u.size(),u.size())=_IHI;
  //uw
  for(int d=0; d<3; d++)
    ret.block(0,u.size()+3+d,u.size(),1)=
      ret.block(u.size()+3+d,0,1,u.size()).transpose()=
        BRHp(cross<T>(Vec3T::Unit(d)),u);
  //vw
  for(int d=0; d<3; d++)
    ret.template block<3,1>(u.size(),u.size()+3+d)=
      ret.template block<1,3>(u.size()+3+d,u.size()).transpose()=
        pRH1(cross<T>(Vec3T::Unit(d)).transpose(),u);
  //ww
  for(int r=0; r<3; r++)
    for(int c=r; c<3; c++)
      ret(u.size()+3+r,u.size()+3+c)=
        ret(u.size()+3+c,u.size()+3+r)=
          pRHp(cross<T>(Vec3T::Unit(r)).transpose()*cross<T>(Vec3T::Unit(c)),u);
  return ret;
}
template <typename T>
void FEMQuadraticFST<T>::assembleSystem(const Vec3T* t,const Mat3T* R,const Vec& u,MatT& GMass,Vec& GForce,T dt) const {
  if(t && R)
    assembleSystem(*t,*R,u,GMass,GForce,dt);
  else {
    GForce.segment(0,u.size())+=_BTHB.block(0,0,_B.cols(),_B.cols())*u+_BTHB.col(_B.cols());
    GMass.block(0,0,u.size(),u.size())+=_BTHB.block(0,0,_B.cols(),_B.cols())*dt;
  }
}
template <typename T>
void FEMQuadraticFST<T>::assembleSystem(const Vec3T& t,const Mat3T& R,const Vec& u,MatT& GMass,Vec& GForce,T dt) const {
  GForce.segment(0,u.size())+=DEDtu(t,R,u);
  GForce.template segment<3>(u.size())+=DEDtv(t,R,u);
  GForce.template segment<3>(u.size()+3)+=DEDtw(t,R,u);
  GMass+=DDEDDt(t,R,u)*dt;
}
template <typename T>
void FEMQuadraticFST<T>::debug() const {
  Vec3T t=Vec3T::Random();
  Mat3T R=expWGradV<T,Vec3T>(Vec3T::Random()*M_PI);
  Vec u=Vec::Random(_B.cols());
  Vec3T v=Vec3T::Random();
  Vec3T w=Vec3T::Random();
  Vec du=Vec::Random(_B.cols());

  DEFINE_NUMERIC_DELTA_T(T)
  T EVal=ERef(t,R,u);
  T E2Val=ERef(t+R*v*DELTA,R,u);
  DEBUG_GRADIENT("DEDtv",DEDtv(t,R,u).dot(v),DEDtv(t,R,u).dot(v)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtvRef",DEDtvRef(t,R,u).dot(v),DEDtvRef(t,R,u).dot(v)-(E2Val-EVal)/DELTA)
  E2Val=ERef(t,expWGradV<T,Vec3T>(R*w*DELTA)*R,u);
  DEBUG_GRADIENT("DEDtw",DEDtw(t,R,u).dot(w),DEDtw(t,R,u).dot(w)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtwRef",DEDtwRef(t,R,u).dot(w),DEDtwRef(t,R,u).dot(w)-(E2Val-EVal)/DELTA)
  E2Val=ERef(t,R,u+du*DELTA);
  DEBUG_GRADIENT("DEDtu",DEDtu(t,R,u).dot(du),DEDtu(t,R,u).dot(du)-(E2Val-EVal)/DELTA)
  DEBUG_GRADIENT("DEDtuRef",DEDtuRef(t,R,u).dot(du),DEDtuRef(t,R,u).dot(du)-(E2Val-EVal)/DELTA)
  //debug different components of DDEDDtuvw
  MatT H=DDEDDt(t,R,u);
  MatT HRef=DDEDDtRef(t,R,u);
  DEBUG_GRADIENT("DDEDDt",HRef.norm(),(HRef-H).norm())
  //DEBUG_GRADIENT("DDEDDt-uu",HRef.block(0,0,u.size(),u.size()).norm(),(HRef-H).block(0,0,u.size(),u.size()).norm())
  //DEBUG_GRADIENT("DDEDDt-vu",HRef.block(u.size(),0,3,u.size()).norm(),(HRef-H).block(u.size(),0,3,u.size()).norm())
  //DEBUG_GRADIENT("DDEDDt-uv",HRef.block(0,u.size(),u.size(),3).norm(),(HRef-H).block(0,u.size(),u.size(),3).norm())
  //DEBUG_GRADIENT("DDEDDt-vv",HRef.block(3,3,u.size(),u.size()).norm(),(HRef-H).block(3,3,u.size(),u.size()).norm())
  //DEBUG_GRADIENT("DDEDDt-vw",HRef.block(u.size(),u.size()+3,3,3).norm(),(HRef-H).block(u.size(),u.size()+3,3,3).norm())
  //DEBUG_GRADIENT("DDEDDt-wv",HRef.block(u.size()+3,u.size(),3,3).norm(),(HRef-H).block(u.size()+3,u.size(),3,3).norm())
  //DEBUG_GRADIENT("DDEDDt-uw",HRef.block(0,u.size()+3,u.size(),3).norm(),(HRef-H).block(0,u.size()+3,u.size(),3).norm())
  //DEBUG_GRADIENT("DDEDDt-wu",HRef.block(u.size()+3,0,3,u.size()).norm(),(HRef-H).block(u.size()+3,0,3,u.size()).norm())
  //DEBUG_GRADIENT("DDEDDt-ww",HRef.block(u.size()+3,u.size()+3,3,3).norm(),(HRef-H).block(u.size()+3,u.size()+3,3,3).norm())
}
template <typename T>
typename FEMQuadraticFST<T>::Mat3XT FEMQuadraticFST<T>::IRHB(const Mat3T& R) const {
  Mat3XT ret=Mat3XT::Zero(3,_B.cols());
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=_IRHB(r,c)*R(r,c);
  return ret;
}
template <typename T>
typename FEMQuadraticFST<T>::Vec3T FEMQuadraticFST<T>::pRH1(const Mat3T& R,const Vec& u) const {
  Vec3T ret=Vec3T::Zero();
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      ret+=(_pRH1(r,c).transpose().block(0,0,3,u.size())*u+_pRH1(r,c).transpose().col(u.size()))*R(r,c);
  return ret;
}
template <typename T>
typename FEMQuadraticFST<T>::Vec FEMQuadraticFST<T>::BRHp(const Mat3T& R,const Vec& u) const {
  Vec ret=Vec::Zero(u.size());
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++) {
      const MatT& m=_pRHp(r,c);
      ret+=(m.block(0,0,u.size(),u.size())*u)*R(r,c);
      ret+=m.col(u.size()).segment(0,u.size())*R(r,c);
    }
  return ret;
}
template <typename T>
T FEMQuadraticFST<T>::pRHp(const Mat3T& R,const Vec& u) const {
  T ret=0;
  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++) {
      const MatT& m=_pRHp(r,c);
      ret+=u.dot(m.block(0,0,u.size(),u.size())*u)*R(r,c);
      ret+=m.row(u.size()).segment(0,u.size()).dot(u)*R(r,c);
      ret+=m.col(u.size()).segment(0,u.size()).dot(u)*R(r,c);
      ret+=m(u.size(),u.size())*R(r,c);
    }
  return ret;
}
//instance
template class FEMLinearFST<FLOAT>;
template class FEMQuadraticFST<FLOAT>;
template class FEMNoninertialFrame<FLOAT>;
}
