#ifndef DACG_STAB_H
#define DACG_STAB_H

#include <Utils/SparseUtils.h>
#include <Utils/IO.h>
#include <memory>

namespace PHYSICSMOTION {
template <typename T>
struct KrylovMatrix {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  virtual ~KrylovMatrix() {}
  virtual Vec operator*(const Vec& b) const {
    Vec tmp(b.size());
    multiply(b,tmp);
    return tmp;
  }
  virtual Vec solve(const Vec& b) const {
    Vec tmp(b.size());
    multiply(b,tmp);
    return tmp;
  }
  virtual bool compute(const Eigen::SparseMatrix<T,0,int>& m) {
    return setMatrix(m);
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m)=0;
  virtual void multiply(const Vec& b,Vec& out) const=0;
  virtual int n() const=0;
  virtual Eigen::ComputationInfo info() const {
    return Eigen::Success;
  }
};
template <typename T>
struct EigenKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  EigenKrylovMatrix() {}
  EigenKrylovMatrix(const Eigen::SparseMatrix<T,0,int>& m):_m(&m) {}
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    _m=&m;
    return true;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out=*_m*b;
  }
  virtual int n() const override {
    return _m->rows();
  }
 protected:
  const Eigen::SparseMatrix<T,0,int>* _m;
};
template <typename T>
struct ReferenceKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  ReferenceKrylovMatrix():_ref(NULL) {}
  virtual void setReference(const KrylovMatrix<T>& ref) {
    _ref=&ref;
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>&) override {
    ASSERT_MSG(_ref,"Reference krylov matrix not set")
    //_ref->setMatrix(m);
    return true;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    ASSERT_MSG(_ref,"Reference krylov matrix not set")
    _ref->multiply(b,out);
  }
  virtual int n() const override {
    ASSERT_MSG(_ref,"Reference krylov matrix not set")
    return _ref->n();
  }
 protected:
  const KrylovMatrix<T>* _ref;
};
template <typename T>
struct IdentityKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  IdentityKrylovMatrix() {}
  IdentityKrylovMatrix(int n):_n(n) {}
  virtual bool setMatrix(int n) {
    _n=n;
    return true;
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    _n=m.rows();
    return true;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out=b;
  }
  virtual int n() const override {
    return _n;
  }
 protected:
  int _n;
};
template <typename T>
struct DiagonalKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  DiagonalKrylovMatrix() {}
  DiagonalKrylovMatrix(const Eigen::SparseMatrix<T,0,int>& m) {
    setMatrix(m);
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    _diag.setZero(m.rows());
    for(int k=0; k<m.outerSize(); ++k)
      for(typename Eigen::SparseMatrix<T,0,int>::InnerIterator it(m,k); it; ++it)
        if(it.row()==it.col())
          _diag[it.row()]=it.value();
    return true;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out.array()=b.array()*_diag.array();
  }
  virtual int n() const override {
    return _diag.size();
  }
 protected:
  Vec _diag;
};
template <typename T>
struct InvDiagonalKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  InvDiagonalKrylovMatrix():_eps(1e-10f) {}
  InvDiagonalKrylovMatrix(const Eigen::SparseMatrix<T,0,int>& m):_eps(1e-10f) {
    setMatrix(m);
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    _diag.setZero(m.rows());
    for(int k=0; k<m.outerSize(); ++k)
      for(typename Eigen::SparseMatrix<T,0,int>::InnerIterator it(m,k); it; ++it)
        if(it.row()==it.col())
          _diag[it.row()]=1/std::max(_eps,it.value());
    return true;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out.array()=b.array()*_diag.array();
  }
  virtual int n() const override {
    return _diag.size();
  }
  void setEps(T eps) {
    _eps=eps;
  }
 protected:
  Vec _diag;
  T _eps;
};
//INNER_SOLVER can be:
//Eigen::IncompleteLUT<T,int>
//Eigen::IncompleteCholesky<T,Eigen::Lower,Eigen::AMDOrdering<int>>
template <typename T,typename INNER_SOLVER>
struct DelegateKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    _I.compute(m);
    return _I.info()==Eigen::Success;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out=_I.solve(b);
  }
  virtual int n() const override {
    return _I.rows();
  }
  virtual Eigen::ComputationInfo info() const override {
    return _I.info();
  }
 protected:
  INNER_SOLVER _I;
};
template <typename T,typename INNER_SOLVER>
struct KKTKrylovMatrix : public KrylovMatrix<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  //this is a constrained preconditioner that takes the matrix:
  //[A UT]
  //[U 0 ]
  KKTKrylovMatrix(bool shiftOffDiag=true):_UTDense(NULL),_UTSparse(NULL),_solUUT(NULL),_shiftOffDiag(shiftOffDiag) {}
  virtual void setUT(const Eigen::SparseMatrix<T,0,int>* UT,const Eigen::LLT<MatT>* solUUT=NULL) {
    _UTDense=NULL;
    _UTSparse=UT;
    _solUUT=solUUT;
  }
  virtual void setUT(const MatT* UT,const Eigen::LLT<MatT>* solUUT=NULL) {
    _UTDense=UT;
    _UTSparse=NULL;
    _solUUT=solUUT;
  }
  virtual bool setMatrix(const Eigen::SparseMatrix<T,0,int>& m) override {
    if(_UTSparse==NULL && _UTDense==NULL)
      _I.compute(m);
    else {
      Vec shiftOffDiag;
      if(_shiftOffDiag) {
        if(_UTSparse!=NULL)
          shiftOffDiag=_UTSparse->transpose().cwiseAbs()*Vec::Ones(m.rows());
        else
          shiftOffDiag=_UTDense->transpose().cwiseAbs()*Vec::Ones(m.rows());
      }
      if(_UTSparse!=NULL)
        _mKKT=buildKKT<T,0,int>(m,*_UTSparse,0,true,_shiftOffDiag?&shiftOffDiag:NULL);
      else
        _mKKT=buildKKT<T,0,int>(m,_UTDense->sparseView(),0,true,_shiftOffDiag?&shiftOffDiag:NULL);
      _mKKT.makeCompressed();
      _I.compute(_mKKT);
    }
    _n=m.rows();
    return _I.info()==Eigen::Success;
  }
  virtual void multiply(const Vec& b,Vec& out) const override {
    out=b;
    if(_solUUT)
      projectOut(out);
    if(_UTSparse==NULL && _UTDense==NULL)
      out=_I.solve(out);
    else if(_UTDense!=NULL)
      out=_I.solve(concat(out,Vec::Zero(_UTDense->cols()))).segment(0,_n);
    else
      out=_I.solve(concat(out,Vec::Zero(_UTSparse->cols()))).segment(0,_n);
    if(_solUUT)
      projectOut(out);
  }
  virtual int n() const override {
    return _n;
  }
  virtual Eigen::ComputationInfo info() const override {
    return _I.info();
  }
  void projectOut(Vec& out) const {
    if(_UTDense)
      out-=*_UTDense*_solUUT->solve(_UTDense->transpose()*out);
    else out-=*_UTSparse*_solUUT->solve(_UTSparse->transpose()*out);
  }
 protected:
  const MatT* _UTDense;
  const Eigen::SparseMatrix<T,0,int>* _UTSparse;
  const Eigen::LLT<MatT>* _solUUT;
  Eigen::SparseMatrix<T,0,int> _mKKT;
  bool _shiftOffDiag;
  INNER_SOLVER _I;
  int _n;
};
template <typename T,typename PRECONDITIONER_TYPE>
class DACGSolver {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  DACGSolver(void) {
    setSolverParameters(1e-20f,1e-5f,100000);
    _interval=10;
  }
  void setSolverParameters(T eps1,T eps2,int maxIterations) {
    _eps1=std::min<T>(std::max<T>(eps1,0.0f),1.0f);
    _eps2=std::min<T>(std::max<T>(eps2,0.0f),1.0f);
    _maxIter=std::max<int>(maxIterations,100);
    _maxIterInner=1000;
  }
  PRECONDITIONER_TYPE& preconditioner() {
    return _pre;
  }
  const PRECONDITIONER_TYPE& preconditioner() const {
    return _pre;
  }
  void resize(const int& n) {
    if(_xk.size() != n) {
      _xk.resize(n);
      _xkA.resize(n);
      _xkB.resize(n);

      _pk.resize(n);
      _pkA.resize(n);
      _pkB.resize(n);
      _pkTilde.resize(n);

      _gk.resize(n);
      _gkM.resize(n);
    }
  }
  bool setA(const Eigen::SparseMatrix<T,0,int>& A) {
    resize(A.rows());
    _A.reset(new EigenKrylovMatrix<T>(A));
    return _pre.setMatrix(A);
  }
  void setB(const Eigen::SparseMatrix<T,0,int>& B) {
    resize(B.rows());
    _B.reset(new EigenKrylovMatrix<T>(B));
  }
  void setU0(const MatT& U0) {
    _U0=U0;
    if(!_B)
      _B.reset(new IdentityKrylovMatrix<T>(_A->n()));
    Vec UC(_B->n()),UCB(_B->n());
    for(int eig=1; eig<=_U0.cols(); eig++) {
      std::cout << eig << "th Basis" << std::endl;
      projectOut(_U0,eig,_U0.col(eig-1),UC,false);
      _B->multiply(UC,UCB);
      _U0.col(eig-1)=UC/sqrt(UC.dot(UCB));
      //check orthogonality
      for(int j=1; j<eig; j++) {
        _B->multiply(_U0.col(eig-1),UCB);
        std::cout << "\tOrthogonal residual: " << UCB.dot(_U0.col(j-1)) << std::endl;
      }
    }
  }
  void setCallbackInterval(int interval) {
    _interval=interval;
    ASSERT(interval>=1)
  }
  void checkSolution(Vec& lambda,MatT& U) const {
    checkMatrix();
    if(lambda.rows() != U.cols() || U.rows() != _A->n()) {
      std::cout << "Incorrect input size!" << std::endl;
      exit(EXIT_FAILURE);
    }
    Vec Ax,lambdaBx,err;
    Ax.resize(U.rows());
    lambdaBx.resize(U.rows());
    err.resize(U.rows());
    for(int r=0; r<lambda.rows(); r++) {
      _A->multiply(U.col(r),Ax);
      _B->multiply(U.col(r),lambdaBx);
      lambdaBx*=lambda[r];
      err=Ax-lambdaBx;
      std::cout << "lambda: " << lambda[r] << ", Ax: " << Ax.norm() << ", lambdaBx: " << lambdaBx.norm() << ", err: " << err.norm() << std::endl;
    }
  }
  bool solve(int s,Vec& lambda,MatT& U) {
    if(!_B)
      _B.reset(new IdentityKrylovMatrix<T>(_A->n()));
    checkMatrix();
    int n=_A->n();
    //adjust
    {
      s=std::min<int>(s,_A->n());
      std::cout << "User want " << s << " eigenpairs" << std::endl;
      if(lambda.size() < s)
        lambda.resize(s);
      if(U.rows() != n || U.cols() < s)
        U.resize(n,s);
      _iterCount.resize(s);
    }
    //compute
    for(int eig=1; eig<=s; eig++) {
      std::cout << "Computing the " << eig << "th pair" << std::endl;
      findInit(U,eig);
      T qk,eta;
      int iter=0;
      while(true) {
        int ret=solve(U,eig,iter,qk,eta);
        if(ret==1)
          break;
        else if(ret==0)
          return false;
        _xk/=_xk.norm();
      }

      _iterCount[eig-1]=iter;
      lambda[eig-1]=qk;
      U.col(eig-1)=_xk/sqrt(eta);
    }
    return true;
  }
  int solve(const MatT& U,int eig,int& iter,T& qk,T& eta) {
    T betak,gamma,qkLast=0.0f,alphak=0.0f;
    T gk_gkM_Last=0.0f,gk_gkM=0.0f;
    T thres1=0.0f,thres2=0.0f;
    //step 1
    betak=0.0f;
    _pk=_xk;
    projectOut(U,eig,_pk,_xk);
    _pk.setZero();
    //step 2
    _A->multiply(_xk,_xkA);
    _B->multiply(_xk,_xkB);
    gamma=_xk.dot(_xkA);
    eta=_xk.dot(_xkB);
    qk=gamma/eta;
    //step
    int innerIter=0;
    for(; innerIter<_maxIterInner && iter<_maxIter; innerIter++,iter++) {
      //3.1
      _gk=_xkA;
      _gk-=qk*_xkB;
      {
        //3.11 part2
        if(_kryU0)
          _gk=_kryU0->operator*(_gk);
        thres2=_gk.norm()/sqrt(gamma);
        callback(_xk,thres2,iter);
        if(thres2 < _eps2)
          break;
      }
      _gk*=2.0f/eta;
      //3.2
      T gk_gkMLast=_gk.dot(_gkM);
      if(_kryU0)
        _gkM=_gk;
      else _pre.multiply(_gk,_gkM);
      //3.3
      gk_gkM=_gk.dot(_gkM);
      if(innerIter>0)
        betak=(gk_gkM-gk_gkMLast)/gk_gkM_Last;
      gk_gkM_Last=gk_gkM;
      //3.4
      _pkTilde=_gkM;
      _pkTilde+=betak*_pk;
      //3.5
      projectOut(U,eig,_pkTilde,_pk);
      //3.6
      _A->multiply(_pk,_pkA);
      _B->multiply(_pk,_pkB);
      //3.7
      {
        T xAp=_pk.dot(_xkA);
        T pAp=_pk.dot(_pkA);
        T xBp=_pk.dot(_xkB);
        T pBp=_pk.dot(_pkB);

        T a=xBp*pAp-xAp*pBp;
        T b=eta*pAp-gamma*pBp;
        T c=eta*xAp-gamma*xBp;
        T maxDenom=std::max(fabs(a),std::max(fabs(b),fabs(c)));
        a/=maxDenom;
        b/=maxDenom;
        c/=maxDenom;

        T delta=b*b-4.0f*a*c;
        if(b > 0.0f)
          alphak=-2.0f*c/(b+sqrt(delta));
        else alphak=(-b+sqrt(delta))/(2.0f*a);

        //efficient method for updating gamma and eta,
        //but we use more stable version below
//#define CHEAP_UPDATE_GAMMA_ETA
#ifdef CHEAP_UPDATE_GAMMA_ETA
        gamma=gamma+2.0f*xAp*alphak+pAp*alphak*alphak;
        eta=eta+2.0f*xBp*alphak+pBp*alphak*alphak;
#endif
      }
      //3.8
      _xk+=alphak*_pk;
      _xkA+=alphak*_pkA;
      _xkB+=alphak*_pkB;
#ifndef CHEAP_UPDATE_GAMMA_ETA
      gamma=_xkA.dot(_xk);
      eta=_xkB.dot(_xk);
#endif
      //3.9
      if(gamma > 1E30f || eta > 1E30f)
        return -1;
      //3.10
      qk=gamma/eta;
      //INFOV("Update qk: %f",qk)
      if(qk < 0.0f) {
        if(gamma < 0.0f)
          std::cout << "A not positive definite because we found x^TAx=" << gamma << std::endl;
        if(eta < 0.0f)
          std::cout << "B not positive definite because we found x^TBx=" << eta << std::endl;
#ifdef CHEAP_UPDATE_GAMMA_ETA
        return -1;
#else
        {
          std::ofstream os("errorDACG.dat",std::ios::binary);
          writeBinaryData(_xk,os);
        }
        exit(EXIT_FAILURE);
#endif
      }
      //3.11 part1
      thres1=fabs(qk-qkLast)/qk;
      if(thres1 < _eps1)
        return -1;
      qkLast=qk;
    }
    if(iter == _maxIter)
      return 0;
    if(innerIter == _maxIterInner)
      return -1;
    return 1;
  }
  void callback(const Vec&,T thres,int it) const {
    if(it%_interval!=0)
      return;
    std::cout << "\tIter=" << it << " thres=" << thres << std::endl;
  }
  void findInit(const MatT& U,int nr) {
    Vec tmp=_xk;
    while(true) {
      tmp.setRandom();
      projectOut(U,nr,tmp,_xk);
      if(_xk.norm() > 1E-3f)
        break;
    }
    //check orthogonality
    _B->multiply(_xk,tmp);
    for(int i=0; i<nr-1; i++) {
      std::cout << "\tOrthogonal residual: " << U.col(i).dot(tmp) << std::endl;
    }
  }
  void projectOut(const MatT& U,int nr,const Vec& from,Vec& to,bool includeU0=true) const {
    Vec tmp=from;
    to=from;
    _B->multiply(to,tmp);
    if(includeU0)
      for(int i=0; i<_U0.cols(); i++)
        to-=_U0.col(i).dot(tmp)*_U0.col(i);
    for(int i=0; i<nr-1; i++)
      to-=U.col(i).dot(tmp)*U.col(i);
  }
  void setKrylovU0(std::shared_ptr<KrylovMatrix<T>> U0) {
    _kryU0=U0;
  }
  const std::vector<int>& getIterationsCount() const {
    return _iterCount;
  }
  void checkMatrix() const {
    if(!_A || !_B) {
      std::cout << "Matrix A or B not provieded!" << std::endl;
      exit(EXIT_FAILURE);
    }
    if(_A->n() != _B->n()) {
      std::cout << "Matrix A and B are of different size!" << std::endl;
      exit(EXIT_FAILURE);
    }
  }
 protected:
  PRECONDITIONER_TYPE _pre;
  std::shared_ptr<KrylovMatrix<T> > _A,_B;
  std::shared_ptr<KrylovMatrix<T> > _kryU0;
  MatT _U0;
  //data
  T _eps1,_eps2;
  int _maxIter,_maxIterInner;
  int _interval;
  //temporary
  Vec _xk,_xkA,_xkB;
  Vec _pk,_pkA,_pkB,_pkTilde;
  Vec _gk,_gkM;
  std::vector<int> _iterCount;
};
}

#endif
