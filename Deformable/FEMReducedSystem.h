#ifndef FEM_REDUCED_SYSTEM_H
#define FEM_REDUCED_SYSTEM_H

#include "FEMNoninertialFrame.h"
#include "FEMSystem.h"
#include "NNHTP.h"

namespace PHYSICSMOTION {
template <typename T>
struct FEMGradientInfo;
template <typename T>
class FEMReducedSystem : public FEMSystem<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(FEMSystem<T>)
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef typename NNHTP<T>::W CUBATURES;
#ifndef SWIG
  using typename FEMSystem<T>::SOLVER;
  using FEMSystem<T>::SP;
  using FEMSystem<T>::LCP;
  using FEMSystem<T>::getEnergy;
  using FEMSystem<T>::getMass;
  using FEMSystem<T>::_proj;
  using FEMSystem<T>::_mesh;
  using FEMSystem<T>::_energies;
  using FEMSystem<T>::_env;
  using FEMSystem<T>::_dampingCoef;
  using FEMSystem<T>::_dt;
  using FEMSystem<T>::_mu;
  using FEMSystem<T>::_maxContact;
  using FEMSystem<T>::_mass;
  using FEMSystem<T>::_simplifier;
  using FEMSystem<T>::_selfCC;
  using FEMSystem<T>::_selfCCoef;
  using FEMSystem<T>::_selfCCDebugOutputPath;
#endif
  FEMReducedSystem();
  FEMReducedSystem(std::shared_ptr<FEMMesh<T>> mesh);
  virtual ~FEMReducedSystem();
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  std::shared_ptr<FEMVertex<T>> getSV(int i) const;
  void debugDStiffness(int iter=10,T deltaPos=1e-2f) const;
  std::shared_ptr<FEMVertex<T>> getReducedVertex(std::shared_ptr<FEMVertex<T>> v) const;
  virtual void setFluidEnergy(T level,T rho,T G,T dragMult=1,int deg=1) override;
  std::vector<std::shared_ptr<FEMEnergy<T>>> getReducedEnergy() const;
  int getReducedVertexOff(std::shared_ptr<FEMVertex<T>> v) const;
  bool buildBasis(int maxBases);
  int nFirstOrderBasis() const;
  void setBasis(const MatT& U);
  const MatT& getBasisInterp() const;
  const MatT& getBasis() const;
  const Vec& getPos0Interp() const;
  const Vec& getPos0() const;
  const MatT& getUTMU() const;
  MatT getUTKU() const;
  T minEigenvalue() const;
  void minEigenvalue(T e);
  virtual void addSelfCC(T coef) override;
  //simulate
  virtual void updateFluid(const Vec& pN,std::vector<FEMEnergyContext<T>>& context) const;
  virtual void updateFluid(const FEMGradientInfo<T>& IN,std::vector<FEMEnergyContext<T>>& context) const;
#ifndef SWIG
  virtual void detectCollision(const Vec& p,std::vector<FEMCollision<T>>& colls) const override;
  virtual void detectCollision(const FEMGradientInfo<T>& I,std::vector<FEMCollision<T>>& colls) const;
  void debugDDxDtDu(const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver) const;
  virtual bool step(const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver,std::vector<FEMCollision<T>>* collsRet=NULL,MatT* DDxDtDu=NULL) const;
  virtual bool step(const FEMGradientInfo<T>& IN,FEMGradientInfo<T>& I,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver,bool wantDDxDtDu=false) const;
#endif
  virtual Vec step(const Vec& pN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver,std::vector<FEMCollision<T>>* collsRet=NULL,MatT* DDxDtDu=NULL) const;  //for python
  virtual FEMGradientInfo<T> step(const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver,bool wantDDxDtDu=false) const;  //for python
  virtual std::vector<std::pair<FEMGradientInfo<T>,bool>> stepMultiple(const std::vector<FEMGradientInfo<T>>& IN,std::vector<std::shared_ptr<FEMPDController<T>>> ctrl,SOLVER solver,bool wantDDxDtDu=false) const;  //for python
 protected:
  bool assemble(Vec& GRHS,MatT& GLHS,const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,std::vector<std::shared_ptr<FEMEnergy<T>>>* ess=NULL) const;
  static void makeOrthogonal(MatT& U);
  void factorOutTranslation(MatT& U) const;
  bool hasFlippedElement(const Vec& p) const;
  void buildReducedEnergies();
  void buildCubature(int batchSize=100,T acceptableReduction=0.0001f,bool debug=true);
  bool findFirstOrderBasis(int n,Vec& eval,MatT& evec,bool removeRigidMode=true);
  bool findSecondOrderBasisFree(int n,const Vec& eval,MatT& evec);
  bool findSecondOrderBasisFixed(int n,MatT& evec);
  MatT cellwiseForceVolume(const Vec& p);
  SMatT DStiffness(const Vec& p,const Vec& dir) const;
  SMatT stiffness(const Vec& p) const;
  bool isLinearStiffness() const;
  bool _buildCellVertex;    //cannot be modified by user
  //data
  T _minEigenvalue;
  Vec _pos0,_pos0Interp;
  MatT _U,_UInterp,_UTMU,_UTKU;
  CUBATURES _cubatures;
  //temporary data
  std::shared_ptr<FEMLinearFST<T>> _linearEnergy;
  std::shared_ptr<FEMQuadraticFST<T>> _quadraticEnergy;
  std::shared_ptr<FEMNoninertialFrame<T>> _noninertialFrame;
  std::vector<std::shared_ptr<FEMEnergy<T>>> _reducedEnergies;
  std::vector<std::shared_ptr<FEMVertex<T>>> _reducedVertices;
  //temporary data: cell vertex
  //cell vertices are not build until a call to getReducedCellVertex()
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>> _mappedVertex;
};
}

#endif
