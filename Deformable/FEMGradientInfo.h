#ifndef FEM_GRADIENT_INFO_H
#define FEM_GRADIENT_INFO_H

#include "FEMCollision.h"
#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
template <typename T>
class FEMReducedSystem;
template <typename T>
struct FEMGradientInfo {
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  FEMGradientInfo();
  FEMGradientInfo(const MatT& B,const MatT& BI,const Vec& pos0L,const Vec& x);
  FEMGradientInfo(const FEMReducedSystem<T>& sys,const Vec& x);
  FEMGradientInfo(std::shared_ptr<FEMReducedSystem<T>> sys,const Vec& x);
#ifndef SWIG
  FEMGradientInfo(const FEMReducedSystem<T>& sys,VecCM x);
  void reset(MatTCM B,MatTCM BI,VecCM pos0L,VecCM x);
  void reset(const FEMReducedSystem<T>& sys,VecCM x);
#endif
  void reset(std::shared_ptr<FEMReducedSystem<T>> sys,const Vec& x);
#ifndef SWIG
  FEMGradientInfo integrate(MatTCM B,MatTCM BI,VecCM pos0L,T dt,const Vec* a,const Vec* vNew) const;
#endif
  FEMGradientInfo integrate(const FEMReducedSystem<T>& sys,T dt,const Vec* a,const Vec* vNew) const;
  FEMGradientInfo integrate(std::shared_ptr<FEMReducedSystem<T>> sys,T dt,const Vec* a,const Vec* vNew) const;
#ifndef SWIG
  VecCM getDOF() const;
  VecCM getDDOFDt() const;
  VecCM getQInterp() const;
  VecCM getQInterpL() const;
  Vec getDQInterpDt(MatTCM BI) const;
  Vec getDQInterpLDt(MatTCM BI) const;
  Mat3XT getQInterpJac(MatTCM BI,int off) const;
#endif
  bool isNoninertial() const;
  const Vec& getX() const;
  const Vec3T getVPos(int off) const;
  const Vec3T getVPosLocal(int off) const;
  const Mat3T& getR() const;
  Vec3T getT() const;
  Vec getU() const;
  Vec getDUDt() const;
  Vec3T getVLocal() const;
  Vec3T getV();
  Vec3T getWLocal() const;
  Vec3T getW() const;
  const MatT& getDDxDtDu() const;
  MatT getDvwDu() const;
  MatT getDvwDuLocal() const;
  void setDDxDtDu(const MatT& DDxDtDu);
  std::vector<FEMCollision<T>>& getCollisions();
  static void debug(int N,int N2,int M);
 private:
  Vec integrate(T dt,const Vec* a,const Vec* vNew) const;
  //data
  std::vector<FEMCollision<T>> _colls;
  Vec _x,_qInterpL,_qInterp;
  MatT _DDxDtDu;
  Mat3T _R;
  int _nB;
};
}

#endif
