import sys, math
import numpy as np

BASE = "../../../omtg-build"
sys.path.append(BASE)
import pyPhysicsMotion as pm


class ControllerResetter:
    def __init__(self, ctrl_type='P', ctrl_reduced='', k=None, design=None):
        self.ctrl_type = ctrl_type
        self.ctrl_reduced = ctrl_reduced
        self.controller = getattr(pm, "FEM" + ctrl_reduced + "PDControllerFLOAT")()
        signal_num = 2 if ctrl_type == 'PD' else 1
        self.k = [0.] * signal_num if k is None else k
        self.desired_values = [0.] * signal_num
        self.design = [0.] * signal_num if design is None else design

    def resetP(self):
        self.controller.resetP(self.desired_values[0], self.k[0])

    def resetD(self):
        self.controller.resetD(self.desired_values[0], self.k[0])

    def resetPD(self):
        self.controller.resetPD(self.desired_values[0], self.k[0],
                                self.desired_values[1], self.k[1])

    def resetPReduced(self):
        self.controller.resetPReduced(self.desired_values[0], self.k[0], self.design[0])

    def resetDReduced(self):
        self.controller.resetDReduced(self.desired_values[0], self.k[0], self.design[0])

    def resetPDReduced(self):
        self.controller.resetPDReduced(self.desired_values[0], self.k[0], self.desired_values[1], self.k[1],
                                       self.design[0])

    def copy(self):
        return ControllerResetter(self.ctrl_type, self.ctrl_reduced, self.k, self.design)


def createBody(body_type=0, mesh_name="torus.obj_tet.mesh"):
    mesh_file_name = BASE + "/" + mesh_name
    if body_type == -1:
        body = pm.FEMMeshFLOAT(True, mesh_file_name, True)
    elif body_type == 0:
        body = pm.FEMMeshFLOAT(mesh_file_name, True)
    elif body_type == 1:
        body = pm.FEMMeshFLOAT(mesh_file_name, .05, True)
    else:
        body = pm.FEMMeshFLOAT(mesh_file_name, .05, True)
        bodyHOct = FEMOctreeMeshFLOAT(body, 0)
        body = bodyHOct.getMesh()
    return body


def createTerrain(world, cur_terrain_type, terrain, gravity=9.81, plane_level=-0.01, plane_size=5.0):
    rot = np.zeros(3)
    norm = np.array([0., 0., 1.])
    if cur_terrain_type == "Fluid":
        world.setFluidEnergy(terrain[0], terrain[1], gravity, terrain[2], deg=0)
        plane = np.array([0.0, 0.0, 1.0, -1.0 * (terrain[0] - 1.0)])
        world.getEnv().createFloor(np.dot(0.001, plane))
    elif cur_terrain_type == "Plane":
        plane = np.array([0.0, 0.0, 1.0, -plane_level / 1.0])
        world.getEnv().createFloor(np.dot(plane_size, plane))
    else:
        assert cur_terrain_type == "Slope"
        norm = np.array([-terrain[1], -terrain[2], 1.0])
        norm = norm / np.linalg.norm(norm)
        floor = np.append(norm, -plane_level / norm[2])
        world.getEnv().createFloor(np.dot(plane_size, floor))
        axis = np.cross(np.array([0, 0, 1]), norm)
        axis = axis / np.linalg.norm(axis)
        theta = math.acos(norm[2])
        rot = axis * theta
    return norm, rot


def sampleTerrain(world, rng, terrain_types, fluid_low, fluid_high, floor_low, floor_high,
                  gravity=9.81, plane_level=-0.01, plane_size=5.0):
    # randomly generate terrain
    rnd_num = rng.uniform(0.0, 1.0)
    cur_terrain_type = 'Empty'
    for i in range(0, len(terrain_types)):
        if 1.0 * i / len(terrain_types) <= rnd_num < 1.0 * (i + 1) / len(terrain_types):
            cur_terrain_type = terrain_types[i]
            break

    # fluid density, fluid level and drag coefficient is random
    assert cur_terrain_type != 'Empty'
    terrain = np.zeros(3)
    if cur_terrain_type == "Fluid":
        terrain = [rng.uniform(fluid_low[i], fluid_high[i]) for i in range(0, len(fluid_low))]
    # friction_mu is random
    else:
        mu = rng.uniform(floor_low[0], floor_high[0])
        world.mu(mu)
        if cur_terrain_type == "Plane":
            terrain = np.array([mu, 0., 0.])
        # a*x+b*y+c*z+d=0, a and b are random, c==1 and d== -self.plane_level
        if cur_terrain_type == "Slope":
            slope_x = rng.uniform(floor_low[1], floor_high[1])
            slope_y = rng.uniform(floor_low[2], floor_high[2])
            terrain = np.array([mu, slope_x, slope_y])
    norm, rot = createTerrain(world, cur_terrain_type, terrain, gravity, plane_level, plane_size)
    return cur_terrain_type, terrain, norm, rot


def sampleTargetDir(rng, phi_low, phi_high, theta_low, theta_high):
    theta_range = np.array([theta_low, theta_high])
    phi = rng.uniform(phi_low, phi_high)
    if math.fabs(theta_range[0] - theta_range[1]) < 1e-3:
        theta = 0.5 * (theta_range[0] + theta_range[1])
        sin_theta = math.sin(theta)
        cos_theta = math.cos(theta)
    else:
        cos_theta_range = np.cos(theta_range)
        cos_theta = rng.uniform(cos_theta_range[0], cos_theta_range[1])
        sin_theta = math.sqrt(1 - cos_theta * cos_theta)
    target_dir = np.array([sin_theta * math.cos(phi), sin_theta * math.sin(phi), cos_theta])
    return target_dir


def applyDirectControl(action, low, high, ctrl_reset):
    ctrl_dim = int(action.shape[-1] / len(ctrl_reset.desired_values))
    for i in range(len(ctrl_reset.desired_values)):
        ctrl_reset.desired_values[i] = 0.5 * (high[i] - low[i]) * action[(ctrl_dim * i):(ctrl_dim * (i + 1))].astype(
            np.double) + 0.5 * (high[i] + low[i])
    getattr(ctrl_reset, "reset" + ctrl_reset.ctrl_type + ctrl_reset.ctrl_reduced)()


def directSimulate(world, frame, controller, contact_solver, wantContact=False):
    if isinstance(frame, list):
        ret = world.stepMultiple(frame, controller, contact_solver, wantContact)
    else:
        ret = world.step(frame, controller, contact_solver, wantContact)
    return ret


def applyDesign(design, ctrl_reset):
    ctrl_reset.design = design


def isSimulationFailed(ret):
    sim_fail = not ret[1]
    if not sim_fail:
        sim_fail = sim_fail | \
                   ((np.abs(ret[0].getDUDt()) > 1e6).any()) | ((np.abs(ret[0].getV()) > 1e6).any()) | (
                       np.abs((ret[0].getW() > 1e6)).any()) | \
                   (np.isnan(ret[0].getDUDt()).any()) | (np.isnan(ret[0].getV()).any()) | (
                       np.isnan(ret[0].getW()).any()) | \
                   (np.isinf(ret[0].getDUDt()).any()) | (np.isinf(ret[0].getV()).any()) | (
                       np.isinf(ret[0].getW()).any())
    return sim_fail


def computeDirectionalReward(vec0, vec1, dir, tan, per, w_dir, w_tan, w_per):
    reward_directional = w_dir * np.dot(vec0, dir)
    reward_tangent = -w_tan * np.fabs(np.dot(vec1, tan))
    reward_perpendicular = -w_per * np.fabs(np.dot(vec1, per))
    reward = reward_directional + reward_tangent + reward_perpendicular
    return reward
