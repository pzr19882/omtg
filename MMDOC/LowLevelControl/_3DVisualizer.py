import numpy as np
import sys

from FEMUtils import BASE
sys.path.append(BASE)
import pyPhysicsMotion as pm
from pyTinyVisualizer import pyTinyVisualizer as vis

class _3DVisualizer:
    def __init__(self, env, scaleEnvTc=None):
        self.drawer = vis.Drawer(0, None)
        self.drawer.addPlugin(vis.CameraExportPlugin(vis.GLFW_KEY_2, vis.GLFW_KEY_3, "camera.dat"))
        self.drawer.addPlugin(vis.CaptureGIFPlugin(vis.GLFW_KEY_1, "record.gif", self.drawer.FPS()))
        # self.cb = None
        self.scaleEnvTc = scaleEnvTc
        self.env = env
        self.world = env.world
        self.bodyS = None
        self.bodiesS = []
        # arrowS for drawing arrow in our environment
        self.arrowS = None
        self.arrow_base = np.array([0., 0., 0], dtype=np.single)
        self.arrow_dir = np.array([1., 0., 0.], dtype=np.single)
        self.arrow_color = np.array([.8, .8, .8], dtype=np.float)
        self.fluidS = None
        self.envS = None
        self.cb = None
        self.envSTrans = None
        self.fluidSize = self.env.plane_size

    def drawEnv(self, Env=None):
        if Env is not None:
            if self.scaleEnvTc is not None:
                self.envS = pm.visualizeEnvironmentFLOAT(Env, self.scaleEnvTc)
            else:
                self.envS = pm.visualizeEnvironmentFLOAT(Env)
            self.envS.setColorAmbient(vis.GL_TRIANGLES, 1., 1., 1.)
            self.envS.setTexture(vis.drawGrid(10, 0.01, 0.03, np.array([1, 1, 1], dtype=np.single),
                                              np.array([135 / 255., 54 / 255., 0 / 255.], dtype=np.single)))
            self.envSTrans = vis.Bullet3DShape()
            self.envSTrans.addShape(self.envS)
            self.drawer.addShape(self.envSTrans)

    def addLight(self, lightPos=None, vec2=None, vec3=None, vec4=None):
        if lightPos is None:
            lightPos = [0, 0, 1]
        self.drawer.addLightSystem()
        self.drawer.getLight().lightSz(10)
        self.drawer.getLight().addLight(np.array(lightPos, dtype=np.single),
                                        np.array(vec2, dtype=np.single),
                                        np.array(vec3, dtype=np.single),
                                        np.array(vec4, dtype=np.single))

    def addCallBack(self):
        if self.cb is None:
            return
        else:
            self.cb.bodyS = self.bodyS
            self.cb.bodiesS = self.bodiesS
            self.cb.arrowS = self.arrowS
            self.cb.envS = self.envS
            if self.cb.envS is not None:
                self.cb.envSTrans = self.envSTrans
            self.drawer.setPythonCallback(self.cb)
            self.drawer.addPlugin(vis.ImGuiPlugin(self.cb))

    def addCamera(self):
        self.drawer.addCamera3D(90, np.array([0, 0, 1], dtype=np.single))
        self.drawer.getCamera3D().setManipulator(vis.FirstPersonCameraManipulator(self.drawer.getCamera3D()))

    def setBackground(self, lightPos=None):
        if lightPos is None:
            lightPos = [0, 0, 1]
        assert (self.world is not None)
        #Body (only valid when drawing reduced body)
        self.bodyS = pm.visualizeFEMLowDimensionalMeshSurfaceFLOAT(self.world)
        self.drawer.addShape(self.bodyS)
        #Light
        self.addLight(lightPos, [.3, .3, .3], [.8, .8, .8], [.5, .5, .5])
        #Camera
        self.addCamera()
        #Callback
        self.addCallBack()

    def addArrow(self):
        assert self.arrowS is None
        self.arrowS = vis.ArrowShape(60, .03, .06)
        self.drawer.addShape(self.arrowS)
        self.arrowS.setColorAmbient(vis.GL_TRIANGLES, self.arrow_color[0], self.arrow_color[1], self.arrow_color[2])
        self.arrowS.setArrow(self.arrow_base, self.arrow_base + self.arrow_dir)

    def switchArrow(self, base=None, dir=None, color=None):
        if base is not None:
            self.arrow_base = base.astype(np.single)
        if dir is not None:
            self.arrow_dir = dir.astype(np.single)
        if color is not None:
            self.arrow_color = color
        self.arrowS.setColorAmbient(vis.GL_TRIANGLES, self.arrow_color[0], self.arrow_color[1], self.arrow_color[2])
        self.arrowS.setArrow(self.arrow_base, self.arrow_base + self.arrow_dir)

    def switchTerrain(self, terrain='Empty', fluid_level=0.0, Env=None):
        # remove old waterlevel and env
        if self.fluidS is not None:
            self.drawer.removeShape(self.fluidS)
            self.fluidS = None
        if self.envSTrans is not None:
            self.drawer.removeShape(self.envSTrans)
            self.envSTrans = None
        # water level
        if terrain == "Fluid":
            self.fluidS = pm.visualizeFEMWaterLevelFLOAT(fluid_level, self.fluidSize)
            self.drawer.addShape(self.fluidS)
        # env
        self.drawEnv(Env)
