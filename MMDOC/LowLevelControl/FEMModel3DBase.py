import os, sys
import numpy as np

from LowLevelControl.FEMUtils import BASE, createBody

sys.path.append(BASE)
import pyPhysicsMotion as pm


class FEMModel3DBase:
    def __init__(self,
                 # mesh & deformable material
                 body_type=0, mesh_name='torus.obj_tet.mesh', elastic_energy='StVK', mtl_lambda=10000., mtl_mu=10000.,
                 # model reduction
                 basis_num=56,
                 # contact solver
                 contact_solver_type='SP',
                 # basic parameters of physical setting
                 gravity=9.81, damp=1.0, max_contact=10, dt=0.001,
                 # basic parameters of customized terrain
                 plane_size=5.0, plane_level=-0.15,
                 # render related
                 x_render_res=0.1, y_render_res=0.1):

        # mesh & deformable material
        self.body_type = body_type
        self.mesh_name = mesh_name
        self.name = self.__class__.__name__ + "_" + mesh_name
        self.elastic_energy = elastic_energy
        self.mtl_lambda = mtl_lambda
        self.mtl_mu = mtl_mu
        # model reduction
        self.basis_num = basis_num
        # contact solver
        self.contact_solver = getattr(pm.FEMSystemFLOAT, contact_solver_type)
        # basic parameters of physical setting
        self.gravity = gravity
        self.damp = damp
        self.max_contact = max_contact
        self.dt = dt
        # basic parameters of customized terrain
        self.plane_size = plane_size
        self.plane_level = plane_level
        # render_related
        self.x_render_res = x_render_res
        self.y_render_res = y_render_res
        self.viewer = None
        # eval
        self.world = self.createWorld()
        self.cur_terrain_type = 'Empty'
        self.basis_num = self.world.getBasis().shape[-1]

    def createWorld(self):
        # add elastic energy for the agent to build proper basis
        if os.path.exists(BASE + "/" + self.mesh_name + "_" + str(self.basis_num) + "Basis.dat"):
            world = pm.FEMReducedSystemFLOAT()
            world.readStr(BASE + "/" + self.mesh_name + "_" + str(self.basis_num) + "Basis.dat")
        else:
            mesh = createBody(self.body_type, self.mesh_name)
            world = pm.FEMReducedSystemFLOAT(mesh)
            getattr(world, "add" + self.elastic_energy + "ElasticEnergy")(self.mtl_lambda, self.mtl_mu)
            world.addGravitationalEnergy(self.gravity)
            world.addFluidEnergy(-100.0, 0.0, 0.0, 0.0, deg=0)
            world.buildBasis(self.basis_num)
            world.writeStr(BASE + "/" + self.mesh_name + "_" + str(self.basis_num) + "Basis.dat")

        world.setEnv(pm.EnvironmentHeightFLOAT())
        world.maxContact(self.max_contact)
        world.dt(self.dt)
        world.dampingCoef(self.damp)
        return world

    def render(self, renderer=None, init_arrow_dir=None):
        from _3DVisualizer import _3DVisualizer
        if self.viewer is None:
            self.viewer = _3DVisualizer(env=self,
                                        scaleEnvTc=np.array([self.x_render_res, self.y_render_res], dtype=np.single))
        self.viewer.cb = renderer
        # Terrain
        if self.cur_terrain_type != 'Empty':
            self.viewer.switchTerrain(terrain=self.cur_terrain_type, Env=self.world.getEnv())
        self.viewer.setBackground()
        # Arrow
        if init_arrow_dir is not None:
            self.viewer.addArrow()
            self.viewer.switchArrow(dir=init_arrow_dir)
        self.viewer.drawer.mainLoop()

    def renderBases(self, mode_num=None, duration_per_basis=400):
        # visualize every basis
        if mode_num is None:
            mode_num = self.basis_num
        assert mode_num <= self.basis_num

        frames = []
        pose = np.zeros(self.basis_num)
        for i in range(mode_num):
            pose[i] = 1.0
            cur_frame = pm.FEMGradientInfoFLOAT(self.world, np.concatenate((pose, np.zeros(self.basis_num + 12))))
            pose[i] = 0.0
            for j in range(duration_per_basis):
                frames.append(cur_frame)

        from OfflineRenderCallBack import OfflineRenderCallBack
        renderer = OfflineRenderCallBack(env=self, switch_arrow_step=-1, frames=frames)
        self.render(renderer, init_arrow_dir=None)

    def reset(self, seed=None):
        pass

    def getObs(self, state):
        pass

    def step(self, action, state):
        pass

    def reward(self, action, observation):
        pass

    def stepMultiple(self, actions, states):
        return [self.step(action, state) for action, state in zip(actions, states)]

    def setDesign(self, design):
        pass

if __name__ == "__main__":
    model = FEMModel3DBase()
    model.renderBases()
