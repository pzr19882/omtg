from pyTinyVisualizer import pyTinyVisualizer as vis
import numpy as np
import sys, math

from LowLevelControl.FEMUtils import BASE

sys.path.append(BASE)
import pyPhysicsMotion as pm


class OfflineRenderCallBack(vis.PythonCallback):
    def __init__(self, env, switch_arrow_step, frames=None, degree_step=0.0, playback_speed=1):
        vis.PythonCallback.__init__(self)
        self.bodyS = None
        self.bodiesS = None
        self.arrowS = None
        self.envS = None
        self.playback_speed = playback_speed
        self.frameId = 0
        self.frames = frames
        self.env = env

        # We allow character to follow a curved trajectory
        # This block of code visualizes the moving direction
        self.switch_arrow_step = switch_arrow_step
        if self.switch_arrow_step >= 0:
            self.next_switch = switch_arrow_step
            self.degree_step = degree_step
            if hasattr(env, 'target_dir'):
                self.degree = math.acos(env.target_dir[0])
            else:
                self.degree = 0.0

    def frame(self, root):
        pm.updateSurfaceFLOAT(self.bodyS, self.frames[self.frameId])
        # We allow character to follow a curved trajectory
        # This block of code visualizes the moving direction
        if self.switch_arrow_step >= 0:
            arrowBase = self.frames[self.frameId].getT().reshape(3, ).astype(np.single)
            self.env.viewer.switchArrow(base=arrowBase)
            if self.frameId >= self.next_switch:
                self.next_switch = (self.next_switch + self.switch_arrow_step) % len(self.frames)
                self.degree = self.degree + self.degree_step
                self.degree = self.degree - int(self.degree / (2 * math.pi)) * 2 * math.pi
                self.env.viewer.switchArrow(dir=np.array([math.cos(self.degree), math.sin(self.degree), 0.0]))
        # move on to next frame
        self.frameId = (self.frameId + self.playback_speed) % len(self.frames)
