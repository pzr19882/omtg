import sys
import math
import numpy as np
from gym.utils import seeding

from LowLevelControl.FEMUtils import BASE, sampleTargetDir, sampleTerrain, applyDirectControl, \
    applyDesign, directSimulate, isSimulationFailed, computeDirectionalReward, ControllerResetter
from LowLevelControl.FEMModel3DBase import FEMModel3DBase

sys.path.append(BASE)
import transforms3d as tr3d
import pyPhysicsMotion as pm


class FEMModel3DDirectionalMove(FEMModel3DBase):
    def __init__(self,
                 seed=None,
                 # mesh & deformable material
                 body_type=-1, mesh_name='Cross.abq', elastic_energy='Corotated', mtl_lambda=10000., mtl_mu=10000.,
                 # model reduction
                 basis_num=56, ctrl_dim=7,
                 # contact solver
                 contact_solver_type='SP',
                 # basic parameters of physical setting
                 gravity=9.81, damp=16.0, max_contact=10, dt=0.001,
                 # basic parameters of customized terrain
                 plane_size=5.0, plane_level=-0.01,
                 # basic parameters of the way to control
                 ctrl_type='P', ctrl_reduced="",
                 # reward(loss) related
                 reward_w=[0., 0., 0., 0.],
                 # render related
                 x_render_res=0.1, y_render_res=0.1,
                 # env(terrain) related, fixed
                 terrain_types=['Fluid'],
                 # env(terrain) related, variable, fluid
                 fluid_level_low=-0.01, fluid_level_high=0.01, fluid_density_low=1., fluid_density_high=2.,
                 drag_coef_low=8., drag_coef_high=12.,
                 # env(terrain) related, variable, floor
                 friction_mu_low=0.5, friction_mu_high=1.0,
                 slope_x_low=math.tan(8.0 * math.pi / 180.0), slope_x_high=math.tan(35.0 * math.pi / 180.0),
                 slope_y_low=math.tan(8.0 * math.pi / 180.0), slope_y_high=math.tan(35.0 * math.pi / 180.0),
                 # target related
                 target_phi_low=0.0, target_phi_high=2 * math.pi,
                 target_theta_low=0.5 * math.pi, target_theta_high=0.5 * math.pi,
                 # time related
                 decision_cycle=0.05,
                 # direct ctrl
                 ctrl_p_low=-1.0, ctrl_p_high=1.0, ctrl_d_low=-5.0, ctrl_d_high=5.0, PCoef=0.03,
                 DCoef=0.03):

        super(FEMModel3DDirectionalMove, self).__init__(
            # mesh & deformable material
            body_type=body_type, mesh_name=mesh_name,
            elastic_energy=elastic_energy, mtl_lambda=mtl_lambda, mtl_mu=mtl_mu,
            # model reduction
            basis_num=basis_num,
            # contact solver
            contact_solver_type=contact_solver_type,
            # basic parameters of physical setting
            gravity=gravity, damp=damp, max_contact=max_contact, dt=dt,
            # basic parameters of customized terrain
            plane_size=plane_size, plane_level=plane_level,
            # render_related
            x_render_res=x_render_res, y_render_res=y_render_res)

        # time related
        self.sim_repeat = int(decision_cycle / dt)
        assert self.sim_repeat >= 1

        # control related
        self.ctrl_dim = ctrl_dim
        if ctrl_type == "P":
            self.ctrl_low = [np.array([ctrl_p_low] * self.ctrl_dim), ]
            self.ctrl_high = [np.array([ctrl_p_high] * self.ctrl_dim), ]
            self.ctrl_reset = ControllerResetter(ctrl_type=ctrl_type, ctrl_reduced=ctrl_reduced, k=[PCoef])
        elif ctrl_type == "D":
            self.ctrl_low = [np.array([ctrl_d_low] * self.ctrl_dim), ]
            self.ctrl_high = [np.array([ctrl_d_high] * self.ctrl_dim), ]
            self.ctrl_reset = ControllerResetter(ctrl_type=ctrl_type, ctrl_reduced=ctrl_reduced, k=[DCoef])
        else:
            self.ctrl_low = [np.array([ctrl_p_low] * self.ctrl_dim), np.array([ctrl_d_low] * self.ctrl_dim), ]
            self.ctrl_high = [np.array([ctrl_p_high] * self.ctrl_dim), np.array([ctrl_d_high] * self.ctrl_dim), ]
            self.ctrl_reset = ControllerResetter(ctrl_type=ctrl_type, ctrl_reduced=ctrl_reduced, k=[PCoef, DCoef])
            self.ctrl_dim *= 2
        self.ctrl_resets = [self.ctrl_reset.copy()]

        # reward related
        self.reward_w = reward_w

        # parameter for sampling terrain
        self.terrain_types = terrain_types
        self.fluid_level_low = fluid_level_low
        self.fluid_level_high = fluid_level_high
        self.fluid_density_low = fluid_density_low
        self.fluid_density_high = fluid_density_high
        self.drag_coef_low = drag_coef_low
        self.drag_coef_high = drag_coef_high
        self.friction_mu_low = friction_mu_low
        self.friction_mu_high = friction_mu_high
        self.slope_x_low = slope_x_low
        self.slope_x_high = slope_x_high
        self.slope_y_low = slope_y_low
        self.slope_y_high = slope_y_high
        self.target_phi_low = target_phi_low
        self.target_phi_high = target_phi_high
        self.target_theta_low = target_theta_low
        self.target_theta_high = target_theta_high
        self.reset(seed=seed)

        # observation related
        state = pm.FEMGradientInfoFLOAT(self.world, self.init_pos)
        self.obs_dim = self.getObs(state).shape[1]
        self.latent_current_states = None

    def reset(self, seed=None):
        # sample stage
        rng, seed = seeding.np_random(seed)
        self.cur_terrain_type, self.terrain, self.target_per, self.rot = \
            sampleTerrain(self.world, rng, self.terrain_types,
                          [self.fluid_level_low, self.fluid_density_low, self.drag_coef_low],
                          [self.fluid_level_high, self.fluid_density_high, self.drag_coef_high],
                          [self.friction_mu_low, self.slope_x_low, self.slope_y_low],
                          [self.friction_mu_high, self.slope_x_high, self.slope_y_high],
                          self.gravity, self.plane_level, self.plane_size)
        if self.cur_terrain_type == 'Fluid':
            x = np.array([1., 0., 0.])
            self.target_dir = sampleTargetDir(rng, self.target_phi_low, self.target_phi_high, self.target_theta_low,
                                              self.target_theta_high)
            self.target_per = np.cross(x, self.target_dir)
            if np.linalg.norm(self.target_per) < 1e-7:
                self.target_per = np.array([0., 0., 1.])
            else:
                self.target_per = self.target_per / np.linalg.norm(self.target_per)
        else:
            self.target_dir = sampleTargetDir(rng, self.target_phi_low, self.target_phi_high, 0.5 * math.pi,
                                              0.5 * math.pi)
            rot_angle = np.linalg.norm(self.rot)
            if math.fabs(rot_angle) > 1e-6:
                self.target_dir = np.matmul(tr3d.axangles.axangle2mat(self.rot / rot_angle, rot_angle, True),
                                            self.target_dir)

        self.target_tan = np.cross(self.target_per, self.target_dir)
        self.init_pos = np.concatenate(
            (np.zeros(self.basis_num + 3), self.rot.astype(np.double), np.zeros(self.basis_num + 6)))
        # log information of current env
        if self.cur_terrain_type == "Fluid":
            print("In Fluid. Level={}, Density={}, Drag={}.".format(self.terrain[0], self.terrain[1], self.terrain[2]))
        # friction_mu is random
        elif self.cur_terrain_type == "Plane":
            print("On Plane. mu={}.".format(self.terrain[0]))
            # a*x+b*y+c*z+d=0, a and b are random, c==1 and d== -self.plane_level
        else:
            assert self.cur_terrain_type == "Slope"
            print("On Slope. mu={}, slope_on_x={}, slope_on_y={}.".format(self.terrain[0], self.terrain[1], self.terrain[2]))
        print("target_dir is {}, target_tan is {}, and target_per is {}".format(self.target_dir, self.target_tan,
                                                                                self.target_per))

    def getObs(self, state):
        if isinstance(state, list):
            obs = np.zeros((len(state), self.obs_dim))
            for i in range(len(state)):
                obs[i] = np.concatenate(
                    (state[i].getT().reshape(-1, ),
                     state[i].getV().reshape(-1, ),
                     np.array([math.fabs(tr3d.axangles.mat2axangle(state[i].getR())[1])]))
                )
            return obs
        else:
            return np.expand_dims(
                np.concatenate(
                    (state.getT().reshape(-1, ),
                     state.getV().reshape(-1, ),
                     np.array([math.fabs(tr3d.axangles.mat2axangle(state.getR())[1])]))
                ) ,0
            )



    def step(self, action, state):
        # PD control signaling
        applyDirectControl(action, self.ctrl_low, self.ctrl_high, self.ctrl_reset)

        # advance simulator
        for i in range(self.sim_repeat):
            ret = directSimulate(self.world, state, self.ctrl_reset.controller, self.contact_solver, False)
            if isSimulationFailed(ret):
                print("Warning: Simulation Failed!")
                print("Now the action is:{}".format(action))
                print("Now the terrain is:{}".format(self.terrain))
            state = ret[0]
        return state

    def reward(self, action, observation):
        # action is not used to evaluate cost
        T = observation[..., 0:3]
        V = observation[..., 3:6]
        rotational_angle = observation[..., 6]
        reward = computeDirectionalReward(V, T, self.target_dir, self.target_tan, self.target_per, self.reward_w[0],
                                          self.reward_w[1], self.reward_w[2])
        reward += self.reward_w[3] * rotational_angle
        return reward

    def stepMultiple(self, actions, states):
        assert len(actions) == len(states)
        # controller resetter initialization
        if len(self.ctrl_resets) > len(actions):
            self.ctrl_resets = self.ctrl_resets[0:len(actions)]
        while len(self.ctrl_resets) < len(actions):
            ctrl_resets = list(self.ctrl_resets)
            for ctrl_reset in ctrl_resets:
                self.ctrl_resets.append(ctrl_reset.copy())
        # PD control signaling
        for action, ctrl_reset in zip(actions, self.ctrl_resets):
            applyDirectControl(action, self.ctrl_low, self.ctrl_high, ctrl_reset)
        # native c++ multi-threading
        for i in range(self.sim_repeat):
            rets = directSimulate(self.world, states, [ctrl_reset.controller for ctrl_reset in self.ctrl_resets],
                                  self.contact_solver, False)
            for ret, action in zip(rets, actions):
                if isSimulationFailed(ret):
                    print("Warning: Simulation Failed!")
                    print("Now the action is:{}".format(action))
                    print("Now the terrain is:{}".format(self.terrain))
            states = [ret[0] for ret in rets]
        return states

    def setDesign(self, design):
        if isinstance(design, list):
            if len(self.ctrl_resets) > len(design):
                self.ctrl_resets = self.ctrl_resets[0:len(actions)]
            while len(self.ctrl_resets) < len(design):
                self.ctrl_resets.append(self.ctrl_reset.copy())
            for k, ctrl_reset in zip(design, self.ctrl_resets):
                applyDesign(k, ctrl_reset)
        else:
            applyDesign(design, self.ctrl_reset)
            self.ctrl_resets = [self.ctrl_reset.copy()]


if __name__ == "__main__":
    for terrain_type in ['Fluid', 'Plane', 'Slope']:
        model = FEMModel3DDirectionalMove(terrain_types=[terrain_type])

        # test simulating 1 frame
        action = np.zeros(model.ctrl_dim)
        state = pm.FEMGradientInfoFLOAT(model.world, model.init_pos)
        state = model.step(action, state)
        reward = model.reward(action, state)

        # test simulating 2 frames simultaneously
        actions = []
        actions.append(np.zeros(model.ctrl_dim))
        actions.append(np.zeros(model.ctrl_dim))
        states = []
        states.append(pm.FEMGradientInfoFLOAT(model.world, model.init_pos))
        states.append(pm.FEMGradientInfoFLOAT(model.world, model.init_pos))
        states = model.stepMultiple(actions, states)
