import os,sys, math
import torch
import numpy as np
import argparse, time

sys.path.append('..')
from LowLevelControl.FEMModel3DDirectionalMove import FEMModel3DDirectionalMove
from LowLevelControl.OfflineRenderCallBack import OfflineRenderCallBack
from _GenerateArgsParser import _GenerateArgsParser
from pytorch_mppi import mppi
import pyPhysicsMotion as pm
import pickle


class MyMPPI(mppi.MPPI):
    """
    This class extended pytorch-mppi: https://github.com/UM-ARM-Lab/pytorch_mppi
    so that 'parallel dynamics' can be applied.
    Suppose a dynamics F handles NA agents parallely, and MPPI generate K samples per agent.
    Then action's shape is (K, NA, nu) instead of (K, nu), and obs' shape is (K, NA, nk) instead of (K, nk)
    The limitation of this extended mppi is that it can't handle stochastic dynamics.
    """

    def __init__(self, NA=1, **base_params):
        super(MyMPPI, self).__init__(rollout_samples=1, rollout_var_cost=0, rollout_var_discount=0.95, **base_params)
        self.NA = NA if NA > 1 else 1
        if self.NA > 1:
            self.U = self.noise_dist.sample((self.T, self.NA))
            self.u_init = self.u_init.repeat(self.NA, 1)

    def reset(self):
        self.U = self.noise_dist.sample((self.T,)) if self.NA == 1 else self.noise_dist.sample((self.T, self.NA))

    def _dynamics(self, state, u, t):
        return self.F(state, u, t) if self.step_dependency else self.F(state, u)

    def _running_cost(self, state, u):

        return self.running_cost(state, u)

    def command(self, obs):
        # shift command 1 time step
        self.U = torch.roll(self.U, -1, dims=0)
        self.U[-1] = self.u_init

        if not torch.is_tensor(obs):
            obs = torch.tensor(obs)
        self.state = obs.to(dtype=self.dtype, device=self.d)

        # compute weights
        cost_total = self._compute_total_cost_batch()  # shape = (num_samples,) or (num_samples, num_agents)
        beta = torch.min(cost_total, dim=0).values
        self.cost_total_non_zero = mppi._ensure_non_zero(cost_total, beta, 1 / self.lambda_)
        eta = torch.sum(self.cost_total_non_zero, dim=0)
        self.omega = (1.0 / eta) * self.cost_total_non_zero  # shape = (num_samples,) or (num_samples, num_agents)
        # action = (weights * noise).sum()
        # noise[:,t] is of shape (num_samples, nu) or (num_samples, num_agents, nu)
        for t in range(self.T):
            self.U[t] += torch.sum(torch.unsqueeze(self.omega, -1) * self.noise[:, t], dim=0)
        action = self.U[:self.u_per_command]
        if self.u_per_command == 1:
            action = action[0]
        return action

    def _compute_total_cost_batch(self):
        self.noise = self.noise_dist.sample((self.K, self.T)) if self.NA == 1 else self.noise_dist.sample(
            (self.K, self.T, self.NA))
        self.perturbed_action = self.U + self.noise
        if self.sample_null_action:
            self.perturbed_action[self.K - 1] = 0
        self.perturbed_action = self._bound_action(self.perturbed_action)
        self.noise = self.perturbed_action - self.U
        if self.noise_abs_cost:
            action_cost = self.lambda_ * torch.abs(self.noise) @ self.noise_sigma_inv
        else:
            action_cost = self.lambda_ * self.noise @ self.noise_sigma_inv  # Like original paper
        self.cost_total, self.states, self.actions = self._compute_rollout_costs(self.perturbed_action)
        self.actions /= self.u_scale
        perturbation_cost = torch.sum(self.U * action_cost, dim=(1, -1))
        self.cost_total += perturbation_cost
        return self.cost_total

    def _compute_rollout_costs(self, perturbed_actions):
        if self.NA == 1:
            K, T, nu = perturbed_actions.shape
            assert nu == self.nu
            cost_total = torch.zeros(K, device=self.d, dtype=self.dtype)
        else:
            K, T, NA, nu = perturbed_actions.shape
            assert nu == self.nu and NA == self.NA
            cost_total = torch.zeros((K, NA), device=self.d, dtype=self.dtype)
        if self.NA == 1:
            obs = self.state if self.state.shape == (K, self.nx) else self.state.view(1, -1).repeat(K, 1)
        else:
            obs = self.state if self.state.shape == (K, self.NA, self.nx) else self.state.repeat(K, 1, 1)

        obss = []
        actions = []
        for t in range(T):
            u = self.u_scale * perturbed_actions[:, t]
            obs = self._dynamics(obs, u, t)
            step_cost = self._running_cost(obs, u)
            cost_total += step_cost
            # Save total states/actions
            obss.append(obs)
            actions.append(u)
        actions = torch.stack(actions, dim=1)
        obss = torch.stack(obss, dim=1)

        # action perturbation cost
        if self.terminal_state_cost:
            cost_total += self.terminal_state_cost(obss, actions)
        return cost_total, obss, actions

    def _slice_control(self, t):
        return slice(t, t + 1)

    def get_rollouts(self, obs, num_rollouts=1):
        assert num_rollouts == 1
        obs = obs.view(self.nx, ) if self.NA == 1 else obs.view(self.NA, self.nx)
        T = self.U.shape[0]
        obss_shape = (T + 1, self.nx) if self.NA == 1 else (T + 1, self.NA, self.nx)
        obss = torch.zeros(obss_shape, dtype=self.U.dtype, device=self.U.device)
        obss[0] = obs
        for t in range(T):
            obss[t + 1] = self._dynamics(obss[t], self.u_scale * self.U[t], t)
        return obss[1:]

    def switch_NA(self, NA):
        orig_NA = self.NA
        self.NA = NA
        if orig_NA != NA:
            self.reset()
        if orig_NA == 1 and NA > 1:
            self.u_init = self.u_init.repeat(NA, 1)
        elif orig_NA > 1 and NA == 1:
            self.u_init = self.u_init[0]


def save_step_inputs(actions, infos, path='step_inputs.pickle'):
    states = [np.array(info.getX()).T[0, :] for info in infos]
    with open(path, 'wb') as f:
        pickle.dump((actions, states), f)


def load_step_inputs(world, path='step_inputs.pickle'):
    with open(path, 'rb') as f:
        actions, states = pickle.load(f)
    infos = [pm.FEMGradientInfoFLOAT(world, state) for state in states]
    return actions, infos


def getMPPIController(env, noise_sigma=0.2, lambda_=0.01, num_samples=4, horizon=0.04, NA=1, device='cpu'):
    def dynamics(obs, actions):
        actions = actions.detach().cpu().numpy()
        length = 1
        d_NA = actions.shape[1] if len(actions.shape) > 2 else 1
        for i in range(len(actions.shape) - 1):
            length *= actions.shape[i]
        if len(env.latent_current_states) != length:
            env.latent_current_states = env.latent_current_states * actions.shape[0]
        actions = list(actions.reshape(length, env.obs_dim))
        env.latent_current_states = env.stepMultiple(actions, env.latent_current_states)

        # convert to observation
        observations = env.getObs(env.latent_current_states).reshape(num_samples, d_NA, -1) if d_NA > 1 else env.getObs(
            env.latent_current_states)
        return torch.from_numpy(observations)

    def running_cost(obss, actions):
        obss = obss.detach().cpu().numpy()
        actions = actions.detach().cpu().numpy()
        cost = -env.reward(actions, obss)
        return torch.from_numpy(cost)

    u_min = np.array([-1.0] * env.ctrl_dim)
    u_max = np.array([1.0] * env.ctrl_dim)

    decision_steps = int(horizon / (env.dt * env.sim_repeat))

    if isinstance(noise_sigma, float):
        noise_sigma = torch.tensor(noise_sigma * np.identity(env.ctrl_dim))

    controller = MyMPPI(NA=NA, noise_abs_cost=True,
                        dynamics=dynamics,
                        running_cost=running_cost,
                        nx=env.obs_dim,
                        noise_sigma=noise_sigma,
                        num_samples=num_samples,
                        horizon=decision_steps,
                        lambda_=lambda_,
                        u_min=torch.tensor(u_min, dtype=torch.double, device=device),
                        u_max=torch.tensor(u_max, dtype=torch.double, device=device))
    return controller


def getMPPIControlCommand(env, state, controller):
    obs = env.getObs(state)
    env.latent_current_states = state if isinstance(state, list) else [state]
    action = controller.command(torch.from_numpy(obs))
    action = action.detach().cpu().numpy()
    next_state = env.step(action, state) if not isinstance(state, list) else env.stepMultiple(action, state)
    obs = env.getObs(next_state)
    reward = env.reward(action, obs)
    return next_state, reward


if __name__ == '__main__':
    data_save_path = '../../../train_data'
    if not os.path.exists(data_save_path):
        os.makedirs(data_save_path)
    parser = _GenerateArgsParser('Lower')
    args = parser.parse_args()

    decision_cycle = 0.01
    itrs = int(args.tot_time / decision_cycle)
    reward_w = [1000.0, 5.0, 5.0, 1.0]
    device = 'cpu'
    target_phi_low = 0
    target_phi_high = 0
    target_theta_low = 0.5 * math.pi
    target_theta_high = 0.5 * math.pi
    seed = int(time.time())
    env = FEMModel3DDirectionalMove(  # basic parameters of customized terrain
        seed=seed,
        plane_size=7.0,
        # basic parameters of the way to control
        # ctrl_type='PD', ctrl_reduced="",
        # reward(loss) related
        reward_w=reward_w,
        # env(terrain) related, fixed
        terrain_types=[args.terrain] if isinstance(args.terrain, str) else args.terrain,
        # env(terrain) related, variable, fluid
        fluid_level_low=0.0, fluid_level_high=0.0, fluid_density_low=2.0, fluid_density_high=2.0,
        drag_coef_low=10.0, drag_coef_high=10.0,
        # env(terrain) related, variable, floor
        friction_mu_low=0.7, friction_mu_high=0.7,
        # target related
        target_phi_low=target_phi_low, target_phi_high=target_phi_high,
        target_theta_low=target_theta_low, target_theta_high=target_theta_high,
        # direct ctrl
        ctrl_p_low=-2.0, ctrl_p_high=2.0,
        # time related
        decision_cycle=decision_cycle)

    # Main loop
    MPPI = getMPPIController(env, noise_sigma=args.sigma, num_samples=args.num_samples, horizon=args.horizon,
                             device=device)
    state = pm.FEMGradientInfoFLOAT(env.world, env.init_pos)
    frames_X = np.zeros((itrs,) + state.getX().shape)
    frames = []
    tot_reward = 0.0
    for i in range(itrs):
        state, reward = getMPPIControlCommand(env, state, MPPI)
        tot_reward += reward
        print('Optimization of the {} decision step finished. step_reward is {}'.format(i, reward[0]))
        frames_X[i] = state.getX()
        frames.append(state)
    print('The Episode reward is {}'.format(tot_reward))
    np.save(data_save_path+'/frames_X_' + str(seed), frames_X)
    renderer = OfflineRenderCallBack(env=env, switch_arrow_step=len(frames), frames=frames)
    env.render(renderer, init_arrow_dir=env.target_dir)
