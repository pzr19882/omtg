import argparse, math


def _GenerateArgsParser(control_level: str = 'Lower'):
    parser = argparse.ArgumentParser(description='Input arguments for ' + control_level + ' Control')
    # env
    parser.add_argument('--reward_w', default=[1000.0, 5.0, 5.0, 1.0], nargs=4, type=float)
    parser.add_argument('--target_phi', default=[0.0, 0.0], nargs=2, type=float)
    parser.add_argument('--target_theta', default=[0.5 * math.pi, 0.5 * math.pi], nargs=2, type=float)
    parser.add_argument('--terrain', default='Fluid', type=str, nargs='+')
    parser.add_argument('--decision_cycle', default=0.01, type=float)
    # Lower(MPPI)
    parser.add_argument('--horizon', default=0.04, type=float)
    parser.add_argument('--num_samples', default=4, type=int)
    parser.add_argument('--noise_sigma', default=0.2, type=float)
    parser.add_argument('--tot_time', default=5, type=float)
    if control_level == 'Upper':
        # Upper(Bayesian Optimization)
        parser.add_argument('--n_pre_train', default=10, type=int)
        parser.add_argument('--n_train', default=1000000, type=int)
        parser.add_argument('--gp_itrs', default=100, type=int)
        parser.add_argument('--gp_lr', default=0.01, type=float)
        parser.add_argument('--gp_kernel', default='RBF', type=str, choices=['RBF', 'Laplace'])
        parser.add_argument('--bo_n_explore', default=10, type=int)
        parser.add_argument('--bo_n_warmup', default=10000, type=int)
        parser.add_argument('--bo_solver', default='SteepestDescent', type=str,
                            choices=['ConjugateGradient', 'SteepestDescent', 'ParticleSwarm', 'TrustRegions', 'NelderMead'])
        parser.add_argument('--save_interval', default=10, type=int)
    return parser
