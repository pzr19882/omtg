import os.path
import sys, argparse, math, time

sys.path.append('..')
from LowLevelControl.FEMUtils import BASE

sys.path.append(BASE)
from LowLevelControl.MPPIController import getMPPIController, getMPPIControlCommand
from LowLevelControl.FEMModel3DDirectionalMove import FEMModel3DDirectionalMove
import gpytorch as gp
import numpy as np
import pymanopt.solvers
from pymanopt.manifolds import Grassmann
from BOUtils import BayesianOptimizationOnManifold, acq_func
from GPUtils import GMGPModel, randOnGM, ExactGPRegression
from BlackBoxes import getEpisodeReward
from _GenerateArgsParser import _GenerateArgsParser

'''
    Find optimal design matrix K using Bayesian Optimization
'''

if __name__ == '__main__':
    parser = _GenerateArgsParser('Upper')
    args = parser.parse_args()
    data_save_path = '../../../train_data'
    if not os.path.exists(data_save_path):
        os.makedirs(data_save_path)
    # create env
    seed = int(time.time())
    env = FEMModel3DDirectionalMove(  # basic parameters of customized terrain
        seed=seed,
        plane_size=7.0,
        # basic parameters of the way to control
        ctrl_type='P', ctrl_reduced="Reduced",
        # reward(loss) related
        reward_w=args.reward_w,
        # env(terrain) related, fixed
        terrain_types=args.terrain if isinstance(args.terrain, list) else [args.terrain],
        # env(terrain) related, variable, fluid
        fluid_level_low=0.0, fluid_level_high=0.0, fluid_density_low=2.0, fluid_density_high=2.0,
        drag_coef_low=10.0, drag_coef_high=10.0,
        # env(terrain) related, variable, floor
        friction_mu_low=0.7, friction_mu_high=0.7,
        # target related
        target_phi_low=args.target_phi[0], target_phi_high=args.target_phi[1],
        target_theta_low=args.target_theta[0], target_theta_high=args.target_theta[1],
        # direct ctrl
        ctrl_p_low=-2.0, ctrl_p_high=2.0,
        # time related
        decision_cycle=args.decision_cycle)

    # initialize information of the black box function, manifold, and information of the rng.
    # Those values will be used in both pre-training GP and bayesian optimization
    controller = getMPPIController(NA=args.n_pre_train, env=env, noise_sigma=args.noise_sigma, lambda_=0.01,
                                   num_samples=args.num_samples, horizon=args.horizon)
    info_black_box = {'env': env, 'controller': controller,
                      'steps': int(args.tot_time / args.decision_cycle), 'debug': False}
    K_feasible_region = Grassmann(n=env.basis_num, p=env.ctrl_dim)
    np.random.seed(seed)
    rng = randOnGM(K_feasible_region, np.random.multivariate_normal)
    init_mean = np.concatenate((np.identity(env.ctrl_dim).reshape(-1, ),
                                np.zeros((env.basis_num - env.ctrl_dim) * env.ctrl_dim)), axis=0)
    init_cov = 0.1 * np.identity(env.basis_num * env.ctrl_dim)

    # pre-train GP model.
    assert args.n_pre_train > 0
    gp_model = GMGPModel(likelihood=gp.likelihoods.GaussianLikelihood(),
                         n=env.basis_num, p=env.ctrl_dim,
                         kernel_type=args.gp_kernel)
    gp_regression = ExactGPRegression(gp_model, rng, getEpisodeReward, info_black_box, dict({'lr': args.gp_lr}))
    gp_regression.rand_train_data(data_size=(args.n_pre_train,), mean=init_mean, cov=init_cov, mat_to_vec=True)
    print("================Pre train===============")
    gp_regression(args.gp_itrs)
    # BO
    bo_solver = getattr(pymanopt.solvers, args.bo_solver)(logverbosity=1)
    info_rng = {'mean': init_mean, 'cov': init_cov, 'mat_to_vec': False}
    BO = BayesianOptimizationOnManifold(gp_regression, K_feasible_region, bo_solver, acq_func, info_rng=info_rng,
                                        info_acq=dict({'kappa': 2.576}), verbosity=0)
    BO(bo_itrs=args.n_train, gp_itrs=args.gp_itrs, bo_n_warmup=args.bo_n_warmup, bo_n_explore=args.bo_n_explore,
       save_interval=args.save_interval, suffix='OptimalB_' + str(seed))
    print("================Finished===============")
