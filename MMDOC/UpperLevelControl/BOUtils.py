import gpytorch as gp
from pymanopt.manifolds import Grassmann
from pymanopt.manifolds.manifold import Manifold
import pymanopt.solvers.solver
import torch as th
import numpy as np
import math
from RandOnManifold import randOnGM
from GPUtils import ExactGPRegression, GMGPModel
from _Backends import MyPyTorch


'''
Recommended kappa value is 2.576 in 
https://github.com/fmfn/BayesianOptimization/blob/91441fe4002fb6ebdb4aa5e33826230d8df560d0/bayes_opt/bayesian_optimization.py#L236
line 236.
'''
def GP_UCB(gp_regression: ExactGPRegression, x, kappa):
    prediction = gp_regression.evaluate(x)
    return prediction.mean + kappa * prediction.variance


def acq_func(gp_regression, x, kappa):
    x = x.reshape((*x.shape[:-2], x.shape[-1] * x.shape[-2]))
    return GP_UCB(gp_regression, x, kappa)


class BayesianOptimizationOnManifold:
    """
    The matrices represents the point on Grassmann Manifold are vectorized.
    All values here are of numpy.ndarray. With the backend decoration, the conversion between numpy and tensor
    is not explicitly written.
    """

    def __init__(self, gp_regression: ExactGPRegression,
                 manifold: Manifold, solver: pymanopt.solvers.solver, acq_func, decorator=MyPyTorch,
                 info_rng: dict = {}, info_acq: dict = {}, verbosity=2):
        self.gp_regression = gp_regression
        self.solver = solver
        self.acq_func = acq_func
        self.gp_regression.verbosity = verbosity
        self.black_box_function = self.gp_regression.black_box_function
        self.info_black_box = self.gp_regression.info_black_box
        self.info_rng = info_rng
        self.info_acq = info_acq

        @decorator(manifold)
        def opposite_acq_func(x):
            return -1.0 * self.acq_func(self.gp_regression, x, **self.info_acq)[0]

        self.verbosity = verbosity
        self.problem = pymanopt.Problem(manifold=manifold, cost=opposite_acq_func, verbosity=verbosity)

        self.x_buffer = []
        self.y_buffer = []
        self.acq_buffer = []

    def __call__(self, *args, **kwargs):
        self.fit(*args, **kwargs)

    def fit(self, bo_itrs, bo_n_explore, bo_n_warmup, gp_itrs, save_interval=0, dir:str='.', suffix: str = ''):
        for i in range(bo_itrs):
            print("================Start {}-th iteration of BO================".format(i))
            x_max, acq_max = self.argmax_acq(bo_n_explore, bo_n_warmup)
            y_max = self.black_box_function(x_max, **self.info_black_box)
            self.x_buffer.append(x_max)
            self.acq_buffer.append(acq_max)
            self.y_buffer.append(y_max[0])
            print("A new acq_max = {} is found after argmax_acq.".format(acq_max))
            print("A new y_max = {} is found, and added to the data set.".format(y_max[0]))
            print("Do GPR with the new data set.")
            self.gp_regression.add_train_data(x_max, y_max)
            self.gp_regression(gp_itrs)
            if save_interval and (i+1) % save_interval == 0:
                np.save(dir+'/BO_x_buffer_' + suffix, self.x_buffer)
                np.save(dir+'/BO_acq_buffer_' + suffix, self.acq_buffer)
                np.save(dir+'/BO_y_buffer_' + suffix, self.y_buffer)
                self.gp_regression.save(dir, suffix)

    def argmax_acq(self, n_explore, n_warmup):
        # warm up with random points on the manifold
        cost_min = math.inf
        res = None
        if n_warmup > 1:
            x_tries = self.gp_regression.rng(**self.info_rng, data_size=(n_warmup,))
            ys = -1.0 * self.acq_func(self.gp_regression, x_tries, **self.info_acq)
            res = np.expand_dims(x_tries[ys.argmin()], 0)
            cost_min = ys.min()
            cost_min = cost_min.detach().numpy() if isinstance(cost_min, th.Tensor) else cost_min
            if self.verbosity:
                print("Warmup finished. A new acq_max = {} is found in the warmup.".format(-1.0 * cost_min))
        # explore the manifold more thoroughly
        for i in range(n_explore):
            if self.verbosity:
                print('The {}-th exploration begins on the manifold.'.format(i))
            x0 = self.gp_regression.rng(**self.info_rng)
            solution = self.solver.solve(self.problem, x0)
            if isinstance(solution, tuple):
                x_new = solution[0]
                cost_new = solution[1]['final_values']['f(x)']
            else:
                x_new = solution
                cost_new = self.problem.cost(x_new)
            if cost_new <= cost_min:
                res = x_new
                cost_min = cost_new
                if self.verbosity:
                    print('A new acq_max = {} is found in the {}-th exploration.\n'.format(-1.0 * cost_min, i))
        return res.reshape((*res.shape[:-2], res.shape[-1] * res.shape[-2])), -1.0 * cost_min

    @property
    def res(self):
        return self.x_buffer, self.y_buffer


if __name__ == '__main__':
    '''
    module test for bayesian optimization, based on the fact that
    GPUtils.py is free of bugs.
    black box function is y = sin(||X^T[0:p,0:p]-I_p||_F)
    '''
    from BlackBoxes import test_function
    import os
    data_save_dir = '../../../train_data'
    if not os.path.exists(data_save_dir):
        os.makedirs(data_save_dir)
    n_pre_train = 100
    bo_itrs = 100
    gp_itrs = 50
    bo_n_explore = 5
    bo_n_warmup = 5000
    Gn = 56
    Gp = 7
    manifold = Grassmann(n=Gn, p=Gp)

    init_mean = np.zeros(Gn * Gp)
    init_cov = np.identity(Gn * Gp)
    rng = randOnGM(manifold, np.random.multivariate_normal)

    # pre_train gp_model
    gp_model = GMGPModel(gp.likelihoods.GaussianLikelihood(), Gn, Gp)
    gp_regression = ExactGPRegression(gp_model, rng, test_function, dict({'Gn': Gn, 'Gp': Gp}), dict({'lr': 0.01}))
    gp_regression.rand_train_data(data_size=(n_pre_train,), mean=init_mean, cov=init_cov, mat_to_vec=True)
    print("================Pre train===============")
    gp_regression(gp_itrs)
    # BO
    bo_solver = getattr(pymanopt.solvers, 'SteepestDescent')(logverbosity=1)
    BO = BayesianOptimizationOnManifold(gp_regression, manifold, bo_solver, acq_func, MyPyTorch,
                                        dict({'mean': init_mean, 'cov': init_cov, 'mat_to_vec': False}),
                                        dict({'kappa': 2.576}), verbosity=0)
    BO(bo_itrs, bo_n_explore, bo_n_warmup, gp_itrs, 2, data_save_dir, 'test')
