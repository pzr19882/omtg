import GMKernels
import gpytorch as gp
from gpytorch.models import ExactGP
from pymanopt.manifolds import Grassmann
import numpy as np
import torch as th
from RandOnManifold import randOnGM


class GMGPModel(ExactGP):
    def __init__(self, likelihood, n, p, train_x=None, train_y=None, mean_prior=None, lengthscale_prior=None,
                 kernel_type="RBF"):
        super(GMGPModel, self).__init__(train_x, train_y, likelihood)
        self.mean = gp.means.ConstantMean(prior=mean_prior)
        self.cov = gp.kernels.ScaleKernel(
            getattr(GMKernels, kernel_type + "GMKernel")(n=n, p=p, lengthscale_prior=lengthscale_prior))
        self.n = n
        self.p = p

    def forward(self, x):
        mean = self.mean(x)
        cov = self.cov(x)
        return gp.distributions.MultivariateNormal(mean, cov)

    def setGM(self, n, p):
        self.n = n
        self.p = p
        self.cov.setGM()


class ExactGPRegression:
    """
    1. Only apply torch.optim.Adam as the solver for ExactGPRegression.
    Are there any alternatives?
    The parameters to be trained should be changed conveniently.
    2. All values are initially numpy arrays and not converted to tensor until they have to.
    """

    def __init__(self, gp_model: gp.models.ExactGP, rng, black_box_function,
                 info_black_box: dict = {}, info_solver: dict = {},
                 train_x=None, train_y=None, verbosity=1):
        self.gp_model = gp_model
        self.rng = rng
        self.black_box_function = black_box_function
        self.info_black_box = info_black_box
        self.info_solver = info_solver
        self.train_x = train_x
        self.train_y = train_y
        self.fitted = False
        self.mode = 'idle'
        self.verbosity = verbosity

    def __call__(self, *args, **kwargs):
        self.fit(*args, **kwargs)

    def train_mode(self):
        self.gp_model.train()
        self.gp_model.likelihood.train()
        self.mode = 'train'

    def eval_mode(self):
        assert self.fitted
        self.gp_model.eval()
        self.gp_model.likelihood.eval()
        self.mode = 'eval'

    def fit(self, itrs):
        if self.mode != 'train':
            self.train_mode()
        solver = th.optim.Adam(self.gp_model.parameters(), **self.info_solver)
        mll = gp.mlls.ExactMarginalLogLikelihood(model=self.gp_model, likelihood=self.gp_model.likelihood)
        for i in range(itrs):
            # Zero gradients from previous iteration
            solver.zero_grad()
            # Calc loss and backprop gradients
            loss_value = -1.0 * mll(self.gp_model(th.tensor(self.train_x, dtype=th.float32)),
                                    th.tensor(self.train_y, dtype=th.float32))
            if self.verbosity:
                print('GPR: In the {}-th iteration, loss is {}'.format(i, loss_value))
            loss_value.backward()
            solver.step()
        self.fitted = True

    def evaluate(self, eval_x=None):
        if self.mode != 'eval':
            self.eval_mode()
        eval_x = th.tensor(eval_x, dtype=th.float32) if not isinstance(eval_x, th.Tensor) else eval_x.float()
        pred = self.gp_model(eval_x)
        return pred

    def rand_train_data(self, data_size: tuple, **info_rng):
        self.train_x = self.rng(data_size=data_size, **info_rng)
        self.train_y = self.black_box_function(self.train_x, **self.info_black_box)
        self.gp_model.set_train_data(th.tensor(self.train_x, dtype=th.float32),
                                     th.tensor(self.train_y, dtype=th.float32), strict=False)

    def add_train_data(self, new_train_x, new_train_y):
        assert self.fitted
        self.train_x = np.concatenate((self.train_x, new_train_x))
        self.train_y = np.concatenate((self.train_y, new_train_y))
        self.gp_model = self.gp_model.get_fantasy_model(th.tensor(new_train_x, dtype=th.float32),
                                                        th.tensor(new_train_y, dtype=th.float32))

    def set_train_data(self, train_x, train_y):
        self.train_x = train_x
        self.train_y = train_y
        self.gp_model.set_train_data(th.tensor(train_x, dtype=th.float32), th.tensor(train_y, dtype=th.float32))

    def save(self, dir: str = '.', suffix: str = ''):
        th.save(self.gp_model.state_dict(), dir + '/GP_model_state_' + suffix)
        np.save(dir + '/GP_train_x_' + suffix, self.train_x)
        np.save(dir + '/GP_train_y_' + suffix, self.train_y)


if __name__ == '__main__':
    '''
        module test for GPUtils.py and GMKernels.py
        do GP regression using instances of GMGPModel with different GMKernels
        black box function is y = sin(||X^T[0:p,0:p]-I_p||_F)
    '''
    from BlackBoxes import test_function

    kernel_types = ['RBF', 'Laplace']
    # when n_points > 400, lazy evaluation of diag covar may be activated in gpytorch.models.ExactGP.__call__().
    n_points = 1000
    n_fantasy_points = 50
    itrs = 50
    Gn = 56
    Gp = 7
    manifold = Grassmann(n=Gn, p=Gp)
    init_mean = np.zeros(Gn * Gp)
    init_cov = np.identity(Gn * Gp)
    rng = randOnGM(manifold, np.random.multivariate_normal)

    for kernel_type in kernel_types:
        print('Start the test of {}GMKernel.'.format(kernel_type))
        print('=======================================================')
        print('First train {} random samples.'.format(n_points))
        gp_model = GMGPModel(gp.likelihoods.GaussianLikelihood(), Gn, Gp, kernel_type=kernel_type)
        gp_regression = ExactGPRegression(gp_model, rng, test_function,
                                          dict({'Gn': Gn, 'Gp': Gp}), dict({'lr': 0.01}))
        gp_regression.rand_train_data(data_size=(n_points,), mean=init_mean, cov=init_cov, mat_to_vec=True)
        gp_regression(itrs)
        print('Then evaluate on {} fantasy points.'.format(n_fantasy_points))
        new_train_x = rng(mean=init_mean, cov=init_cov, mat_to_vec=True, data_size=(n_fantasy_points,))
        pred = gp_regression.evaluate(new_train_x)
        print('posterior mean is {}'.format(pred.mean))
        print('posterior sigma is {}'.format(pred.variance))
        print('Then add these fantasy points, and refit.')
        new_train_y = test_function(new_train_x, Gn=Gn, Gp=Gp)
        gp_regression.add_train_data(new_train_x, new_train_y)
        gp_regression(itrs)
