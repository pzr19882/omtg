import sys
import torch as th
import numpy as np
import math

sys.path.append('..')
from LowLevelControl.FEMUtils import BASE

sys.path.append(BASE)
import pyPhysicsMotion as pm
from LowLevelControl.MPPIController import getMPPIControlCommand, getMPPIController
from LowLevelControl.FEMModel3DDirectionalMove import FEMModel3DDirectionalMove


# y = sin(||X^T[0:p,0:p]-I_p||_F)
def test_function(x, Gn, Gp):
    x = x.reshape((*x.shape[:-1], Gn, Gp))
    Ip = np.identity(Gp)
    y = np.sin(np.linalg.norm((x[..., 0:Gp, 0:Gp] - Ip), ord='fro', axis=(1, 2)))
    return y


def getEpisodeReward(K, env, controller, steps, debug=False):
    """
        black_box_function here is a function from R^(n*m) to R.
        Given a design matrix K, a maximum reward under this K should be computed using MPPI controller.
        Make full use of multi-step interface to sample the points for pre-training of GPR.
    """
    K = K.reshape((*K.shape[:-1], env.basis_num, env.ctrl_dim))
    if len(env.ctrl_reset.k) == 1:
        K = np.expand_dims(K, axis=-3)
    NA = K.shape[0]
    controller.switch_NA(NA)
    K = K[0] if NA == 1 else list(K)
    env.setDesign(K)
    reward = 0.0
    state = [pm.FEMGradientInfoFLOAT(env.world, env.init_pos)] * NA \
        if NA > 1 else pm.FEMGradientInfoFLOAT(env.world, env.init_pos)
    if debug:
        frames_X = np.zeros((NA, steps)+state[0].getX().shape) if NA > 1 else np.zeros((steps,)+state.getX().shape)
        for i in range(steps):
            state, step_reward = getMPPIControlCommand(env, state, controller)
            reward += step_reward
            if NA > 1:
                for idx in range(NA):
                    frames_X[idx][i] = state[idx].getX()
            else:
                frames_X[i] = state.getX()
            print('Optimization of the {} decision step finished. step_reward is {}'.format(i, step_reward))
        return reward, frames_X
    else:
        for i in range(steps):
            state, step_reward = getMPPIControlCommand(env, state, controller)
            reward += step_reward
        return reward


if __name__ == "__main__":
    import os
    data_save_path = '../../../train_data'
    if not os.path.exists(data_save_path):
        os.makedirs(data_save_path)
    env = FEMModel3DDirectionalMove(
        # basic parameters of customized terrain
        plane_size=7.0,
        # basic parameters of the way to control
        ctrl_type='P', ctrl_reduced='Reduced',
        basis_num=56, ctrl_dim=7,
        # reward(loss) related
        reward_w=[1000.0, 5.0, 5.0, 1.0],
        # env(terrain) related, fixed
        terrain_types=['Plane'],
        # env(terrain) related, variable, fluid
        fluid_level_low=0.0, fluid_level_high=0.0, fluid_density_low=2.0, fluid_density_high=2.0,
        drag_coef_low=10.0, drag_coef_high=10.0,
        # env(terrain) related, variable, floor
        friction_mu_low=0.7, friction_mu_high=0.7,
        # target related
        target_phi_low=0.0, target_phi_high=0.0,
        target_theta_low=0.5*math.pi, target_theta_high=0.5*math.pi,
        # direct ctrl
        ctrl_p_low=-2.0, ctrl_p_high=2.0,
        # time related
        decision_cycle=0.01)
    print('Current terrain is {}'.format(env.cur_terrain_type))
    if env.cur_terrain_type == 'Fluid':
        print('(level, density, drag)={}'.format(env.terrain))
    else:
        print('(friction_mu, slope_x, slope_y)={}'.format(env.terrain))
    print("target_dir is {}, tanget_tan is {}, and target_per is {}".format(env.target_dir, env.target_tan,
                                                                            env.target_per))
    tot_time = 5.0
    n_agent = 5
    K = np.array([np.concatenate((np.identity(env.ctrl_dim).reshape(-1, ),
                                  np.zeros(env.ctrl_dim * (env.basis_num - env.ctrl_dim))))] * n_agent)
    controller = getMPPIController(env, NA=n_agent)
    reward, frames_X = getEpisodeReward(K, env, controller, int(tot_time / (env.sim_repeat * env.dt)), True)
    print('The Episode reward is {}'.format(reward))
    np.save(data_save_path+'/frames_X_black_box_debug', frames_X)
