import gpytorch as gp
import torch as th
from typing import Optional, Tuple
from gpytorch.constraints import Interval
from gpytorch.priors import Prior


class GMDistance(gp.kernels.kernel.Distance):
    """
       The projection metric is used to measure the distance d(K1, K2) where K1 and K2 are on Grassmann manifold.
       The projection metric is a well-structured metric on Grassmann manifold induced from a positive definite
       Grassmann kernel, and easy to compute.
       Please refer to J. Hamm, D. D. Lee, Grassmann discriminant analysis: A unifying view on subspace-based learning,
       in: Proceedings of the 25th International Conference on Machine Learning, ACM, 2008, pp. 376–383
       for more information about metrics on Grassmann manifold.
       """
    def __init__(self, postprocess_script=lambda x: x):
        super(GMDistance, self).__init__(postprocess_script)

    def _GM_projection_kernel(self, x1, x2, n, p, postprocess):
        # input shape(x1) = (..., b1, n*p), shape(x2)=(..., b2, n*p)
        assert len(x1.shape) >= 2 and x1.shape[:-2] == x2.shape[:-2]
        # vectors -> matrices
        x1 = x1.view((*x1.shape[:-1], n, p))
        x1T = th.transpose(x1, dim0=-2, dim1=-1)  # shape = (a0..., b1,p, n)
        x2 = x2.view((*x2.shape[:-1], n, p))  # shape = (a0..., b2, n ,p)
        # batch matrix multiplication
        x1Tx2 = th.zeros((*x1T.shape[-3:-2], *x2.shape[-3:-2], *x1T.shape[:-3], p, p)) # shape = (b1, b2, ..., p, p)
        x1T = x1T.view((*x1T.shape[-3:-2], *x1T.shape[:-3], p, n))  # shape = (b1, ..., p,n)
        x2 = x2.view((*x2.shape[-3:-2], *x2.shape[:-3], n, p))  # shape = (b2, ..., n, p)
        for i in range(len(x1T)):
            x1Tx2[i] = th.matmul(x1T[i], x2)
        x1Tx2 = x1Tx2.view((*x1T.shape[1:-2], *x1T.shape[0:1], *x2.shape[0:1], p, p))  # shape = (..., b1, b2, p,p)
        res = th.square(th.norm(x1Tx2, p='fro', dim=[x1Tx2.dim() - 2, x1Tx2.dim() - 1]))
        return res if not postprocess else self._postprocess(res)

    def _GM_projection_dist(self, x1, x2, n, p, postprocess):
        sin_primal_angle_square_sum = p - self._GM_projection_kernel(x1, x2, n, p, th.tensor(False))
        sin_primal_angle_square_sum[sin_primal_angle_square_sum < 0] = 0
        res = th.sqrt(sin_primal_angle_square_sum)
        return res if not postprocess else self._postprocess(res)


'''
Two Types of positive definite Grassmann kernels: Laplace Kernel and RBF Kernel.
These two types of kernels are universal, and easy to implement.
Both of them have the form e^(beta*f(K1, K2)), so the forward() method of both of them resembles
gpytorch.kernels.RBFKernel.forward().
In RBF kernel, f(K1, K2) is a projection kernel and beta > 0, while in Laplace Kernel, f(K1, K2) is a projection
distance and beta < 0.
Refer to M. Harandi, M. Salzmann, S. Jayasumana et al.: Expanding the family of Grassmannian kernels: 
An embedding perspective. In Lecture Notes in Computer Science 8695 LNCS(7), 408-423(2014) 
for more information on Grassmann kernels.
'''


class GMKernel(gp.kernels.Kernel):
    has_lengthscale = True

    def __init__(
            self,
            n, p,
            ard_num_dims: Optional[int] = None,
            batch_shape: Optional[th.Size] = th.Size([]),
            active_dims: Optional[Tuple[int, ...]] = None,
            lengthscale_prior: Optional[Prior] = None,
            lengthscale_constraint: Optional[Interval] = None,
            eps: Optional[float] = 1e-6,
    ):
        super(GMKernel, self).__init__(ard_num_dims,
                                       batch_shape,
                                       active_dims,
                                       lengthscale_prior,
                                       lengthscale_constraint,
                                       eps,
                                       )
        self.n = n
        self.p = p

    def setGM(self, n, p):
        self.n = n
        self.p = p


class RBFGMKernel(GMKernel):

    # Rewrite gpytorch.kernels.kernel.Kernel.covar_dist()
    # Ref: https://docs.gpytorch.ai/en/stable/_modules/gpytorch/kernels/kernel.html#Kernel
    def covar_dist(self, x1, x2, dist_postprocess_func=lambda x: x, postprocess=True, diag=False,last_dim_is_batch=False,
                   **params):
        # last_dim_is_batch is mute
        assert not last_dim_is_batch
        # torch scripts expect tensors
        postprocess = th.tensor(postprocess)
        # lazy evaluation when x1 == x2
        x1_eq_x2 = th.equal(x1, x2)
        if diag:
            assert x1_eq_x2
            dist_shape = (*x1.shape[:-2], x1.shape[-2])
            dist = self.p * th.ones(dist_shape)
            return dist_postprocess_func(dist).reshape(dist_shape)
        # Cache the Distance object or else JIT will recompile every time
        if not self.distance_module or self.distance_module._postprocess != dist_postprocess_func:
            self.distance_module = GMDistance(dist_postprocess_func)
        # return distance
        return self.distance_module._GM_projection_kernel(x1, x2, self.n, self.p, postprocess)

    # Rewrite gpytorch.kernels.rbf_kernel.Kernel.forward()
    # Ref: https://docs.gpytorch.ai/en/stable/_modules/gpytorch/kernels/rbf_kernel.html#RBFKernel
    def forward(self, x1, x2, **params):
        covar = self.covar_dist(x1, x2,
                                dist_postprocess_func=lambda x: x.mul(self.lengthscale).exp(), postprocess=True,
                                **params)
        return covar


class LaplaceGMKernel(GMKernel):

    def covar_dist(self, x1, x2,
                   dist_postprocess_func=lambda x: x, postprocess=True, diag=False,last_dim_is_batch=False,
                   **params):
        # last_dim_is_batch is mute
        assert not last_dim_is_batch
        # torch scripts expect tensors
        postprocess = th.tensor(postprocess)
        # lazy evaluation when x1 == x2
        x1_eq_x2 = th.equal(x1, x2)
        if diag:
            assert x1_eq_x2
            dist_shape = (*x1.shape[:-2], x1.shape[-2])
            dist = th.zeros(dist_shape)
            return dist_postprocess_func(dist).reshape(dist_shape)
        # Cache the Distance object or else JIT will recompile every time
        if not self.distance_module or self.distance_module._postprocess != dist_postprocess_func:
            self.distance_module = GMDistance(dist_postprocess_func)
        # return distance
        return self.distance_module._GM_projection_dist(x1, x2, self.n, self.p, postprocess)

    def forward(self, x1, x2, **params):
        return self.covar_dist(x1, x2,
                               dist_postprocess_func=lambda x: x.mul(-self.lengthscale).exp(), postprocess=True,
                               **params)


if __name__ == '__main__':
    '''
        module test for two GM Kernels
        The core of the kernels are the batch computation of covar_dist
    '''
    x1 = th.randn((4, 60, 3, 6))
    x2 = th.randn((4, 60, 100, 6))
    covar1 = RBFGMKernel(n=3, p=2)
    covar2 = LaplaceGMKernel(n=3, p=2)
    print(covar1.covar_dist(x1, x2, n=3, p=2).shape)
    print(covar2.covar_dist(x1, x2, n=3, p=2).shape)
