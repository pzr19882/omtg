from pymanopt.autodiff.backends._pytorch import _PyTorchBackend
from pymanopt.autodiff.backends._backend import Backend
from pymanopt.autodiff import make_tracing_backend_decorator
import functools


class _MyPyTorchBackend(_PyTorchBackend):
    """
    This class is developed because the backends given in pymanopt 0.2.5 is not reliable.
    Specifically, in the file pymanopt.autodiff.backends._pytorch, line 47 should be
    "return function(*map(self._from_numpy, args)).detach().numpy()" instead of
    "return function(*map(self._from_numpy, args)).numpy()"
    I inherit the erroneous class and rewrite the corresponding method
    in order to circumvent modifying the pymanopt library directly
    """

    @Backend._assert_backend_available
    def compile_function(self, function):
        @functools.wraps(function)
        def wrapper(*args):
            return function(*map(self._from_numpy, args)).detach().numpy()

        return wrapper


MyPyTorch = make_tracing_backend_decorator(_MyPyTorchBackend)
