import numpy as np
from pymanopt.manifolds import Grassmann

'''
These classes are written to strengthen the ability to sample points on manifolds
so that points can be sampled from any distributions.
In pymanopt.manifolds.manifold, points can only be sampled from a normal distribution. 
'''


class randOnManifold:
    def __init__(self, manifold, distribution):
        self.manifold = manifold
        self.distribution = distribution

    def rand(self, *args, **params):
        pass

    def __call__(self, *args, **kwargs):
        return self.rand(*args, **kwargs)


class randOnGM(randOnManifold):
    def __init__(self, manifold, distribution):
        super(randOnGM, self).__init__(manifold, distribution)

    '''
    Note that numpy 1.22 supports vectorized qr decomposition,
    and projecting samples on grassmann manifold can be directly achieved by
    Q ,r = np.linalg.qr(X.reshape((k,n, p))) no matter what value k is.
    '''

    def rand(self, mat_to_vec=False, data_size: tuple = (1,), **params):
        k = self.manifold._k
        n = self.manifold._n
        p = self.manifold._p
        size = data_size if k == 1 else data_size + (k,)
        X = self.distribution(**params, size=size)
        Q, r = np.linalg.qr(X.reshape((*size, n, p)))
        return Q.reshape((*size, n * p)) if mat_to_vec else Q


if __name__ == '__main__':
    Gn = 56
    Gp = 7
    data_size = (1,)
    m1 = Grassmann(n=Gn, p=Gp)
    m2 = Grassmann(n=Gn, p=Gp, k=100)
    mean = np.zeros(Gn * Gp)
    cov = np.identity(Gn * Gp)
    rng1 = randOnGM(m1, np.random.multivariate_normal)
    rng2 = randOnGM(m2, np.random.multivariate_normal)
    print(rng1(mean=mean, cov=cov, data_size=data_size).shape, rng2(mean=mean, cov=cov, data_size=data_size).shape)
    print(rng1(mean=mean, cov=cov, data_size=data_size, mat_to_vec=True).shape,
          rng2(mean=mean, cov=cov, data_size=data_size, mat_to_vec=True).shape)
